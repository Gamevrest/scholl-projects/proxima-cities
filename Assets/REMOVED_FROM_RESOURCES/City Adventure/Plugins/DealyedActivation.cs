﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DealyedActivation : MonoBehaviour {

	public float delay;
	List<Transform> childs;

	// Use this for initialization
	private void Start () {

		childs = new List<Transform> ();
		for (var i = 0; i < transform.childCount; i++) {
			childs.Add(transform.GetChild(i));
			transform.GetChild (i).gameObject.SetActive (false);
		}

		StartCoroutine (Enable ());
	}

	private IEnumerator Enable(){
		var iterations=Mathf.RoundToInt(delay/Time.deltaTime)*200;
		if (iterations == 0)
			iterations = 1;
		var seed = 0;
		while (childs.Count > 0) {
			yield return new WaitForSeconds (delay);
			for (int a = 0; a < iterations; a++) {
				if (childs.Count == 0)
					yield break;
				var i = Random.Range (0, childs.Count);
				childs [i].gameObject.SetActive (true);
				childs.RemoveAt (i);
				Random.InitState(++seed);
			}
		}
	}
}
