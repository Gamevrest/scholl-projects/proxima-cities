﻿using System.Collections;
using System.Collections.Generic;
using Translation;
using UnityEngine;
using UnityEngine.SceneManagement;

public class trInitializer : MonoBehaviour
{
    void Start()
    {
        Tr.SelectLanguage(Tr.Languages.francais);
        StartCoroutine(Finished());
    }

    IEnumerator Finished()
    {
        yield return new WaitUntil(() => Tr.hasFinished);
        Tr.SelectLanguage(Tr.Languages.francais);
        SceneManager.LoadScene(1);
    }
}
