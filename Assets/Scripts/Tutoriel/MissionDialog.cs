﻿using System;
using System.Collections.Generic;
using TMPro;
using Translation;
using UnityEngine;
using UnityEngine.UI;

public class MissionDialog : MonoBehaviour
{
    public GameObject tutorielObject;
    public GameObject mascotte;
    public Image redCircle;
    private RectTransform redCircleRectTransform;
    public TextMeshProUGUI displayedText;
    public List<Tuple<string, object[], Vector2, int>> ids = new List<Tuple<string, object[], Vector2, int>>();
    private readonly List<List<Tuple<string, object[], Vector2, int>>> _wait = new List<List<Tuple<string, object[], Vector2, int>>>();

    private readonly List<bool> _waitBool = new List<bool>();

    // private string currentText;
    //private bool clock = false;
    private int _index;
    private AutoSetter _setter;

    private void Start()
    {
        redCircleRectTransform = redCircle.GetComponent<RectTransform>();
        _setter = new AutoSetter("MISSION_DEFAULT", displayedText);
        ids.Clear();
        //InvokeRepeating(nameof(TextAnimation), 0f, 0.8f);
    }

    private void Update()
    {
        if (tutorielObject.activeSelf && Input.GetMouseButtonDown(0))
            UpdateDisplay();
    }

    private void UpdateDisplay()
    {
        if (_index < ids.Count)
        {
            ChangeMessage(ids[_index].Item1, ids[_index].Item2, ids[_index].Item3, ids[_index].Item4);
            _index++;
        }
        else
        {
            _index = 0;
            ids.Clear();
            if (_wait.Count > 0)
            {
                ids = _wait[0];
                mascotte.SetActive(_waitBool[0]);
                _wait.RemoveAt(0);
                _waitBool.RemoveAt(0);
                UpdateDisplay();
            }
            else
            {
                DisplayDialog(false);
            }
        }
    }

    public void SetUpDialog(List<Tuple<string, object[], Vector2, int>> text, bool showMascot = false)
    {
        if (ids.Count > 0)
        {
            _wait.Add(text);
            _waitBool.Add(showMascot);
            return;
        }

        DisplayDialog(true);
        mascotte.SetActive(showMascot);
        ids.Clear();
        ids = text;
        UpdateDisplay();
    }

    private void ChangeMessage(string id, object[] vars, Vector2 position, int scale)
    {
        _setter.SetId(id);
        _setter.SetVars(vars);
        Debug.Log("POS => " +position);
        redCircleRectTransform.anchoredPosition = new Vector3(position.x, position.y);
        redCircleRectTransform.localScale = new Vector3(scale, scale);
        //displayedText.text = text;
    }

    private void DisplayDialog(bool display)
    {
        tutorielObject.SetActive(display);
    }

//    private void TextAnimation()
//    {
//        clock = !clock;
//        if (clock)
//            ChangeMessage(currentText);
//        else
//            ChangeMessage(currentText + ".");
//    }
}