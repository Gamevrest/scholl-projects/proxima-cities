using UnityEngine;

public class XPGainedEvent : GameEvent
{
    private long xp;

    public XPGainedEvent() : base(GameEventType.XP_GAINED)
    {
                
    }

    public XPGainedEvent setXP(long newXp)
    {
        xp = newXp;
        return this;
    }

    public long getXP()
    {
        return xp;
    }
    
}