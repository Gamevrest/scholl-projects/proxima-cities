public class PlantSeedEvent : GameEvent
{

    private int _potIndex;
    private SeedType _type;
    public PlantSeedEvent() : base(GameEventType.PLANT_SEED)
    {
        
    }

    public PlantSeedEvent setPotIndex(int potIndex)
    {
        _potIndex = potIndex;
        return this;
    }

    
    public int getPotIndex()
    {
        return _potIndex;
    }
    
    public PlantSeedEvent setSeedType(SeedType type)
    {
        _type = type;
        return this;
    }

    
    public SeedType getSeedType()
    {
        return _type;
    }
    
}