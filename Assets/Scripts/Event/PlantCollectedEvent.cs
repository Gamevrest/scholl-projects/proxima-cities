public class PlantCollectedEvent : GameEvent
{

    private int _potIndex = -1;
    private int _potIndex2 = -1;
    public PlantCollectedEvent() : base(GameEventType.PLANT_COLLECTED)
    {
        
    }

    public PlantCollectedEvent setPotIndex(int potIndex)
    {
        _potIndex = potIndex;
        return this;
    }

    
    public int getPotIndex()
    {
        return _potIndex;
    }
    
    public PlantCollectedEvent setSecondPotIndex(int potIndex)
    {
        _potIndex2 = potIndex;
        return this;
    }

    
    public int getSecondPotIndex()
    {
        return _potIndex2;
    }
    
}