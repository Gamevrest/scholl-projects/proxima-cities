using GameLogic.Weather;
using UnityEngine;

public class WeatherChangeEvent : GameEvent
{
    private WeatherState _weather;

    public WeatherChangeEvent() : base(GameEventType.WEATHER_CHANGE)
    {
                
    }

    public WeatherChangeEvent SetWeather(WeatherState newWeather)
    {
        _weather = newWeather;
        return this;
    }

    public WeatherState GetWeather()
    {
        return _weather;
    }
}