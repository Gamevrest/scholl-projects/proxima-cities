public class SeedFinishGrowEvent : GameEvent
{

    private SeedType _type;
    private int _potIndex;
    public SeedFinishGrowEvent() : base(GameEventType.SEED_FINISH_GROW)
    {
        
    }

    public SeedFinishGrowEvent setSeedType(SeedType newType)
    {
        _type = newType;
        return this;
    }

    
    public SeedType getSeedType()
    {
        return _type;
    }
    
    public SeedFinishGrowEvent setPotIndex(int potIndex)
    {
        _potIndex = potIndex;
        return this;
    }

    
    public int getPotIndex()
    {
        return _potIndex;
    }
    
}