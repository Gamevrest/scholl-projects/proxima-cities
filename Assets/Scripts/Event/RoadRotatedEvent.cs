using UnityEngine;

public class RoadRotatedEvent : GameEvent
{

    private Vector2Int _position;
    private int _rotation;
    public RoadRotatedEvent() : base(GameEventType.ROAD_ROTATED)
    {
        
    }
    public RoadRotatedEvent setPosition(Vector2Int newPos)
    {
        _position = newPos;
        return this;
    }

    
    public Vector2Int getPosition()
    {
        return _position;
    }

    public RoadRotatedEvent setRotation(int newRot)
    {
        _rotation = newRot;
        return this;
    }

    
    public int getRotation()
    {
        return _rotation;
    }
}