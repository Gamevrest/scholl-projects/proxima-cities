using UnityEngine;

public class UpdateRoadEvent : GameEvent
{
    private int _neighbours;

    private Vector2Int _position;
    private int _rotation;
    private RoadType _type;

    public UpdateRoadEvent() : base(GameEventType.UPDATE_ROAD)
    {
    }

    public UpdateRoadEvent setPosition(Vector2Int newPos)
    {
        _position = newPos;
        return this;
    }


    public Vector2Int getPosition()
    {
        return _position;
    }

    public UpdateRoadEvent setRoadType(RoadType newType)
    {
        _type = newType;
        return this;
    }


    public RoadType getRoadType()
    {
        return _type;
    }

    public UpdateRoadEvent setRotation(int newRot)
    {
        _rotation = newRot;
        return this;
    }


    public int getRotation()
    {
        return _rotation;
    }

    public UpdateRoadEvent setNeighbours(int neighbours)
    {
        _neighbours = neighbours;
        return this;
    }

    public int getNeighbours()
    {
        return _neighbours;
    }
}