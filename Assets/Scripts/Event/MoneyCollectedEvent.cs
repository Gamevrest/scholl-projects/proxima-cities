using UnityEngine;

public class MoneyCollectedEvent : GameEvent
{
    private Vector2 position;
    private long amount;

    public MoneyCollectedEvent() : base(GameEventType.MONEY_COLLECTED)
    {
                
    }

    public MoneyCollectedEvent setPosition(Vector2 newPosition)
    {
        position = newPosition;
        return this;
    }
    
    public MoneyCollectedEvent setAmount(long newAmount)
    {
        amount = newAmount;
        return this;
    }

    public Vector2 getPosition()
    {
        return position;
    }

    public long getAmount()
    {
        return amount;
    }
}