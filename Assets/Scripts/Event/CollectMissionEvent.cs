public class CollectMissionEvent : GameEvent
{
    private int _missionIndex;
    public CollectMissionEvent() : base(GameEventType.COLLECT_MISSION)
    {   
    }
    
    public CollectMissionEvent setMissionIndex(int index)
    {
        _missionIndex = index;
        return this;
    }

    public int getMissionIndex()
    {
        return _missionIndex;
    }
}