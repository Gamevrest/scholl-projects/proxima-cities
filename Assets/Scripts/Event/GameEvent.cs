public abstract class GameEvent
{
    private readonly GameEventType eventType = GameEventType.NONE;

    protected GameEvent()
    {
    }

    protected GameEvent(GameEventType newEventType)
    {
        eventType = newEventType;
    }

    public GameEventType getEventType()
    {
        return eventType;
    }
}