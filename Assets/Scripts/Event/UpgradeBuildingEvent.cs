using UnityEngine;

public class UpgradeBuildingEvent : GameEvent
{
    private Vector2Int _position;
    private BuildingType _oldType;
    private BuildingType _type;

    public UpgradeBuildingEvent() : base(GameEventType.UPGRADE_BUILDING)
    {
                
    }

    public UpgradeBuildingEvent setPosition(Vector2Int newPosition)
    {
        _position = newPosition;
        return this;
    }

    public Vector2Int getPosition()
    {
        return _position;
    }

    public UpgradeBuildingEvent setNewBuildingType(BuildingType newType)
    {
        _type = newType;
        return this;
    }
    
    public BuildingType getNewBuildingType()
    {
        return _type;
    }
    
    public UpgradeBuildingEvent setOldBuildingType(BuildingType newType)
    {
        _oldType = newType;
        return this;
    }
    
    public BuildingType getOldBuildingType()
    {
        return _oldType;
    }


}