using UnityEngine;

public class BuildingSoldEvent : GameEvent
{
    private Vector2Int position;

    public BuildingSoldEvent() : base(GameEventType.BUILDING_SOLD)
    {
                
    }

    public BuildingSoldEvent setPosition(Vector2Int newPosition)
    {
        position = newPosition;
        return this;
    }

    public Vector2Int getPosition()
    {
        return position;
    }
}