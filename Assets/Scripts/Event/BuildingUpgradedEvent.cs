using UnityEngine;

public class BuildingUpgradedEvent : GameEvent
{
    private Vector2 position;
    private BuildingType oldType;
    private BuildingType type;

    public BuildingUpgradedEvent() : base(GameEventType.BUILDING_UPGRADED)
    {
                
    }

    public BuildingUpgradedEvent setPosition(Vector2 newPosition)
    {
        position = newPosition;
        return this;
    }

    public Vector2 getPosition()
    {
        return position;
    }

    public BuildingUpgradedEvent setNewBuildingType(BuildingType newType)
    {
        type = newType;
        return this;
    }
    
    public BuildingType getNewBuildingType()
    {
        return type;
    }
    
    public BuildingUpgradedEvent setOldBuildingType(BuildingType newType)
    {
        oldType = newType;
        return this;
    }
    
    public BuildingType getOldBuildingType()
    {
        return oldType;
    }


}