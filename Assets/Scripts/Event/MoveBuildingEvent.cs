using UnityEngine;

public class MoveBuildingEvent : GameEvent
{
    
    private Vector2Int position;
    private Vector2Int oldPosition;

    public MoveBuildingEvent() : base(GameEventType.MOVE_BUILDING)
    {
                
    }
    
    public MoveBuildingEvent setNewPosition(Vector2Int newPosition)
    {
        position = newPosition;
        return this;
    }

    public Vector2Int getNewPosition()
    {
        return position;
    }
    
    public MoveBuildingEvent setOldPosition(Vector2Int oldPosition)
    {
        this.oldPosition = oldPosition;
        return this;
    }

    public Vector2Int getOldPosition()
    {
        return oldPosition;
    }
}