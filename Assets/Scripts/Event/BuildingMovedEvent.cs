using UnityEngine;

public class BuildingMovedEvent : GameEvent
{
    
    private Vector2 position;
    private Vector2 oldPosition;

    public BuildingMovedEvent() : base(GameEventType.BUILDING_MOVED)
    {
                
    }
    
    public BuildingMovedEvent setNewPosition(Vector2 newPosition)
    {
        position = newPosition;
        return this;
    }

    public Vector2 getNewPosition()
    {
        return position;
    }
    
    public BuildingMovedEvent setOldPosition(Vector2 oldPosition)
    {
        this.oldPosition = oldPosition;
        return this;
    }

    public Vector2 getOldPosition()
    {
        return oldPosition;
    }
}