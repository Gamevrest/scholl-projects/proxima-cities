using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Event;

public class EventManager
{
    
    private static EventManager instance;

    private EventManager() {}

    public static EventManager Instance => instance ?? (instance = new EventManager());

    private readonly List<Action<GameEvent>> _subscribersAll = new List<Action<GameEvent>>();
    private readonly List<Action<AddBuildingEvent>> _subscribersAddBuildingEvent = new List<Action<AddBuildingEvent>>();
    private readonly List<Action<BuildingAddedEvent>> _subscribersBuildingAddedEvent = new List<Action<BuildingAddedEvent>>();
    private readonly List<Action<UpgradeBuildingEvent>> _subscribersUpgradeBuildingEvent = new List<Action<UpgradeBuildingEvent>>();
    private readonly List<Action<BuildingUpgradedEvent>> _subscribersBuildingUpgradedEvent = new List<Action<BuildingUpgradedEvent>>();
    private readonly List<Action<LevelDownBuildingEvent>> _subscribersLevelDownBuildingEvent = new List<Action<LevelDownBuildingEvent>>();
    private readonly List<Action<LevelUpBuildingEvent>> _subscribersLevelUpBuildingEvent = new List<Action<LevelUpBuildingEvent>>();
    private readonly List<Action<RenameBuildingEvent>> _subscribersRenameBuildingEvent = new List<Action<RenameBuildingEvent>>();
    private readonly List<Action<BuildingRenamedEvent>> _subscribersBuildingRenamedEvent = new List<Action<BuildingRenamedEvent>>();
    private readonly List<Action<ResetDataEvent>> _subscribersResetDataEvent = new List<Action<ResetDataEvent>>();
    private readonly List<Action<DataResetedEvent>> _subscribersDataResetedEvent= new List<Action<DataResetedEvent>>();
    private readonly List<Action<MoveBuildingEvent>> _subscribersMoveBuildingEvent = new List<Action<MoveBuildingEvent>>();
    private readonly List<Action<BuildingMovedEvent>> _subscribersBuildingMovedEvent = new List<Action<BuildingMovedEvent>>();
    private readonly List<Action<SwitchTo2DEvent>> _subscribersSwitchTo2DEvent = new List<Action<SwitchTo2DEvent>>();
    private readonly List<Action<SwitchToAREvent>> _subscribersSwitchToArEvent = new List<Action<SwitchToAREvent>>();
    private readonly List<Action<CollectMoneyEvent>> _subscribersCollectMoneyEvent = new List<Action<CollectMoneyEvent>>();
    private readonly List<Action<MoneyCollectedEvent>> _subscribersMoneyCollectedEvent = new List<Action<MoneyCollectedEvent>>();
    private readonly List<Action<GameLoopEvent>> _subscribersGameLoopEvent = new List<Action<GameLoopEvent>>();
    private readonly List<Action<StartLoadingEvent>> _subscribersStartLoadingEvent = new List<Action<StartLoadingEvent>>();
    private readonly List<Action<StopLoadingEvent>> _subscribersStopLoadingEvent = new List<Action<StopLoadingEvent>>();
    private readonly List<Action<SellBuildingEvent>> _subscribersSellBuildingEvent = new List<Action<SellBuildingEvent>>();
    private readonly List<Action<BuildingSoldEvent>> _subscribersBuildingSoldEvent = new List<Action<BuildingSoldEvent>>();
    private readonly List<Action<AddRoadEvent>> _subscribersAddRoadEvent = new List<Action<AddRoadEvent>>();
    private readonly List<Action<RemoveRoadEvent>> _subscribersRemoveRoadEvent = new List<Action<RemoveRoadEvent>>();
    private readonly List<Action<RoadAddedEvent>> _subscribersRoadAddedEvent = new List<Action<RoadAddedEvent>>();
    private readonly List<Action<RoadRemovedEvent>> _subscribersRoadRemovedEvent = new List<Action<RoadRemovedEvent>>();
    private readonly List<Action<MissionUpdateEvent>> _subscribersMissionUpdateEvent = new List<Action<MissionUpdateEvent>>();
    private readonly List<Action<MissionCollectedEvent>> _subscribersMissionCollectedEvent = new List<Action<MissionCollectedEvent>>();
    private readonly List<Action<CollectMissionEvent>> _subscribersCollectMissionEvent = new List<Action<CollectMissionEvent>>();
    private readonly List<Action<RotateRoadEvent>> _subscribersRotateRoadEvent = new List<Action<RotateRoadEvent>>();
    private readonly List<Action<RoadRotatedEvent>> _subscribersRoadRotatedEvent = new List<Action<RoadRotatedEvent>>();
    private readonly List<Action<SeedFinishGrowEvent>> _subscribersSeedFinishGrowEvent = new List<Action<SeedFinishGrowEvent>>();
    private readonly List<Action<CollectPlantEvent>> _subscribersCollectPlantEvent = new List<Action<CollectPlantEvent>>();
    private readonly List<Action<StartMissionEvent>> _subscribersStartMissionEvent = new List<Action<StartMissionEvent>>();
    private readonly List<Action<PlantSeedEvent>> _subscribersPlantSeedEvent = new List<Action<PlantSeedEvent>>();
    private readonly List<Action<SeedPlantedEvent>> _subscribersSeedPlantedEvent = new List<Action<SeedPlantedEvent>>();
    private readonly List<Action<PlantCollectedEvent>> _subscribersPlantCollectedEvent = new List<Action<PlantCollectedEvent>>();
    private readonly List<Action<SwitchToGreenHouseEvent>> _subscribersSwitchToGreenHouseEvent = new List<Action<SwitchToGreenHouseEvent>>();
    private readonly List<Action<RoadUpdatedEvent>> _subscribersRoadUpdatedEvent = new List<Action<RoadUpdatedEvent>>();
    private readonly List<Action<UpdateRoadEvent>> _subscribersUpdateRoadEvent = new List<Action<UpdateRoadEvent>>();
    private readonly List<Action<XPGainedEvent>> _subscribersXpGainedEvent = new List<Action<XPGainedEvent>>();
    private readonly List<Action<WeatherChangeEvent>> _subscribersWeatherChangeEvent = new List<Action<WeatherChangeEvent>>();
    private readonly List<Action<ChangeLanguageEvent>> _subscribersChangeLanguageEvent = new List<Action<ChangeLanguageEvent>>();

    private readonly List<Action<GameErrorEvent>> _subscribersGameErrorEvent = new List<Action<GameErrorEvent>>();
    
    public void Raise(GameErrorEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }

    private void RaiseAsync(GameErrorEvent gameEvent) {
        for (var i = _subscribersGameErrorEvent.Count - 1; i >= 0; i--){new Task(() => {_subscribersGameErrorEvent[i](gameEvent);}).RunSynchronously();}
        RaiseAll(gameEvent);
    }

    public void Raise(AddBuildingEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }

    private void RaiseAsync(AddBuildingEvent gameEvent) {
        for (var i = _subscribersAddBuildingEvent.Count - 1; i >= 0; i--){new Task(() => {_subscribersAddBuildingEvent[i](gameEvent);}).RunSynchronously();}
        RaiseAll(gameEvent);
    }
    
    public void Raise(BuildingAddedEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }

    private void RaiseAsync(BuildingAddedEvent gameEvent) {
        for(var i = _subscribersBuildingAddedEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersBuildingAddedEvent[i](gameEvent);}).RunSynchronously(); }
        RaiseAll(gameEvent);
    }
    
    public void Raise(UpgradeBuildingEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }

    private void RaiseAsync(UpgradeBuildingEvent gameEvent) {
        for (var i = _subscribersUpgradeBuildingEvent.Count - 1; i >= 0; i--){new Task(() => { _subscribersUpgradeBuildingEvent[i](gameEvent); }).RunSynchronously();}
        RaiseAll(gameEvent);
    }
    
    public void Raise(BuildingUpgradedEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }

    private void RaiseAsync(BuildingUpgradedEvent gameEvent) {
        for(var i = _subscribersBuildingUpgradedEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersBuildingUpgradedEvent[i](gameEvent);}).RunSynchronously();}
        RaiseAll(gameEvent);
    }
    
    public void Raise(LevelDownBuildingEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }

    private void RaiseAsync(LevelDownBuildingEvent gameEvent) {
        for(var i = _subscribersLevelDownBuildingEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersLevelDownBuildingEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(LevelUpBuildingEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }

    private void RaiseAsync(LevelUpBuildingEvent gameEvent) {
        for(var i = _subscribersLevelUpBuildingEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersLevelUpBuildingEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(RenameBuildingEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }

    private void RaiseAsync(RenameBuildingEvent gameEvent) {
        for(var i = _subscribersRenameBuildingEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersRenameBuildingEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(BuildingRenamedEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }

    private void RaiseAsync(BuildingRenamedEvent gameEvent) {
        for(var i = _subscribersBuildingRenamedEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersBuildingRenamedEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(ResetDataEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }

    private void RaiseAsync(ResetDataEvent gameEvent) {
        for(var i = _subscribersResetDataEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersResetDataEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(DataResetedEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(DataResetedEvent gameEvent) {
        for(var i = _subscribersDataResetedEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersDataResetedEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(MoveBuildingEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(MoveBuildingEvent gameEvent) {
        for(var i = _subscribersMoveBuildingEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersMoveBuildingEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(BuildingMovedEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(BuildingMovedEvent gameEvent) {
        for(var i = _subscribersBuildingMovedEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersBuildingMovedEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(SwitchTo2DEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(SwitchTo2DEvent gameEvent) {
        for(var i = _subscribersSwitchTo2DEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersSwitchTo2DEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(SwitchToAREvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(SwitchToAREvent gameEvent) {
        for(var i = _subscribersSwitchToArEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersSwitchToArEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(CollectMoneyEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(CollectMoneyEvent gameEvent) {
        for(var i = _subscribersCollectMoneyEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersCollectMoneyEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(MoneyCollectedEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(MoneyCollectedEvent gameEvent) {
        for(var i = _subscribersMoneyCollectedEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersMoneyCollectedEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(GameLoopEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(GameLoopEvent gameEvent) {
        for(var i = _subscribersGameLoopEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersGameLoopEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(StartLoadingEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(StartLoadingEvent gameEvent) {
        for(var i = _subscribersStartLoadingEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersStartLoadingEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(StopLoadingEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(StopLoadingEvent gameEvent) {
        for(var i = _subscribersStopLoadingEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersStopLoadingEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(SellBuildingEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(SellBuildingEvent gameEvent) {
        for(var i = _subscribersSellBuildingEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersSellBuildingEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(BuildingSoldEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(BuildingSoldEvent gameEvent) {
        for(var i = _subscribersBuildingSoldEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersBuildingSoldEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(AddRoadEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(AddRoadEvent gameEvent) {
        for(var i = _subscribersAddRoadEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersAddRoadEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(RemoveRoadEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(RemoveRoadEvent gameEvent) {
        for(var i = _subscribersRemoveRoadEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersRemoveRoadEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(RoadAddedEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(RoadAddedEvent gameEvent) {
        for(var i = _subscribersRoadAddedEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersRoadAddedEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(RoadRemovedEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(RoadRemovedEvent gameEvent) {
        for(var i = _subscribersRoadRemovedEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersRoadRemovedEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(MissionUpdateEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(MissionUpdateEvent gameEvent) {
        for(var i = _subscribersMissionUpdateEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersMissionUpdateEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(CollectMissionEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(CollectMissionEvent gameEvent) {
        for(var i = _subscribersCollectMissionEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersCollectMissionEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(MissionCollectedEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(MissionCollectedEvent gameEvent) {
        for(var i = _subscribersMissionCollectedEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersMissionCollectedEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }

    public void Raise(RotateRoadEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(RotateRoadEvent gameEvent) {
        for(var i = _subscribersRotateRoadEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersRotateRoadEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(RoadRotatedEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(RoadRotatedEvent gameEvent) {
        for(var i = _subscribersRoadRotatedEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersRoadRotatedEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(SeedFinishGrowEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(SeedFinishGrowEvent gameEvent) {
        for(var i = _subscribersSeedFinishGrowEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersSeedFinishGrowEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(CollectPlantEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(CollectPlantEvent gameEvent) {
        for(var i = _subscribersCollectPlantEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersCollectPlantEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(StartMissionEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(StartMissionEvent gameEvent) {
        for(var i = _subscribersStartMissionEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersStartMissionEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(PlantSeedEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(PlantSeedEvent gameEvent) {
        for(var i = _subscribersPlantSeedEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersPlantSeedEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(SeedPlantedEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(SeedPlantedEvent gameEvent) {
        for(var i = _subscribersSeedPlantedEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersSeedPlantedEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(PlantCollectedEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(PlantCollectedEvent gameEvent) {
        for(var i = _subscribersPlantCollectedEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersPlantCollectedEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(SwitchToGreenHouseEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(SwitchToGreenHouseEvent gameEvent) {
        for(var i = _subscribersSwitchToGreenHouseEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersSwitchToGreenHouseEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(RoadUpdatedEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(RoadUpdatedEvent gameEvent) {
        for(var i = _subscribersRoadUpdatedEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersRoadUpdatedEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(UpdateRoadEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(UpdateRoadEvent gameEvent) {
        for(var i = _subscribersUpdateRoadEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersUpdateRoadEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(XPGainedEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(XPGainedEvent gameEvent) {
        for(var i = _subscribersXpGainedEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersXpGainedEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(WeatherChangeEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(WeatherChangeEvent gameEvent) {
        for(var i = _subscribersWeatherChangeEvent.Count - 1; i >= 0; i--)  {new Task(() => {_subscribersWeatherChangeEvent[i](gameEvent);}).RunSynchronously();  }
        RaiseAll(gameEvent);
    }
    
    public void Raise(ChangeLanguageEvent gameEvent)
    {
        new Task(() => {RaiseAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAsync(ChangeLanguageEvent gameEvent) {
        for (var i = _subscribersChangeLanguageEvent.Count - 1; i >= 0; i--)
        {
            new Task(() => {_subscribersChangeLanguageEvent[i](gameEvent);}).RunSynchronously();
        }
        RaiseAll(gameEvent);
    }
    
    
    
    

    private void RaiseAll(GameEvent gameEvent)
    {
        new Task(() => {RaiseAllAsync(gameEvent);}).RunSynchronously();
    }
    
    private void RaiseAllAsync(GameEvent gameEvent) {
        for (var i = _subscribersAll.Count - 1; i >= 0; i--){new Task(() => {_subscribersAll[i](gameEvent);}).RunSynchronously();}
    }
    
    public void SubscribeAll(Action<GameEvent> subscriber) {
        _subscribersAll.Add(subscriber);
    }
    
    public void UnSubscribeAll(Action<GameEvent> subscriber) {
        _subscribersAll.Remove(subscriber);
    }
    
    
    
    
    
    public void Subscribe(Action<GameErrorEvent> subscriber) {
        _subscribersGameErrorEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<GameErrorEvent> subscriber) {
        _subscribersGameErrorEvent.Remove(subscriber);
    }
    
    
    public void Subscribe(Action<AddBuildingEvent> subscriber) {
        _subscribersAddBuildingEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<AddBuildingEvent> subscriber) {
        _subscribersAddBuildingEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<BuildingAddedEvent> subscriber) {
        _subscribersBuildingAddedEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<BuildingAddedEvent> subscriber) {
        _subscribersBuildingAddedEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<UpgradeBuildingEvent> subscriber) {
        _subscribersUpgradeBuildingEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<UpgradeBuildingEvent> subscriber) {
        _subscribersUpgradeBuildingEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<BuildingUpgradedEvent> subscriber) {
        _subscribersBuildingUpgradedEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<BuildingUpgradedEvent> subscriber) {
        _subscribersBuildingUpgradedEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<LevelDownBuildingEvent> subscriber) {
        _subscribersLevelDownBuildingEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<LevelDownBuildingEvent> subscriber) {
        _subscribersLevelDownBuildingEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<LevelUpBuildingEvent> subscriber) {
        _subscribersLevelUpBuildingEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<LevelUpBuildingEvent> subscriber) {
        _subscribersLevelUpBuildingEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<RenameBuildingEvent> subscriber) {
        _subscribersRenameBuildingEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<RenameBuildingEvent> subscriber) {
        _subscribersRenameBuildingEvent.Remove(subscriber);
    } 
    
    public void Subscribe(Action<BuildingRenamedEvent> subscriber) {
        _subscribersBuildingRenamedEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<BuildingRenamedEvent> subscriber) {
        _subscribersBuildingRenamedEvent.Remove(subscriber);
    } 
    
    public void Subscribe(Action<ResetDataEvent> subscriber) {
        _subscribersResetDataEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<ResetDataEvent> subscriber) {
        _subscribersResetDataEvent.Remove(subscriber);
    } 
    
    public void Subscribe(Action<DataResetedEvent> subscriber) {
        _subscribersDataResetedEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<DataResetedEvent> subscriber) {
        _subscribersDataResetedEvent.Remove(subscriber);
    } 
    
    public void Subscribe(Action<MoveBuildingEvent> subscriber) {
        _subscribersMoveBuildingEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<MoveBuildingEvent> subscriber) {
        _subscribersMoveBuildingEvent.Remove(subscriber);
    } 
    
    public void Subscribe(Action<BuildingMovedEvent> subscriber) {
        _subscribersBuildingMovedEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<BuildingMovedEvent> subscriber) {
        _subscribersBuildingMovedEvent.Remove(subscriber);
    } 
    
    public void Subscribe(Action<SwitchTo2DEvent> subscriber) {
        _subscribersSwitchTo2DEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<SwitchTo2DEvent> subscriber) {
        _subscribersSwitchTo2DEvent.Remove(subscriber);
    } 
    
    public void Subscribe(Action<SwitchToAREvent> subscriber) {
        _subscribersSwitchToArEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<SwitchToAREvent> subscriber) {
        _subscribersSwitchToArEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<MoneyCollectedEvent> subscriber) {
        _subscribersMoneyCollectedEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<MoneyCollectedEvent> subscriber) {
        _subscribersMoneyCollectedEvent.Remove(subscriber);
    } 
    
    public void Subscribe(Action<CollectMoneyEvent> subscriber) {
        _subscribersCollectMoneyEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<CollectMoneyEvent> subscriber) {
        _subscribersCollectMoneyEvent.Remove(subscriber);
    } 
    
    public void Subscribe(Action<GameLoopEvent> subscriber) {
        _subscribersGameLoopEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<GameLoopEvent> subscriber) {
        _subscribersGameLoopEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<StartLoadingEvent> subscriber) {
        _subscribersStartLoadingEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<StartLoadingEvent> subscriber) {
        _subscribersStartLoadingEvent.Remove(subscriber);
    } 
    
    public void Subscribe(Action<StopLoadingEvent> subscriber) {
        _subscribersStopLoadingEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<StopLoadingEvent> subscriber) {
        _subscribersStopLoadingEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<SellBuildingEvent> subscriber) {
        _subscribersSellBuildingEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<SellBuildingEvent> subscriber) {
        _subscribersSellBuildingEvent.Remove(subscriber);
    } 
    
    public void Subscribe(Action<BuildingSoldEvent> subscriber) {
        _subscribersBuildingSoldEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<BuildingSoldEvent> subscriber) {
        _subscribersBuildingSoldEvent.Remove(subscriber);
    } 
    
    public void Subscribe(Action<AddRoadEvent> subscriber) {
        _subscribersAddRoadEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<AddRoadEvent> subscriber) {
        _subscribersAddRoadEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<RemoveRoadEvent> subscriber) {
        _subscribersRemoveRoadEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<RemoveRoadEvent> subscriber) {
        _subscribersRemoveRoadEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<RoadAddedEvent> subscriber) {
        _subscribersRoadAddedEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<RoadAddedEvent> subscriber) {
        _subscribersRoadAddedEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<RoadRemovedEvent> subscriber) {
        _subscribersRoadRemovedEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<RoadRemovedEvent> subscriber) {
        _subscribersRoadRemovedEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<MissionUpdateEvent> subscriber) {
        _subscribersMissionUpdateEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<MissionUpdateEvent> subscriber) {
        _subscribersMissionUpdateEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<CollectMissionEvent> subscriber) {
        _subscribersCollectMissionEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<CollectMissionEvent> subscriber) {
        _subscribersCollectMissionEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<MissionCollectedEvent> subscriber) {
        _subscribersMissionCollectedEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<MissionCollectedEvent> subscriber) {
        _subscribersMissionCollectedEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<RotateRoadEvent> subscriber) {
        _subscribersRotateRoadEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<RotateRoadEvent> subscriber) {
        _subscribersRotateRoadEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<RoadRotatedEvent> subscriber) {
        _subscribersRoadRotatedEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<RoadRotatedEvent> subscriber) {
        _subscribersRoadRotatedEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<SeedFinishGrowEvent> subscriber) {
        _subscribersSeedFinishGrowEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<SeedFinishGrowEvent> subscriber) {
        _subscribersSeedFinishGrowEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<CollectPlantEvent> subscriber) {
        _subscribersCollectPlantEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<CollectPlantEvent> subscriber) {
        _subscribersCollectPlantEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<StartMissionEvent> subscriber) {
        _subscribersStartMissionEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<StartMissionEvent> subscriber) {
        _subscribersStartMissionEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<PlantSeedEvent> subscriber) {
        _subscribersPlantSeedEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<PlantSeedEvent> subscriber) {
        _subscribersPlantSeedEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<SeedPlantedEvent> subscriber) {
        _subscribersSeedPlantedEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<SeedPlantedEvent> subscriber) {
        _subscribersSeedPlantedEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<PlantCollectedEvent> subscriber) {
        _subscribersPlantCollectedEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<PlantCollectedEvent> subscriber) {
        _subscribersPlantCollectedEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<SwitchToGreenHouseEvent> subscriber) {
        _subscribersSwitchToGreenHouseEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<SwitchToGreenHouseEvent> subscriber) {
        _subscribersSwitchToGreenHouseEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<RoadUpdatedEvent> subscriber) {
        _subscribersRoadUpdatedEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<RoadUpdatedEvent> subscriber) {
        _subscribersRoadUpdatedEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<UpdateRoadEvent> subscriber) {
        _subscribersUpdateRoadEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<UpdateRoadEvent> subscriber) {
        _subscribersUpdateRoadEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<XPGainedEvent> subscriber) {
        _subscribersXpGainedEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<XPGainedEvent> subscriber) {
        _subscribersXpGainedEvent.Remove(subscriber);
    }
    
    public void Subscribe(Action<WeatherChangeEvent> subscriber) {
        _subscribersWeatherChangeEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<WeatherChangeEvent> subscriber) {
        _subscribersWeatherChangeEvent.Remove(subscriber);
    }
    
    
    public void Subscribe(Action<ChangeLanguageEvent> subscriber) {
        _subscribersChangeLanguageEvent.Add(subscriber);
    }
    
    public void UnSubscribe(Action<ChangeLanguageEvent> subscriber) {
        _subscribersChangeLanguageEvent.Remove(subscriber);
    }
    
    
}