using UnityEngine;

public class SellBuildingEvent : GameEvent
{
    private Vector2Int position;

    public SellBuildingEvent() : base(GameEventType.SELL_BUILDING)
    {
                
    }

    public SellBuildingEvent setPosition(Vector2Int newPosition)
    {
        position = newPosition;
        return this;
    }

    public Vector2Int getPosition()
    {
        return position;
    }
}