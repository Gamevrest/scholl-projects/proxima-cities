#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameLogic
{
#if UNITY_EDITOR
    [CustomEditor(typeof(GameLoop))]
    public class GameLoopEditor : Editor
    {
        private GameLoop _target;
        private List<string> _scenes = new List<string>();

        private void OnEnable()
        {
            _target = (GameLoop) target;
            EditorSceneManager.sceneClosed += EditorSceneManagerOnSceneClosed;
            EditorSceneManager.sceneOpened += EditorSceneManagerOnSceneOpened;
        }

        private void LoadScenes()
        {
            foreach (var scene in _target.scenesToLoad)
                if (!_scenes.Contains(scene))
                    EditorSceneManager.OpenScene(scene, OpenSceneMode.Additive);
        }

        private void EditorSceneManagerOnSceneClosed(Scene scene) => _scenes.Remove(scene.name);
        private void EditorSceneManagerOnSceneOpened(Scene scene, OpenSceneMode mode) => _scenes.Add(scene.name);

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (GUILayout.Button("Load Scenes"))
                LoadScenes();
        }
    }
#endif
}