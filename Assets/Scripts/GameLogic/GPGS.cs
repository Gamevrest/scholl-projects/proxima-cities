﻿using System;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using UnityEngine;

namespace GameLogic
{
    public class GPGS : MonoBehaviour
    {
        private static GPGS instance;
        private bool _logged;
        public static GPGS Instance => instance ? instance : instance = new GameObject("GPGS").AddComponent<GPGS>();

        public bool logged => _logged;

        private bool _mSaving;
        private Texture2D _mScreenImage;

        public void Awake()
        {
            instance = this;
#if UNITY_ANDROID
            var config = new PlayGamesClientConfiguration.Builder()
                .EnableSavedGames()
                .Build();

            PlayGamesPlatform.InitializeInstance(config);
            // recommended for debugging:
            PlayGamesPlatform.DebugLogEnabled = true;
            // Activate the Google Play Games platform
            PlayGamesPlatform.Activate();
#endif
        }

        public void Start()
        {
#if UNITY_ANDROID
            LogIn();
#endif
        }

        private void LogIn()
        {
            Social.localUser.Authenticate((bool success) =>
            {
                _logged = success;
                if (_logged)
                    PlayGamesPlatform.Instance.Events.IncrementEvent("CgkIrJHUrqUXEAIQAQ", 1);
                else
                {
                    Debug.Log("Login Error: Unable to authenticate");
                }
            });
        }

        public void NewGameEvent()
        {
            if (_logged)
                PlayGamesPlatform.Instance.Events.IncrementEvent("CgkIrJHUrqUXEAIQAg", 1);
        }

        public void LoadGameEvent()
        {
            if (_logged)
                PlayGamesPlatform.Instance.Events.IncrementEvent("CgkIrJHUrqUXEAIQBg", 1);
        }

        public void OpenCloudSave()
        {
            if (!_logged) return;
            const uint maxNumToDisplay = 5;
            const bool allowCreateNew = true;
            const bool allowDelete = true;

            ((PlayGamesPlatform) Social.Active).SavedGame.ShowSelectSavedGameUI("Select saved game",
                maxNumToDisplay,
                allowCreateNew,
                allowDelete,
                OnSavedGameSelected);
        }
        public void OnSavedGameSelected(SelectUIStatus status, ISavedGameMetadata game)
        {
            if (!_logged) return;
            if (status == SelectUIStatus.SavedGameSelected)
            {
                string filename;
                if (game == null || string.IsNullOrEmpty(game.Description) && string.IsNullOrEmpty(game.Filename))
                {
                    _mSaving = true;
                    filename = "save" + DateTime.Now.ToBinary();
                }
                else
                {
                    filename = game.Filename;
                }
                
                //open the data.
                ((PlayGamesPlatform) Social.Active).SavedGame.OpenWithAutomaticConflictResolution(
                    filename,
                    DataSource.ReadCacheOrNetwork,
                    ConflictResolutionStrategy.UseLongestPlaytime,
                    SavedGameOpened); 
            }
            else if (status == SelectUIStatus.UserClosedUI)
            {
                return;
            }
            else
            {
                EventManager.Instance.Raise(new GameErrorEvent().SetTitleId("GPGS_ONSAVEDGAMESELECTED")
                    .SetMessageId("Error with cloud save\n%s").SetVars(status));
            }
        }

        public void SavedGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game) {
            if (!_logged) return;
            Debug.Log("SavedGameOpened");

            if(status == SavedGameRequestStatus.Success) {
                if( _mSaving) {
                    Debug.Log("SAVING");

                    byte[] pngData = (_mScreenImage!=null) ?_mScreenImage.EncodeToPNG():null;
                    byte[] data = GameManager.Instance.GenerateCloudSave();
                    SavedGameMetadataUpdate.Builder builder =  new
                            SavedGameMetadataUpdate.Builder()
                        //.WithUpdatedPlayedTime(playedTime)
                        .WithUpdatedDescription(GameManager.Instance.GetTownHall().GetCityName()+" at " + DateTime.Now);

                    if (pngData != null) {
                        builder = builder.WithUpdatedPngCoverImage(pngData);
                    }
                    SavedGameMetadataUpdate updatedMetadata  = builder.Build();
                    ((PlayGamesPlatform) Social.Active).SavedGame.CommitUpdate(game,updatedMetadata,data,SavedGameWritten);
                } else {
                    ((PlayGamesPlatform) Social.Active).SavedGame.ReadBinaryData(game,SavedGameLoaded);
                }
            } else {
                EventManager.Instance.Raise(new GameErrorEvent().SetTitleId("GPGS_SAVEDGAMEOPENED").SetMessageId("Error with cloud save\n%s").SetVars(status));
            }
        }

        public void SavedGameLoaded(SavedGameRequestStatus status, byte[] data) {
            if (status == SavedGameRequestStatus.Success) {
                //Todo put the save in game data
                Data dat = GameManager.Instance.GenerateSaveFromCloudSave(data);
                GameManager.Instance.SetDataAndReload(dat);
            } else {
                EventManager.Instance.Raise(new GameErrorEvent().SetTitleId("GPGS_SAVEGAMEOPENED").SetMessageId("Error with cloud save\n%s").SetVars(status));
            }
        }


        public void SavedGameWritten(SavedGameRequestStatus status, ISavedGameMetadata game)
        {
            _mSaving = false;
            if(status == SavedGameRequestStatus.Success) {
                EventManager.Instance.Raise(new GameErrorEvent().SetTitleId("SUCCESS").SetMessageId("GPGS_GAME_SAVED_ON_CLOUD"));
            } else {
                EventManager.Instance.Raise(new GameErrorEvent().SetTitleId("GPGS_SAVEDGAMEWRITTEN").SetMessageId("Error with cloud save\n%s").SetVars(status));
            }
        }

        public IEnumerator CaptureScreenShot()
        {
            yield return new WaitForEndOfFrame();
            _mScreenImage = ScreenCapture.CaptureScreenshotAsTexture();
        }
    }
}