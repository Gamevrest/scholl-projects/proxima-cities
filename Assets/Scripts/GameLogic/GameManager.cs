using System;
using System.Collections.Generic;
using Buildings;
using GameLogic.Weather;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

public class GameManager
{
    private const float MoneyRefundPercentage = 0.5f;

    public static readonly Dictionary<BuildingType, BuildingsAbstract> AvailableBuildings =
        new Dictionary<BuildingType, BuildingsAbstract>
        {
            {BuildingType.HOUSE, new House()},
            {BuildingType.WIND_TURBINE, new WindTurbine()},
            {BuildingType.CHARCOAL_POWER_PLANTS, new CharcoalPowerPlant()},
            {BuildingType.WATER_RECYCLING_STATION, new WaterRecyclingStation()},
            {BuildingType.AIR_PURIFIER, new AirPurifier()},
            {BuildingType.PLASTIC_RECYCLING_STATION, new PlasticRecyclingStation()},
        };

    public static readonly Dictionary<BuildingType, BuildingsAbstract> AllBuildings =
        new Dictionary<BuildingType, BuildingsAbstract>
        {
            {BuildingType.HOUSE, new House()},
            {BuildingType.ECONOMICAL_HABITATION, new EconomicalHabitation()},
            {BuildingType.HABITATION, new Habitation()},
            {BuildingType.ECOLOGICAL_HOUSE, new EcologicalHouse()},
            {BuildingType.BUSINESS, new Business()},
            {BuildingType.LARGE_ECONOMICAL_HABITATION, new LargeEconomicalHabitation()},
            {BuildingType.BETTER_HABITATION, new BetterHabitation()},
            {BuildingType.ECOLOGICAL_HABITATION, new EcologicalHabitation()},
            {BuildingType.ECOLOGICAL_BUSINESS, new EcologicalBusiness()},
            {BuildingType.FIRST_CLASS_BUSINESS, new FirstClassBusiness()},
            {BuildingType.WIND_TURBINE, new WindTurbine()},
            {BuildingType.WATER_TURBINE, new WaterTurbine()},
            {BuildingType.CHARCOAL_POWER_PLANTS, new CharcoalPowerPlant()},
            {BuildingType.SOLAR_PANELS, new SolarPanels()},
            {BuildingType.NUCLEAR_POWER_PLANTS, new NuclearPowerPlant()},
            {BuildingType.PLASTIC_RECYCLING_STATION, new PlasticRecyclingStation()},
            {BuildingType.AIR_PURIFIER, new AirPurifier()},
            {BuildingType.WATER_RECYCLING_STATION, new WaterRecyclingStation()},
            {BuildingType.GREEN_HOUSE, new GreenHouseBuilding()},
        };

    public static readonly Dictionary<SeedType, Seed> AllSeeds = new Dictionary<SeedType, Seed>
    {
        {SeedType.BASIC, new SeedBasic()},
        {SeedType.ADVANCED, new SeedAdvanced()},
        {SeedType.EXPERIMENTAL, new SeedExperimental()},
        {SeedType.TWISTED, new SeedTwisted()},
    };

    private static GameManager instance;

    private readonly EventManager _eventManager;
    private Data _gameData;

    private MissionDialog _missionDialogRef;

    private WeatherManager _weatherManager = new WeatherManager();

    private GameManager()
    {
        _eventManager = EventManager.Instance;
        _eventManager.SubscribeAll(RaisedEvent);
        _eventManager.SubscribeAll(MissionEventHandler);
    }

    public void SetDataAndReload(Data dat)
    {
        _eventManager.Raise(new StartLoadingEvent());
        _gameData = dat;
        SceneManager.LoadScene("Core");
    }

    public static Dictionary<MissionName, Mission> missionList => MissionList.list;

    private MissionDialog missionDialog =>
        _missionDialogRef ? _missionDialogRef : _missionDialogRef = Object.FindObjectOfType<MissionDialog>();

    public static GameManager Instance => instance ?? (instance = new GameManager());


    ~GameManager()
    {
        _eventManager.UnSubscribeAll(RaisedEvent);
    }

    public void LoopGame()
    {
        foreach (var building in _gameData.GetBuildings())
            building.addMoneyToCollect(GetProductionPercentage(building.getPosition(), building.getEnergyNeeded()));
        _gameData.GreenHouseLoop(false);
        _weatherManager.WeatherLoop();
    }

    public void LoadData()
    {
        _gameData = SaveManager.Load();
        int i = 0;
        foreach (var mission in _gameData.GetCurrentMissions())
        {
            Debug.Log("Mission " + i + "\n" + mission);
            i++;
        }
    }

    public void SaveData()
    {
        SaveManager.saveGame(_gameData);
    }

    public void ResetData()
    {
        _gameData = new Data();
    }

    public List<BuildingsAbstract> GetBuildings()
    {
        return _gameData.GetBuildings();
    }

    public BuildingsAbstract GetBuildingByPosition(Vector2Int pos)
    {
        return _gameData.GetBuildingByPosition(pos);
    }

    public List<Road> GetRoads()
    {
        return _gameData.getRoads();
    }

    public Road GetRoadByPosition(Vector2Int pos, bool shouldThrow = true)
    {
        return _gameData.GetRoadByPosition(pos, shouldThrow);
    }

    public long GetEnergyByPosition(Vector2Int pos)
    {
        return _gameData.GetEnergyByPosition(pos);
    }

    public double GetMoney()
    {
        return _gameData.GetMoney();
    }

    public long GetCitizens()
    {
        return _gameData.GetCitizens();
    }

    public long GetPollution()
    {
        return _gameData.GetPollution();
    }

    public void ReducePollution(long amount)
    {
        _gameData.RemovePollution(amount);
    }

    public void AddMoney(long amount)
    {
        _gameData.AddMoney(amount);
    }

    public void FinishTuto()
    {
        _gameData.FinishTuto();
    }

    public float GetProductionPercentage(Vector2Int pos, long needed)
    {
        var generated = _gameData.GetEnergyByPosition(pos);
        if (needed == 0 || generated == 0)
            return 10;
        var percentage = generated / needed * 100;
        if (percentage < 10) percentage = 10;
        if (percentage < 10)
            percentage = 10;
        else if (percentage > 100)
            percentage = 100;

        return percentage;
    }

    public float GetHapiness()
    {
        return _gameData.GetHapiness();
    }

    public List<Mission> GetCurrentMissions()
    {
        return _gameData.GetCurrentMissions();
    }

    public void AddMission(Mission mission)
    {
        _gameData.AddMission(mission);
    }

    private void RaisedEvent(GameEvent gameEvent)
    {
        switch (gameEvent.getEventType())
        {
            case GameEventType.ADD_BUILDING:
                Compute((AddBuildingEvent) gameEvent);
                break;
            case GameEventType.LEVEL_UP_BUILDING:
                Compute((LevelUpBuildingEvent) gameEvent);
                break;
            case GameEventType.LEVEL_DOWN_BUILDING:
                Compute((LevelDownBuildingEvent) gameEvent);
                break;
            case GameEventType.UPGRADE_BUILDING:
                Compute((UpgradeBuildingEvent) gameEvent);
                break;
            case GameEventType.RENAME_BUILDING:
                Compute((RenameBuildingEvent) gameEvent);
                break;
            case GameEventType.RESET_DATA:
                Compute((ResetDataEvent) gameEvent);
                break;
            case GameEventType.MOVE_BUILDING:
                Compute((MoveBuildingEvent) gameEvent);
                break;
            case GameEventType.COLLECT_MONEY:
                Compute((CollectMoneyEvent) gameEvent);
                break;
            case GameEventType.SELL_BUILDING:
                Compute((SellBuildingEvent) gameEvent);
                break;
            case GameEventType.ADD_ROAD:
                Compute((AddRoadEvent) gameEvent);
                break;
            case GameEventType.REMOVE_ROAD:
                Compute((RemoveRoadEvent) gameEvent);
                break;
            case GameEventType.UPDATE_ROAD:
                Compute((UpdateRoadEvent) gameEvent);
                break;
            case GameEventType.COLLECT_MISSION:
                Compute((CollectMissionEvent) gameEvent);
                break;
            case GameEventType.START_MISSION:
                Compute((StartMissionEvent) gameEvent);
                break;
            case GameEventType.ROTATE_ROAD:
                Compute((RotateRoadEvent) gameEvent);
                break;
            case GameEventType.PLANT_SEED:
                Compute((PlantSeedEvent) gameEvent);
                break;
            case GameEventType.COLLECT_PLANT:
                Compute((CollectPlantEvent) gameEvent);
                break;
            case GameEventType.XP_GAINED:
                Compute((XPGainedEvent) gameEvent);
                break;
            default:
                return;
        }
    }

    private void Compute(XPGainedEvent e)
    {
        _gameData.GetTownHall().AddExperience(e.getXP());
    }

    private void Compute(CollectPlantEvent e)
    {
        _gameData.GetGreenHouse().collectPlant(e);
    }

    private void Compute(PlantSeedEvent e)
    {
        _gameData.GetGreenHouse().plantInPot(e.getPotIndex(), e.getSeedType());
    }

    private void Compute(StartMissionEvent e)
    {
        var index = e.getMissionIndex();
        if (index > GetCurrentMissions().Count) return;

        var mission = GetCurrentMissions()[index];
        var textToShow = new List<Tuple<string, object[], Vector2, int>>();
        var missionToAdd = new List<PushMissionMissionAction>();
        mission.getActionOnStart().ForEach(action =>
        {
            switch (action.getMissionActionType())
            {
                case MissionActionType.SHOW_TEXT:
                    var ac = (ShowTextMissionAction) action;
                    textToShow.Add(new Tuple<string, object[], Vector2, int>(ac.GetId(), ac.GetVars(), ac.GetPosition(), ac.GetScale()));
                    break;
                case MissionActionType.SHOW_PROXIMIAM:
                    MissionHandler.Action((ShowProximiamMissionAction) action);
                    break;
                case MissionActionType.PUSH_MISSION:
                    missionToAdd.Add((PushMissionMissionAction) action);
                    break;
                case MissionActionType.GIVE_GOLD:
                    MissionHandler.Action((GiveMoneyMissionAction) action);
                    break;
                case MissionActionType.GIVE_SEED:
                    MissionHandler.Action((GiveSeedMissionAction) action);
                    break;
                case MissionActionType.REDUCE_POLLUTION:
                    MissionHandler.Action((ReducePollutionMissionAction) action);
                    break;
                case MissionActionType.FINISH_TUTO:
                    MissionHandler.Action((FinishTutoMissionAction) action);
                    break;
                case MissionActionType.ADD_BUILDING:
                    MissionHandler.Action((AddBuildingMissionAction) action);
                    break;
            }
        });
        missionDialog.SetUpDialog(textToShow, mission.getMissionType() == MissionType.TUTO);

        missionToAdd.ForEach(MissionHandler.Action);

        mission.getMissionRequirements().ForEach(requirement =>
        {
            switch (requirement.getMissionRequirementType())
            {
                case MissionRequirementType.POPULATION:
                    requirement.setCurrentCount(_gameData.GetCitizens());
                    break;
                case MissionRequirementType.BUILDING_OWNED:
                    requirement.setCurrentCount(
                        CountBuildingType(((BuildingOwnedMissionRequirement) requirement).getBuildingType()));
                    break;
                case MissionRequirementType.MISSION_DONE_COUNT:
                    requirement.setCurrentCount(_gameData.GetTotalMissionDone());
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        });
    }

    private void Compute(RotateRoadEvent e)
    {
        try
        {
            _gameData.GetRoadByPosition(e.getPosition()).SetRotation(e.getRotation());
            _eventManager.Raise(new RoadRotatedEvent().setPosition(e.getPosition()).setRotation(e.getRotation()));
        }
        catch (GamevrestException exception)
        {
            _eventManager.Raise(new GameErrorEvent().SetException(exception));
        }
        catch (Exception exception)
        {
            Debug.LogError(exception);
            //Console.WriteLine(exception);
            throw;
        }
    }

    private void Compute(CollectMissionEvent e)
    {
        var index = e.getMissionIndex();
        try
        {
            var mission = _gameData.GetCurrentMissions()[index];
            if (!mission.isCompleted()) return;
            _gameData.GetCurrentMissions().RemoveAt(index);
            var textToShow = new List<Tuple<string, object[], Vector2, int>>();
            var missionToAdd = new List<PushMissionMissionAction>();

            mission.getActionOnCompletion().ForEach(action =>
            {
                switch (action.getMissionActionType())
                {
                    case MissionActionType.SHOW_TEXT:
                        var ac = (ShowTextMissionAction) action;
                        textToShow.Add(new Tuple<string, object[], Vector2, int>(ac.GetId(), ac.GetVars(), ac.GetPosition(), ac.GetScale()));
                        break;
                    case MissionActionType.SHOW_PROXIMIAM:
                        MissionHandler.Action((ShowProximiamMissionAction) action);
                        break;
                    case MissionActionType.PUSH_MISSION:
                        missionToAdd.Add((PushMissionMissionAction) action);
                        break;
                    case MissionActionType.GIVE_GOLD:
                        MissionHandler.Action((GiveMoneyMissionAction) action);
                        break;
                    case MissionActionType.GIVE_SEED:
                        MissionHandler.Action((GiveSeedMissionAction) action);
                        break;
                    case MissionActionType.REDUCE_POLLUTION:
                        MissionHandler.Action((ReducePollutionMissionAction) action);
                        break;
                    case MissionActionType.FINISH_TUTO:
                        MissionHandler.Action((FinishTutoMissionAction) action);
                        break;
                }
            });
            missionDialog.SetUpDialog(textToShow, mission.getMissionType() == MissionType.TUTO);
            missionToAdd.ForEach(MissionHandler.Action);

            _eventManager.Raise(new MissionCollectedEvent().setMissionIndex(index));
            _gameData.AddTotalMissionDone(1);
        }
        catch (Exception exception)
        {
            Debug.LogError(exception);
//            _eventManager.Raise(new GameErrorEvent().SetMessageId(exception.Message).SetException(exception));
            //  Console.WriteLine(exception);
        }
    }

    private void Compute(AddRoadEvent e)
    {
        var road = new Road().SetPosition(e.getPosition()).SetRotation(e.getRotation()).SetRoadType(e.getRoadType())
            .SetNeighbours(e.getNeighbours());
        try
        {
            _gameData.AddRoad(road);
            _eventManager.Raise(new RoadAddedEvent().setPosition(e.getPosition())
                .setRoadType(e.getRoadType()).setRotation(e.getRotation()));
        }
        catch (GamevrestException exception)
        {
            _eventManager.Raise(new GameErrorEvent().SetMessageId(exception.Message).SetException(exception));
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
            throw;
        }
    }

    private void Compute(RemoveRoadEvent e)
    {
        try
        {
            var road = _gameData.GetRoadByPosition(e.getPosition());
            _gameData.RemoveRoad(road.GetPosition());
            _eventManager.Raise(new RoadRemovedEvent().setPosition(road.GetPosition()).setRotation(road.GetRotation())
                .setRoadType(road.GetRoadType()));
        }
        catch (GamevrestException exception)
        {
            _eventManager.Raise(new GameErrorEvent().SetMessageId(exception.Message).SetException(exception));
        }
        catch (Exception exception)
        {
            Console.WriteLine(exception);
            throw;
        }
    }

    private void Compute(UpdateRoadEvent e)
    {
        try
        {
            var oldRoad = _gameData.GetRoadByPosition(e.getPosition());
            _gameData.RemoveRoad(oldRoad.GetPosition());
            var road = new Road().SetPosition(e.getPosition()).SetRotation(e.getRotation()).SetRoadType(e.getRoadType())
                .SetNeighbours(e.getNeighbours());
            _gameData.AddRoad(road);
            _eventManager.Raise(new RoadUpdatedEvent().setPosition(road.GetPosition()).setRotation(road.GetRotation())
                .setRoadType(road.GetRoadType()));
        }
        catch (GamevrestException exception)
        {
            _eventManager.Raise(new GameErrorEvent().SetMessageId(exception.Message).SetException(exception));
        }
        catch (Exception exception)
        {
            Console.WriteLine(exception);
            throw;
        }
    }

    private void Compute(ResetDataEvent _)
    {
        _gameData = new Data();
        SaveData();
        _eventManager.Raise(new DataResetedEvent());
    }

    private void Compute(MoveBuildingEvent e)
    {
        BuildingsAbstract building = null;
        try
        {
            building = _gameData.GetBuildingByPosition(e.getOldPosition());
            _gameData.GetBuildingByPosition(e.getNewPosition());
            _eventManager.Raise(new GameErrorEvent().SetMessageId("ERROR_BUILDING_ATPOSITION")
                .SetException(new NotEmptyPositionException(e.getNewPosition())));
        }
        catch (EmptyPositionException exception)
        {
            if (exception.getPosition() == e.getOldPosition())
            {
                _eventManager.Raise(new GameErrorEvent().SetMessageId("ERROR_NOBUILDING_TOMOVE")
                    .SetException(exception));
            }
            else
            {
                if (building != null)
                {
                    building.setPosition(e.getNewPosition());
                    _gameData.UpdateEnergyZone(e.getOldPosition(), building.getEnergyZone(),
                        -building.getEnergyGenerate());
                    _gameData.UpdateEnergyZone(e.getNewPosition(), building.getEnergyZone(),
                        building.getEnergyGenerate());
                    _eventManager.Raise(new BuildingMovedEvent().setOldPosition(e.getOldPosition())
                        .setNewPosition(e.getNewPosition()));
                }
            }
        }
        catch (Exception exception)
        {
            Console.WriteLine(exception);
            throw;
        }
    }

    private void Compute(AddBuildingEvent e, bool noCost = false)
    {
        BuildingsAbstract newBuilding;

        switch (e.getBuildingType())
        {
            case BuildingType.HOUSE:
                newBuilding = BuildingFactory.generateNewBuilding<House>(e.getPosition());
                break;
            case BuildingType.ECONOMICAL_HABITATION:
                newBuilding = BuildingFactory.generateNewBuilding<EconomicalHabitation>(e.getPosition());
                break;
            case BuildingType.HABITATION:
                newBuilding = BuildingFactory.generateNewBuilding<Habitation>(e.getPosition());
                break;
            case BuildingType.ECOLOGICAL_HOUSE:
                newBuilding = BuildingFactory.generateNewBuilding<EcologicalHouse>(e.getPosition());
                break;
            case BuildingType.BUSINESS:
                newBuilding = BuildingFactory.generateNewBuilding<Business>(e.getPosition());
                break;
            case BuildingType.LARGE_ECONOMICAL_HABITATION:
                newBuilding = BuildingFactory.generateNewBuilding<LargeEconomicalHabitation>(e.getPosition());
                break;
            case BuildingType.BETTER_HABITATION:
                newBuilding = BuildingFactory.generateNewBuilding<BetterHabitation>(e.getPosition());
                break;
            case BuildingType.ECOLOGICAL_HABITATION:
                newBuilding = BuildingFactory.generateNewBuilding<EcologicalHabitation>(e.getPosition());
                break;
            case BuildingType.ECOLOGICAL_BUSINESS:
                newBuilding = BuildingFactory.generateNewBuilding<EcologicalBusiness>(e.getPosition());
                break;
            case BuildingType.FIRST_CLASS_BUSINESS:
                newBuilding = BuildingFactory.generateNewBuilding<FirstClassBusiness>(e.getPosition());
                break;
            case BuildingType.WIND_TURBINE:
                newBuilding = BuildingFactory.generateNewBuilding<WindTurbine>(e.getPosition());
                break;
            case BuildingType.WATER_TURBINE:
                newBuilding = BuildingFactory.generateNewBuilding<WaterTurbine>(e.getPosition());
                break;
            case BuildingType.SOLAR_PANELS:
                newBuilding = BuildingFactory.generateNewBuilding<SolarPanels>(e.getPosition());
                break;
            case BuildingType.NUCLEAR_POWER_PLANTS:
                newBuilding = BuildingFactory.generateNewBuilding<NuclearPowerPlant>(e.getPosition());
                break;
            case BuildingType.CHARCOAL_POWER_PLANTS:
                newBuilding = BuildingFactory.generateNewBuilding<CharcoalPowerPlant>(e.getPosition());
                break;
            case BuildingType.WATER_RECYCLING_STATION:
                newBuilding = BuildingFactory.generateNewBuilding<WaterRecyclingStation>(e.getPosition());
                break;
            case BuildingType.PLASTIC_RECYCLING_STATION:
                newBuilding = BuildingFactory.generateNewBuilding<PlasticRecyclingStation>(e.getPosition());
                break;
            case BuildingType.AIR_PURIFIER:
                newBuilding = BuildingFactory.generateNewBuilding<AirPurifier>(e.getPosition());
                break;
            default:
                throw new BuildingTypeNotFoundException(e.getBuildingType().ToString());
        }

        try
        {
            _gameData.AddBuilding(newBuilding, noCost);
            _eventManager.Raise(new BuildingAddedEvent().setForce(noCost).setPosition(e.getPosition())
                .setBuildingType(e.getBuildingType()));
        }
        catch (GamevrestException exception)
        {
            _eventManager.Raise(new GameErrorEvent().SetMessageId(exception.Message).SetException(exception));
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
            throw;
        }
    }

    private void Compute(UpgradeBuildingEvent e)
    {
        try
        {
            var oldBuilding = _gameData.GetBuildingByPosition(e.getPosition());
            BuildingsAbstract newBuilding;
            switch (e.getNewBuildingType())
            {
                case BuildingType.HOUSE:
                    newBuilding = BuildingFactory.upgradeBuilding<House>(oldBuilding);
                    break;
                case BuildingType.ECONOMICAL_HABITATION:
                    newBuilding = BuildingFactory.upgradeBuilding<EconomicalHabitation>(oldBuilding);
                    break;
                case BuildingType.HABITATION:
                    newBuilding = BuildingFactory.upgradeBuilding<Habitation>(oldBuilding);
                    break;
                case BuildingType.ECOLOGICAL_HOUSE:
                    newBuilding = BuildingFactory.upgradeBuilding<EcologicalHouse>(oldBuilding);
                    break;
                case BuildingType.BUSINESS:
                    newBuilding = BuildingFactory.upgradeBuilding<Business>(oldBuilding);
                    break;
                case BuildingType.LARGE_ECONOMICAL_HABITATION:
                    newBuilding = BuildingFactory.upgradeBuilding<LargeEconomicalHabitation>(oldBuilding);
                    break;
                case BuildingType.BETTER_HABITATION:
                    newBuilding = BuildingFactory.upgradeBuilding<BetterHabitation>(oldBuilding);
                    break;
                case BuildingType.ECOLOGICAL_HABITATION:
                    newBuilding = BuildingFactory.upgradeBuilding<EcologicalHabitation>(oldBuilding);
                    break;
                case BuildingType.ECOLOGICAL_BUSINESS:
                    newBuilding = BuildingFactory.upgradeBuilding<EcologicalBusiness>(oldBuilding);
                    break;
                case BuildingType.FIRST_CLASS_BUSINESS:
                    newBuilding = BuildingFactory.upgradeBuilding<FirstClassBusiness>(oldBuilding);
                    break;
                case BuildingType.WIND_TURBINE:
                    newBuilding = BuildingFactory.upgradeBuilding<WindTurbine>(oldBuilding);
                    break;
                case BuildingType.WATER_TURBINE:
                    newBuilding = BuildingFactory.upgradeBuilding<WaterTurbine>(oldBuilding);
                    break;
                case BuildingType.SOLAR_PANELS:
                    newBuilding = BuildingFactory.upgradeBuilding<SolarPanels>(oldBuilding);
                    break;
                case BuildingType.NUCLEAR_POWER_PLANTS:
                    newBuilding = BuildingFactory.upgradeBuilding<NuclearPowerPlant>(oldBuilding);
                    break;
                default:
                    throw new BuildingTypeNotFoundException(e.getNewBuildingType().ToString());
            }

            _gameData.ReplaceBuilding(e.getPosition(), newBuilding);
            _eventManager.Raise(new BuildingUpgradedEvent().setPosition(e.getPosition())
                .setOldBuildingType(e.getOldBuildingType()).setNewBuildingType(e.getNewBuildingType()));
        }
        catch (GamevrestException exception)
        {
            _eventManager.Raise(new GameErrorEvent().SetMessageId(exception.Message).SetException(exception));
        }
        catch (Exception exception)
        {
            Console.WriteLine(exception);
            throw;
        }
    }

    private void Compute(RenameBuildingEvent e)
    {
        try
        {
            _gameData.GetBuildingByPosition(e.getPosition()).setName(e.getBuildingName());
            _eventManager.Raise(new BuildingRenamedEvent().setPosition(e.getPosition())
                .setBuildingName(e.getBuildingName()));
        }
        catch (GamevrestException exception)
        {
            _eventManager.Raise(new GameErrorEvent().SetMessageId(exception.Message).SetException(exception));
        }
        catch (Exception exception)
        {
            Console.WriteLine(exception);
            throw;
        }
    }

    private void Compute(SellBuildingEvent e)
    {
        try
        {
            var building = _gameData.GetBuildingByPosition(e.getPosition());
            var refundMoney = building.getPrice() * MoneyRefundPercentage;
            _gameData.RemoveBuilding(building.getPosition());
            _gameData.AddMoney(Convert.ToInt64(refundMoney));
            _eventManager.Raise(new BuildingSoldEvent().setPosition(e.getPosition()));
        }
        catch (GamevrestException exception)
        {
            _eventManager.Raise(new GameErrorEvent().SetMessageId(exception.Message).SetException(exception));
        }
        catch (Exception exception)
        {
            Console.WriteLine(exception);
            throw;
        }
    }

    private void Compute(CollectMoneyEvent e)
    {
        try
        {
            var amount = _gameData.GetBuildingByPosition(e.getPosition()).collectMoney();
            _gameData.AddMoney(amount);
            _eventManager.Raise(new MoneyCollectedEvent().setAmount(amount).setPosition(e.getPosition()));
        }
        catch (Exception exception)
        {
            Console.WriteLine(exception);
            throw;
        }
    }

    private void Compute(LevelUpBuildingEvent e)
    {
        try
        {
            _gameData.GetBuildingByPosition(e.getPosition()).levelUp();
        }
        catch (Exception exception)
        {
            Console.WriteLine(exception);
            throw;
        }
    }

    private void Compute(LevelDownBuildingEvent e)
    {
        try
        {
            _gameData.GetBuildingByPosition(e.getPosition()).levelUp();
        }
        catch (Exception exception)
        {
            Console.WriteLine(exception);
            throw;
        }
    }

    public List<Road> GetRoad()
    {
        return _gameData.getRoads();
    }

    private void MissionEventHandler(GameEvent gameEvent)
    {
        bool needUpdate;
        switch (gameEvent.getEventType())
        {
            case GameEventType.BUILDING_ADDED:
                needUpdate = MissionHandler.Compute((BuildingAddedEvent) gameEvent);
                break;
            case GameEventType.BUILDING_UPGRADED:
                needUpdate = MissionHandler.Compute((BuildingUpgradedEvent) gameEvent);
                break;
            case GameEventType.MONEY_COLLECTED:
                needUpdate = MissionHandler.Compute((MoneyCollectedEvent) gameEvent);
                break;
            case GameEventType.SELL_BUILDING:
                needUpdate = MissionHandler.Compute((BuildingSoldEvent) gameEvent);
                break;
            case GameEventType.ROAD_ADDED:
                needUpdate = MissionHandler.Compute((RoadAddedEvent) gameEvent);
                break;
            case GameEventType.SWITCH_TO_2D:
                needUpdate = MissionHandler.Compute((SwitchTo2DEvent) gameEvent);
                break;
            case GameEventType.SWITCH_TO_AR:
                needUpdate = MissionHandler.Compute((SwitchToAREvent) gameEvent);
                break;
            case GameEventType.SWITCH_TO_GREENHOUSE:
                needUpdate = MissionHandler.Compute((SwitchToGreenHouseEvent) gameEvent);
                break;
            case GameEventType.MISSION_COLLECTED:
                needUpdate = MissionHandler.Compute((MissionCollectedEvent) gameEvent);
                break;
            case GameEventType.PLANT_COLLECTED:
                needUpdate = MissionHandler.Compute((PlantCollectedEvent) gameEvent);
                break;
            case GameEventType.SEED_PLANTED:
                needUpdate = MissionHandler.Compute((SeedPlantedEvent) gameEvent);
                break;

            default:
                return;
        }

        if (needUpdate)
        {
            _eventManager.Raise(new MissionUpdateEvent());
        }
    }

    public int CountBuildingType(BuildingType type)
    {
        var count = 0;
        foreach (var building in GetBuildings())
        {
            if (building.getBuildingType().Equals(type))
                count++;
        }

        return count;
    }

    public void AddSeedToGreenHouse(SeedType type, int amount)
    {
        _gameData.GetGreenHouse().addSeed(type, amount);
    }

    public bool GetGameExist()
    {
        return _gameData.GetGameExist();
    }

    public void SetGameExist()
    {
        _gameData.SetGameExist();
    }

    public GreenHouse GetGreenHouse()
    {
        return _gameData.GetGreenHouse();
    }

    public TownHall GetTownHall()
    {
        return _gameData.GetTownHall();
    }

    public WeatherState GetCurrentWeather()
    {
        return _weatherManager.GetCurrentWeather();
    }

    public List<WeatherState> GetWeatherSchedule()
    {
        return _gameData.GetWeatherSchedule();
    }
    
    public void SetWeatherSchedule(List<WeatherState> newSchedule)
    {
        _gameData.SetWeatherSchedule(newSchedule);
    }

    public DateTime GetNextWeatherChange()
    {
        return _gameData.GetNextWeatherChange();
    }
    public void SetNextWeatherChange(DateTime newNext)
    {
        _gameData.SetNextWeatherChange(newNext);
    }

    public float GetMoneyRefundPercentage() {
        return MoneyRefundPercentage;
    }


    public void AddBuilding(BuildingType buildingType, Vector2Int pos)
    {
        Compute(new AddBuildingEvent().setPosition(pos).setBuildingType(buildingType), true);
    }

#if DEBUG
    public void LoadDataDebug(string name)
    {
        _gameData = SaveManager.Load(name);
    }

    public void SaveDataDebug(string name)
    {
        SaveManager.saveGame(_gameData, name);
    }
#endif
    public byte[] GenerateCloudSave()
    {
        var data = SaveManager.saveGame(_gameData);
        return SaveManager.GenerateCloudSave(data);
    }

    public Data GenerateSaveFromCloudSave(byte[] data)
    {
        return SaveManager.GenerateSaveFromCloudSave(data);
    }
}