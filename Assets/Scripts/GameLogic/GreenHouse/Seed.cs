using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public enum SeedType
{
    BASIC,
    ADVANCED,
    EXPERIMENTAL,
    TWISTED,
}

public enum SeedState
{
    GERM,
    GROW,
    PLANT
}

[Serializable]
public class Seed
{
    private readonly SeedType _type;
    private List<SeedReward> _rewards;
    private readonly long _maturationTime;
    private DateTime _plantingTiming;
    private long _elapsedTime;
    private int _potIndex;

    protected Seed(SeedType type, long maturationTime)
    {
        _type = type;
        _maturationTime = maturationTime*60;
        _rewards = new List<SeedReward>();
    }

    public float getMaturationTime()
    {
        return _maturationTime;
    }
    
    public SeedType getSeedType()
    {
        return _type;
    }

    protected void addReward(SeedReward rewards)
    {
        _rewards.Add(rewards);
    }
    
    protected void setReward(List<SeedReward> rewards)
    {
        _rewards = rewards;
    }

    public List<SeedReward> getRewards()
    {
        return _rewards;
    }

    public void addElapsedTime(long amount)
    {
        if (_elapsedTime == _maturationTime) return;
        if (_elapsedTime + amount >= _maturationTime)
        {
            _elapsedTime = _maturationTime;
            EventManager.Instance.Raise(new SeedFinishGrowEvent().setPotIndex(_potIndex).setSeedType(_type));
        }
        else
        {
            _elapsedTime += amount;
        }
    }

    public void Plant(int potIndex)
    {
        _plantingTiming = DateTime.Now;
        _potIndex = potIndex;
    }

    public DateTime getPlantingTime()
    {
        return _plantingTiming;
    }

    public long getElapsedTime()
    {
        return _elapsedTime;
    }

    public long getRemainingTime()
    {
        return _maturationTime - _elapsedTime;
    }
    
    public bool isGrown()
    {
        return _elapsedTime == _maturationTime;
    }

    public SeedState GetState()
    {
        if (isGrown())
        {
            return SeedState.PLANT;
        }
        if (_elapsedTime >= _maturationTime / 2)
        {
            return SeedState.GROW;
        }

        return SeedState.GERM;
    }
    
    public Seed Clone()
    {
        using (var ms = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            formatter.Serialize(ms, this);
            ms.Position = 0;

            return (Seed) formatter.Deserialize(ms);
        }
    }
}