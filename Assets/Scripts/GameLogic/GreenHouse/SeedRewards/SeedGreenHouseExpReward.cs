using System;

[Serializable]
public class SeedGreenHouseExpReward : SeedReward
{
    private readonly long _exp;
    private bool _variable;

    public SeedGreenHouseExpReward(long exp, bool variable = false) : base(SeedRewardType.GREENHOUSE_EXP)
    {
        _exp = exp;
        _variable = variable;
    }

    public long GetExp()
    {
        return _exp;
    }
    
    public bool IsVariable()
    {
        return _variable;
    }
}