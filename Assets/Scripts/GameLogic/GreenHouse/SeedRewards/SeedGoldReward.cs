using System;

[Serializable]
public class SeedGoldReward : SeedReward
{
    private readonly int _gold;
    private bool _variable;
    
    public SeedGoldReward(int gold, bool variable = false) : base(SeedRewardType.GOLD)
    {
        _gold = gold;
        _variable = variable;
    }

    public int GetGold()
    {
        return _gold;
    }

    public bool IsVariable()
    {
        return _variable;
    }
}