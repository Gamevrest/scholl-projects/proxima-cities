using System;

[Serializable]
public class SeedReducePolutionReward : SeedReward
{
    private readonly int _amount;
    private bool _variable;
    
    public SeedReducePolutionReward(int amount, bool variable = false) : base(SeedRewardType.REDUCE_POLUTION)
    {
        _amount = amount;
        _variable = variable;
    }

    public int GetAmount()
    {
        return _amount;
    }

    public bool IsVariable()
    {
        return _variable;
    }
}