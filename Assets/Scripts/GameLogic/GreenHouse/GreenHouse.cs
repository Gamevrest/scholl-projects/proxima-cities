using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

[Serializable]
public class GreenHouse
{
    private Dictionary<SeedType, int> _seedStorage = new Dictionary<SeedType, int>();
    private readonly Dictionary<int, Seed> _pots = new Dictionary<int, Seed>();
    [NonSerialized]
    private EventManager _eventManager = EventManager.Instance;
    
    [OnDeserialized]
    private void OnDeserialized()
    {
        _eventManager = EventManager.Instance;
    }

    public GreenHouse()
    {
        _pots.Add(0, null);
        _pots.Add(1, null);
        _pots.Add(2, null);
    }
    
    public Seed GetPotAtIndex(int index)
    {
        return _pots[index];
    }

    public Dictionary<SeedType, int> GetSeedStorage()
    {
        return _seedStorage;
    }

    public void addSeed(SeedType type, int amount)
    {
        if (_seedStorage.ContainsKey(type))
        {
            _seedStorage[type] += amount;
        }
        else
        {
            _seedStorage[type] = amount;
        }
    }

    public void greenHouseLoop(bool perSecond = true)
    {
        foreach (var pot in _pots)
        {
            pot.Value?.addElapsedTime(perSecond ? 1 : 60);
        }
    }
    
    public void greenHouseOfflineGain(long time, bool perSecond = true)
    {
        foreach (var pot in _pots)
        {
            pot.Value?.addElapsedTime(perSecond ? time/60 : time);
        }
    }

    public void plantInPot(int potIndex, SeedType type)
    {
        if (!_pots.ContainsKey(potIndex)) return;
        if (_pots[potIndex] != null) return;
        if(_seedStorage[type] == 0)
            throw new NotEnoughSeedException(type);

        _seedStorage[type]--;
        _pots[potIndex] = GameManager.AllSeeds[type].Clone();
        _pots[potIndex].Plant(potIndex);
        EventManager.Instance.Raise(new SeedPlantedEvent().setPotIndex(potIndex).setSeedType(type));
    }
    
    public void collectPlant(CollectPlantEvent e)
    {
        var index1 = e.getPotIndex();
        var index2 = e.getSecondPotIndex();
        if (index1 != -1 && index2 != -1)
        {
            if (_pots[index1] == null) 
                throw new EmptyPotException(index1);  
            if (_pots[index2] == null) 
                throw new EmptyPotException(index2);

            if (_pots[index1].isGrown() && _pots[index2].isGrown())
            {
                _pots[index1].getRewards().ForEach(reward =>
                {
                    switch (reward.GetSeedRewardType())
                    {
                        case SeedRewardType.GOLD:
                            compute((SeedGoldReward) reward);
                            break;
                        case SeedRewardType.GREENHOUSE_EXP:
                            compute((SeedGreenHouseExpReward) reward);
                            break;
                        case SeedRewardType.REDUCE_POLUTION:
                            compute((SeedReducePolutionReward) reward);

                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                });
                
                _pots[index2].getRewards().ForEach(reward =>
                {
                    switch (reward.GetSeedRewardType())
                    {
                        case SeedRewardType.GOLD:
                            compute((SeedGoldReward) reward);
                            break;
                        case SeedRewardType.GREENHOUSE_EXP:
                            compute((SeedGreenHouseExpReward) reward);
                            break;
                        case SeedRewardType.REDUCE_POLUTION:
                            compute((SeedReducePolutionReward) reward);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                });
                _pots[index1] = null;
                _pots[index2] = null;
                EventManager.Instance.Raise(new PlantCollectedEvent().setPotIndex(index1).setSecondPotIndex(index2));
            }
        } 
        else if (index1 != -1 && index2 == -1)
        {
            if (_pots[index1] == null) 
                throw new EmptyPotException(index1);

            if (_pots.ContainsKey(index1 + 1) && _pots[index1 + 1] != null ||
                _pots.ContainsKey(index1 - 1) && _pots[index1 - 1] != null)
                throw new NotEmptyPotException();

            if (_pots[index1].isGrown())
            {
                _pots[index1].getRewards().ForEach(reward =>
                {
                    switch (reward.GetSeedRewardType())
                    {
                        case SeedRewardType.GOLD:
                            compute((SeedGoldReward) reward);
                            break;
                        case SeedRewardType.GREENHOUSE_EXP:
                            compute((SeedGreenHouseExpReward) reward);
                            break;
                        case SeedRewardType.REDUCE_POLUTION:
                            compute((SeedReducePolutionReward) reward);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                });
                _pots[index1] = null;
                EventManager.Instance.Raise(new PlantCollectedEvent().setPotIndex(index1).setSecondPotIndex(index2));
            }
        }
    }

    private void compute(SeedReducePolutionReward reward)
    {
        var amount = reward.GetAmount();
        if (reward.IsVariable())
            amount *= GameManager.Instance.GetTownHall().GetLevel();
        if (reward.IsBoosted())
            amount *= 5;
        
        GameManager.Instance.ReducePollution(amount);
    }

    private void compute(SeedGreenHouseExpReward reward)
    {
        var amount = reward.GetExp();
        if (reward.IsVariable())
            amount *= GameManager.Instance.GetTownHall().GetLevel();
        if (reward.IsBoosted())
            amount *= 5;
        _eventManager.Raise(new XPGainedEvent().setXP(amount));
    }

    private void compute(SeedGoldReward reward)
    {
        var amount = reward.GetGold();
        if (reward.IsVariable())
            amount *= GameManager.Instance.GetTownHall().GetLevel();
        if (reward.IsBoosted())
            amount *= 5;

        GameManager.Instance.AddMoney(amount);
    }
}