using System;

[Serializable]
public class SeedAdvanced : Seed
{
    public SeedAdvanced() : base(SeedType.ADVANCED, 60)
    {
        addReward(new SeedReducePolutionReward(5));
        addReward(new SeedReducePolutionReward(1, true));
    }
}