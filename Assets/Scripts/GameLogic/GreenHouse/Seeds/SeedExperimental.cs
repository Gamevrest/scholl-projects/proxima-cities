using System;

[Serializable]
public class SeedExperimental : Seed
{
    public SeedExperimental() : base(SeedType.EXPERIMENTAL, 240)
    {
        addReward(new SeedGreenHouseExpReward(100));
        addReward(new SeedGreenHouseExpReward(40, true));
    }
}