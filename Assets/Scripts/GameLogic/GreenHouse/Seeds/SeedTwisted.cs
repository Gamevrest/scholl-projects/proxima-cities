using System;

[Serializable]
public class SeedTwisted : Seed
{
    public SeedTwisted() : base(SeedType.TWISTED, 1440)
    {
        addReward(new SeedGoldReward(100, true));
    }
}