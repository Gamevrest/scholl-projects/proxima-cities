using System;
using UnityEngine;

public static class MissionHandler
{
    public static bool Compute(BuildingAddedEvent e)
    {
        var needUpdate = false;
        var missions = GameManager.Instance.GetCurrentMissions();
        foreach (var mission in missions)
        {
            foreach (var requirement in mission.getMissionRequirements())
            {
                if (requirement.getMissionRequirementType().Equals(MissionRequirementType.BUILDING_OWNED))
                {
                    var req = (BuildingOwnedMissionRequirement)requirement;
                    if (!req.getBuildingType().Equals(e.getBuildingType()) || requirement.isAchieved()) continue;
                    requirement.addToCurrentCount(1);
                    needUpdate = true;
                } else if (requirement.getMissionRequirementType().Equals(MissionRequirementType.ADD_BUILDING))
                {
                    var req = (AddBuildingMissionRequirement)requirement;
                    if (!req.getBuildingType().Equals(e.getBuildingType()) || requirement.isAchieved()) continue;
                    requirement.addToCurrentCount(1);
                    needUpdate = true;
                }
                else if (requirement.getMissionRequirementType().Equals(MissionRequirementType.POPULATION))
                {
                    if (requirement.isAchieved()) continue;
                    requirement.addToCurrentCount(GameManager.AllBuildings[e.getBuildingType()].getCitizens());
                    needUpdate = true;
                }
            }
        }
        return needUpdate;
    }
    public static bool Compute(BuildingUpgradedEvent e)
    {
        var needUpdate = false;
        var missions = GameManager.Instance.GetCurrentMissions();
        foreach (var mission in missions)
        {
            foreach (var requirement in mission.getMissionRequirements())
            {
                if (requirement.getMissionRequirementType().Equals(MissionRequirementType.ADD_BUILDING) || requirement.getMissionRequirementType().Equals(MissionRequirementType.BUILDING_OWNED))
                {
                    var req = (BuildingOwnedMissionRequirement)requirement;
                    if (!req.getBuildingType().Equals(e.getNewBuildingType()) || requirement.isAchieved()) continue;
                    requirement.addToCurrentCount(1);
                    needUpdate = true;
                }else if (requirement.getMissionRequirementType().Equals(MissionRequirementType.ADD_BUILDING))
                {
                    var req = (AddBuildingMissionRequirement)requirement;
                    if (!req.getBuildingType().Equals(e.getNewBuildingType()) || requirement.isAchieved()) continue;
                    requirement.addToCurrentCount(1);
                    needUpdate = true;
                }
                else if (requirement.getMissionRequirementType().Equals(MissionRequirementType.POPULATION))
                {
                    if (requirement.isAchieved()) continue;
                    requirement.addToCurrentCount(GameManager.AllBuildings[e.getNewBuildingType()].getCitizens() - GameManager.AllBuildings[e.getOldBuildingType()].getCitizens());
                    needUpdate = true;
                }
            }
        }
        return needUpdate;
    }
    public static bool Compute(MoneyCollectedEvent e)
    {
        var needUpdate = false;
        var missions = GameManager.Instance.GetCurrentMissions();
        foreach (var mission in missions)
        {
            foreach (var requirement in mission.getMissionRequirements())
            {
                if (!requirement.getMissionRequirementType().Equals(MissionRequirementType.EARN_MONEY) && !requirement.getMissionRequirementType().Equals(MissionRequirementType.TOTAL_MONEY)) continue;
                if (requirement.isAchieved()) continue;

                requirement.addToCurrentCount(e.getAmount());
                needUpdate = true;
            }
        }
        return needUpdate;
    }
    public static bool Compute(BuildingSoldEvent e)
    {
        var needUpdate = false;
        var missions = GameManager.Instance.GetCurrentMissions();
        var building = GameManager.Instance.GetBuildingByPosition(e.getPosition());
        foreach (var mission in missions)
        {
            foreach (var requirement in mission.getMissionRequirements())
            {
                if (requirement.getMissionRequirementType().Equals(MissionRequirementType.ADD_BUILDING) || requirement.getMissionRequirementType().Equals(MissionRequirementType.BUILDING_OWNED))
                {
                    var req = (BuildingOwnedMissionRequirement)requirement;
                    if (!req.getBuildingType().Equals(building.getBuildingType()) || requirement.isAchieved()) continue;
                    requirement.addToCurrentCount(-1);
                    needUpdate = true;
                }
                else if (requirement.getMissionRequirementType().Equals(MissionRequirementType.POPULATION))
                {
                    if (requirement.isAchieved()) continue;
                    requirement.addToCurrentCount(-building.getCitizens());
                    needUpdate = true;
                }
            }
        }
        return needUpdate;
    }
    public static bool Compute(RoadAddedEvent e)
    {
        var needUpdate = false;
        var missions = GameManager.Instance.GetCurrentMissions();
        foreach (var mission in missions)
        {
            foreach (var requirement in mission.getMissionRequirements())
            {
                if (!requirement.getMissionRequirementType().Equals(MissionRequirementType.ADD_ROAD)) continue;
                if (requirement.isAchieved()) continue;
                requirement.addToCurrentCount(1);
                needUpdate = true;
            }
        }
        return needUpdate;
    }
    public static bool Compute(SwitchTo2DEvent e)
    {
        var needUpdate = false;
        var missions = GameManager.Instance.GetCurrentMissions();
        foreach (var mission in missions)
        {
            foreach (var requirement in mission.getMissionRequirements())
            {
                if (!requirement.getMissionRequirementType().Equals(MissionRequirementType.GO_TO_2D)) continue;
                if (requirement.isAchieved()) continue;
                requirement.setCompleted();
                needUpdate = true;
            }
        }
        return needUpdate;
    }
    public static bool Compute(SwitchToAREvent e)
    {
        var needUpdate = false;
        var missions = GameManager.Instance.GetCurrentMissions();
        foreach (var mission in missions)
        {
            foreach (var requirement in mission.getMissionRequirements())
            {
                if (!requirement.getMissionRequirementType().Equals(MissionRequirementType.GO_TO_AR)) continue;
                if (requirement.isAchieved()) continue;
                requirement.setCompleted();
                needUpdate = true;

            }
        }
        return needUpdate;
    }
    
    public static bool Compute(SwitchToGreenHouseEvent e)
    {
        var needUpdate = false;
        var missions = GameManager.Instance.GetCurrentMissions();
        foreach (var mission in missions)
        {
            foreach (var requirement in mission.getMissionRequirements())
            {
                if (!requirement.getMissionRequirementType().Equals(MissionRequirementType.GO_TO_GREENHOUSE)) continue;
                if (requirement.isAchieved()) continue;
                requirement.setCompleted();
                needUpdate = true;

            }
        }
        return needUpdate;
    }

    public static bool Compute(MissionCollectedEvent e)
    {
        var needUpdate = false;
        var missions = GameManager.Instance.GetCurrentMissions();
        foreach (var mission in missions)
        {
            foreach (var requirement in mission.getMissionRequirements())
            {
                if (!requirement.getMissionRequirementType().Equals(MissionRequirementType.MISSION_DONE_COUNT)) continue;
                if (requirement.isAchieved()) continue;
                requirement.addToCurrentCount(1);
                needUpdate = true;
            }
        }
        return needUpdate;
    }
    
    public static bool Compute(SeedPlantedEvent e)
    {
        var needUpdate = false;
        var missions = GameManager.Instance.GetCurrentMissions();
        foreach (var mission in missions)
        {
            foreach (var requirement in mission.getMissionRequirements())
            {
                if (!requirement.getMissionRequirementType().Equals(MissionRequirementType.PLANT_SEED)) continue;
                if (requirement.isAchieved()) continue;
                requirement.addToCurrentCount(1);
                needUpdate = true;
            }
        }   
        return needUpdate;
    }
    
    public static bool Compute(PlantCollectedEvent e)
    {
        var needUpdate = false;
        var missions = GameManager.Instance.GetCurrentMissions();
        foreach (var mission in missions)
        {
            foreach (var requirement in mission.getMissionRequirements())
            {
                if (!requirement.getMissionRequirementType().Equals(MissionRequirementType.GROW_PLANT)) continue;
                if (requirement.isAchieved()) continue;
                requirement.addToCurrentCount(1);
                needUpdate = true;
            }
        }
        return needUpdate;
    }

    public static void Action(ShowProximiamMissionAction action)
    {
        throw new NotImplementedException();
    }

    public static void Action(GiveMoneyMissionAction action)
    {
        GameManager.Instance.AddMoney(action.GetMoneyCount());
    }

    public static void Action(ReducePollutionMissionAction action)
    {
        GameManager.Instance.ReducePollution(action.GetPollutionCount());
    }

    public static void Action(FinishTutoMissionAction action)
    {
        GameManager.Instance.FinishTuto();
    }

    public static void Action(PushMissionMissionAction action)
    {
        GameManager.Instance.AddMission(GameManager.missionList[action.getMissionName()].Clone());
        EventManager.Instance.Raise(new StartMissionEvent().setMissionIndex(GameManager.Instance.GetCurrentMissions().Count - 1));
    }

    public static void Action(GiveSeedMissionAction action)
    {
        GameManager.Instance.AddSeedToGreenHouse(action.getSeedType(), action.getAmount());
    }

    public static void Action(AddBuildingMissionAction action)
    {
        GameManager.Instance.AddBuilding(action.GetBuildingType(), action.GetPosition());
    }
}