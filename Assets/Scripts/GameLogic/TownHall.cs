using System;
using System.Net;

[Serializable]
public class TownHall
{
    private string _cityName = "Nancy";
    private int _level = 1;
    private long _experience;
    
    public void SetCityName(string newName)
    {
        _cityName = newName;
    }

    public string GetCityName()
    {
        return _cityName;
    }

    public int GetLevel()
    {
        return _level;
    }
    public long GetExperienceToNextLevel()
    {
        var exp = 100;
        for (var i = 1; i <= _level; i++)
        {
            exp += exp * 50 / 100;
        }
        
        return exp;
    }
    public long GetExperience()
    {
        return _experience;
    }

    public void AddExperience(long amount)
    {
        _experience += amount;
        if (_experience >= GetExperienceToNextLevel())
        {
            _level++;
        }
    }

    public int GetNumberOfBuildingWithoutFullEnergy()
    {
        var gameManager = GameManager.Instance;
        var buildingCount = 0;
        gameManager.GetBuildings().ForEach(building =>
        {
            var generated = gameManager.GetEnergyByPosition(building.getPosition());
            var needed = building.getEnergyNeeded();
            if (needed == 0)return;
            if (generated == 0)
            {
                buildingCount++;
                return;
            }
            if ((float)(Math.Truncate((double)generated / needed * 100) / 100.0) < 1.0f)
            {
                buildingCount++;
            }
        });
        return buildingCount;
    }

    public long GetTotalMoneyPerHour()
    {
        var gameManager = GameManager.Instance;
        long money = 0;
        gameManager.GetBuildings().ForEach(building =>
        {
            var generated = gameManager.GetEnergyByPosition(building.getPosition());
            var needed = building.getEnergyNeeded();
            float percentage;
            if (needed == 0 || generated == 0)
            {
                percentage = 0.1f;
            }
            else
            {
                percentage = (float)(Math.Truncate((double)generated / needed * 100) / 100.0);
            }
            if (percentage < 0.1f)
                percentage = 0.1f;
            else if (percentage > 1)
                percentage = 1;
            money += building.GetMoneyGeneratedOverHour(percentage);
        });
        return money;
    }
}