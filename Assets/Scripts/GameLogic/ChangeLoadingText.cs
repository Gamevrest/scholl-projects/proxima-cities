﻿using System;
using System.Collections;
using TMPro;
using Translation;
using UnityEngine;
using UnityEngine.UI;

public class ChangeLoadingText : MonoBehaviour
{
    public TextMeshProUGUI loadingText;
    public float delay = 1;
    [ReadOnly] public int i;
    public string[] anim;
    private AutoSetter _setter;

    private void Awake()
    {
        if (!loadingText) loadingText = GetComponent<TextMeshProUGUI>();
        _setter = new AutoSetter("TITLE_LOADING", loadingText, anim[0]);
    }

    private void OnEnable()
    {
        StartCoroutine(ChangeAnim());
    }

    private void OnDisable()
    {
        StopCoroutine(ChangeAnim());
    }

    private IEnumerator ChangeAnim()
    {
        _setter.SetVars(anim[i]);
        yield return new WaitForSeconds(delay);
        i++;
        if (i >= anim.Length) i = 0;
        yield return ChangeAnim();
    }
}