using System;
using System.Collections.Generic;
using Buildings;
using GameLogic.Weather;
using TMPro;
using UnityEngine;

[Serializable]
public class Data
{
    private List<BuildingsAbstract> _buildings = new List<BuildingsAbstract>
    {
        new GreenHouseBuilding(),
        new TownHallBuilding()
    };
    private readonly List<BuildingData> _buildingDatas = new List<BuildingData>();
    private List<Road> _roads = new List<Road>();
    private long _money;
    private ulong _totalMoneyOverTime;
    private long _citizens;
    private long _pollution;
    private long _hapiness;
    private bool _tutoDone;
    private bool _gameExist;
    private long _totalMissionDone;
    private List<Mission> _currentMission = new List<Mission>();
    private Dictionary<Vector2Int, long> _energyZone = new Dictionary<Vector2Int, long>();
    private GreenHouse _greenHouse = new GreenHouse();
    private TownHall _townHall = new TownHall();
    private DateTime? _saveDate;
    private List<WeatherState> _weatherschedule = new List<WeatherState>();
    private DateTime _nextWeatherChange = DateTime.Now;

    public long GetMoney()
    {
        return _money;
    }

    public long AddMoney(long amount)
    {
        _money += amount;
        _totalMoneyOverTime += (ulong)amount;
        return _money;
    }

    public long removeMoney(long amount)
    {
        _money -= amount;
        return _money;
    }

    public long setMoney(long amount)
    {
        _money = amount;
        return _money;
    }
    
    public long GetCitizens()
    {
        return _citizens;
    }

    public long AddCitizens(long amount)
    {
        _citizens += amount;
        return _citizens;
    }

    public long removeCitizens(long amount)
    {
        _citizens -= amount;
        return _citizens;
    }

    public long setCitizens(long amount)
    {
        _citizens = amount;
        return _citizens;
    }
    
    public long GetPollution()
    {
        return _pollution;
    }

    public long addPollution(long amount)
    {
        _pollution += amount;
        return _pollution;
    }
    
    public long RemovePollution(long amount)
    {
        _pollution -= amount;
        return _pollution;
    }

    public long setPollution(long amount)
    {
        _pollution = amount;
        return _pollution;
    }

    public long GetHapiness()
    {
        return _hapiness;
    }

    public long addHapiness(long amount)
    {
        _hapiness += amount;
        return _hapiness;
    }
    
    public long removeHapiness(long amount)
    {
        _hapiness -= amount;
        return _hapiness;
    }

    public long setHapiness(long amount)
    {
        _hapiness = amount;
        return _hapiness;
    }

    public long GetTotalMissionDone()
    {
        return _totalMissionDone;
    }

    public long AddTotalMissionDone(long amount)
    {
        _totalMissionDone += amount;
        return _totalMissionDone;
    }

    public List<BuildingsAbstract> GetBuildings()
    {
        return _buildings;
    }

    public void UpdateEnergyZone(Vector2Int bpos, List<Vector2Int> energyPos, long energy)
    {
        energyPos.ForEach(pos =>
        {
            var newPos = bpos + pos;
            Debug.Log($"[{newPos}] => {energy}");
            if (_energyZone.ContainsKey(newPos))
            {
                _energyZone[newPos] += energy;
            }
            else
            {
                _energyZone[newPos] = energy;
            }
        });
    }

    public void AddBuilding(BuildingsAbstract newBuilding, bool noCost = false)
    {
        try
        {
            GetBuildingByPosition(newBuilding.getPosition());
        }
        catch (GamevrestException ex)
        {
            if (!noCost)
            {
                var price = newBuilding.getPrice();
                if (_money < price)
                {
                    throw new NotEnoughMoneyException();
                }

                _money -= newBuilding.getPrice();
            }

            _citizens += newBuilding.getCitizens();
            UpdateEnergyZone(newBuilding.getPosition(), newBuilding.getEnergyZone(), newBuilding.getEnergyGenerate());
            _pollution += newBuilding.getPollution();
            _buildings.Add(newBuilding);
            Debug.Log(newBuilding.ToString());
            return;
        }
        throw new NotEmptyPositionException(newBuilding.getPosition());
    }
    
    public void ReplaceBuilding(Vector2Int position, BuildingsAbstract newBuidling)
    {
        
        foreach (var building in _buildings)
        {
            if (building.getPosition().Equals(position))
            {
                var price = newBuidling.getPrice();
                if (_money < price)
                {
                    throw new NotEnoughMoneyException();
                }
                newBuidling.setMoneyToCollect(building.getMoneyToCollect());
                _money -= newBuidling.getPrice();
                _citizens -= building.getCitizens();
                UpdateEnergyZone(building.getPosition(), building.getEnergyZone(), -building.getEnergyGenerate());
                _pollution -= building.getPollution();
                _buildings.Remove(building);
                _citizens += newBuidling.getCitizens();
                UpdateEnergyZone(newBuidling.getPosition(), newBuidling.getEnergyZone(), newBuidling.getEnergyGenerate());
                _pollution += newBuidling.getPollution();
                _buildings.Add(newBuidling);
                return;
            }
                
        }
        throw new EmptyPositionException(position);
    }
    
    public void RemoveBuilding(Vector2Int position)
    {
        foreach (var building in _buildings)
        {
            if (building.getPosition().Equals(position))
            {
                _citizens -= building.getCitizens();
                UpdateEnergyZone(building.getPosition(),building.getEnergyZone(), -building.getEnergyGenerate());
                _pollution -= building.getPollution();
                _buildings.Remove(building);
                return;
            }
                
        }
        throw new EmptyPositionException(position);
    }

    public BuildingsAbstract GetBuildingByPosition(Vector2Int position)
    {
        foreach (var building in _buildings)
        {
            if (building.getPosition().Equals(position))
                return building;
        }
        throw new EmptyPositionException(position);
    }
    
    public List<Road> getRoads()
    {
        return _roads;
    }

    public void AddRoad(Road newRoad)
    {
        try
        {
            GetRoadByPosition(newRoad.GetPosition());
        }
        catch (GamevrestException)
        {
            _roads.Add(newRoad);
            return;
        }
        throw new NotEmptyPositionException(newRoad.GetPosition());
    }
    
    public void RemoveRoad(Vector2Int position)
    {
        foreach (var road in _roads)
        {
            if (road.GetPosition().Equals(position))
            {
                _roads.Remove(road);
                return;
            }
                
        }
        throw new EmptyPositionException(position);
    }
    
    public Road GetRoadByPosition(Vector2Int position, bool shouldThrow = true)
    {
        foreach (var road in _roads)
        {
            if (road.GetPosition().Equals(position))
                return road;
        }

        if (shouldThrow)
            throw new EmptyPositionException(position);
        return null;
    }

    public List<Mission> GetCurrentMissions()
    {
        return _currentMission;
    }

    public void AddMission(Mission mission)
    {
        _currentMission.Add(mission);
    }

    public bool IsTutoDone()
    {
        return _tutoDone;
    }
    
    public void FinishTuto()
    {
        _tutoDone = true;
    }

    public long GetEnergyByPosition(Vector2Int pos, bool needThrow = false)
    {
        try
        {
            return _energyZone[pos];
        }
        catch (Exception)
        {
            if (needThrow)
                throw new NoEnergyAtPositionException(pos);
        }
        return 0;
    }

    public GreenHouse GetGreenHouse()
    {
        return _greenHouse;
    }
    public void GreenHouseLoop(bool perSecond = true)
    {
        _greenHouse.greenHouseLoop(perSecond);
    }

    public void Plant(int potIndex, SeedType type)
    {
        _greenHouse.plantInPot(potIndex, type);
    }

    public TownHall GetTownHall()
    {
        return _townHall;
    }

    public void SetGameExist()
    {
        _gameExist = true;
    }

    public bool GetGameExist()
    {
        return _gameExist;
    }

    private Dictionary<Vector2Int, long> GetEnergyZone()
    {
        return _energyZone;
    }

    private List<BuildingData> GetBuildingData()
    {
        return _buildingDatas;
    }

    public List<WeatherState> GetWeatherSchedule()
    {
        return _weatherschedule;
    }
    
    public void SetWeatherSchedule(List<WeatherState> newSchedule)
    {
        _weatherschedule = newSchedule;
    }

    public DateTime GetNextWeatherChange()
    {
        return _nextWeatherChange;
    }
    public void SetNextWeatherChange(DateTime newDate)
    {
        _nextWeatherChange = newDate;
    }
    
    

    public double GetOfflineTime()
    {
        return (DateTime.Now - _saveDate).Value.TotalMinutes;
    }

    public void CreateSaveFromData(Data data)
    {
        data.GetBuildings().ForEach(building =>
        {
            _buildingDatas.Add(building.getBuildingData());
        });
        _buildings.Clear();
        _roads = data._roads;
        _money = data._money;
        _weatherschedule = data._weatherschedule;
        Debug.Log(_weatherschedule.Count);
        _nextWeatherChange = data._nextWeatherChange;
        _citizens = data._citizens;
        _pollution = data._pollution;
        _hapiness = data._hapiness;
        _tutoDone = data._tutoDone;
        _gameExist = data._gameExist;
        _totalMissionDone = data._totalMissionDone;
        _totalMoneyOverTime = data._totalMoneyOverTime;
        _currentMission = data._currentMission;
        _energyZone = data._energyZone;
        _greenHouse = data._greenHouse;
        _townHall = data._townHall;
        _saveDate = DateTime.Now;
    }

    private List<BuildingsAbstract> generateBuildingList(Data save)
    {
        var newList = new List<BuildingsAbstract>();
        save.GetBuildingData().ForEach(data =>
        {
            BuildingsAbstract newBuilding;
            switch (data.BuildingType)
            {
                case BuildingType.HOUSE:
                    newBuilding = BuildingFactory.generateNewBuilding<House>();
                    break;
                case BuildingType.ECONOMICAL_HABITATION:
                    newBuilding = BuildingFactory.generateNewBuilding<EconomicalHabitation>();
                    break;
                case BuildingType.HABITATION:
                    newBuilding = BuildingFactory.generateNewBuilding<Habitation>();
                    break;
                case BuildingType.ECOLOGICAL_HOUSE:
                    newBuilding = BuildingFactory.generateNewBuilding<EcologicalHouse>();
                    break;
                case BuildingType.BUSINESS:
                    newBuilding = BuildingFactory.generateNewBuilding<Business>();
                    break;
                case BuildingType.LARGE_ECONOMICAL_HABITATION:
                    newBuilding = BuildingFactory.generateNewBuilding<LargeEconomicalHabitation>();
                    break;
                case BuildingType.BETTER_HABITATION:
                    newBuilding = BuildingFactory.generateNewBuilding<BetterHabitation>();
                    break;
                case BuildingType.ECOLOGICAL_HABITATION:
                    newBuilding = BuildingFactory.generateNewBuilding<EcologicalHabitation>();
                    break;
                case BuildingType.ECOLOGICAL_BUSINESS:
                    newBuilding = BuildingFactory.generateNewBuilding<EcologicalBusiness>();
                    break;
                case BuildingType.FIRST_CLASS_BUSINESS:
                    newBuilding = BuildingFactory.generateNewBuilding<FirstClassBusiness>();
                    break;
                case BuildingType.WIND_TURBINE:
                    newBuilding = BuildingFactory.generateNewBuilding<WindTurbine>();
                    break;
                case BuildingType.WATER_TURBINE:
                    newBuilding = BuildingFactory.generateNewBuilding<WaterTurbine>();
                    break;
                case BuildingType.SOLAR_PANELS:
                    newBuilding = BuildingFactory.generateNewBuilding<SolarPanels>();
                    break;
                case BuildingType.NUCLEAR_POWER_PLANTS:
                    newBuilding = BuildingFactory.generateNewBuilding<NuclearPowerPlant>();
                    break;
                case BuildingType.CHARCOAL_POWER_PLANTS:
                    newBuilding = BuildingFactory.generateNewBuilding<CharcoalPowerPlant>();
                    break;
                case BuildingType.WATER_RECYCLING_STATION:
                    newBuilding = BuildingFactory.generateNewBuilding<WaterRecyclingStation>();
                    break;
                case BuildingType.PLASTIC_RECYCLING_STATION:
                    newBuilding = BuildingFactory.generateNewBuilding<PlasticRecyclingStation>();
                    break;
                case BuildingType.AIR_PURIFIER:
                    newBuilding = BuildingFactory.generateNewBuilding<AirPurifier>();
                    break;
                case BuildingType.GREEN_HOUSE:
                    newBuilding = BuildingFactory.generateNewBuilding<GreenHouseBuilding>();
                    break;
                case BuildingType.TOWN_HALL:
                    newBuilding = BuildingFactory.generateNewBuilding<TownHallBuilding>();
                    break;
                default:
                    throw new BuildingTypeNotFoundException(data.BuildingType.ToString());
            }
            newBuilding.setBuildingData(data);
            newList.Add(newBuilding);
        });
        return newList;
    }

    public void CreateDataFromSave(Data save)
    {
        _buildings.Clear();
        _buildings = generateBuildingList(save);
        _roads = save._roads ?? new List<Road>();
        _money = save._money;
        _weatherschedule = save._weatherschedule ?? new List<WeatherState>();
        Debug.Log(_weatherschedule.Count);
        _nextWeatherChange = save._nextWeatherChange;
        _citizens = save._citizens;
        _pollution = save._pollution;
        _hapiness = save._hapiness;
        _tutoDone = save._tutoDone;
        _gameExist = save._gameExist;
        _totalMissionDone = save._totalMissionDone;
        _totalMoneyOverTime = save._totalMoneyOverTime;
        _currentMission = save._currentMission ?? new List<Mission>();
        _energyZone = save._energyZone ?? new Dictionary<Vector2Int, long>();
        _greenHouse = save._greenHouse ?? new GreenHouse();
        _townHall = save._townHall ?? new TownHall();
        _saveDate = save._saveDate ?? DateTime.Now;
    }
}