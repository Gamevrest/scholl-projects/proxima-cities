using System;
using UnityEngine;

public enum RoadType
{
    CROSSROAD,
    DEADEND,
    TURN,
    THREE_WAY,
    LINE,
}

[Serializable]
public class Road
{
    private int _neighbours;
    private int _positionX;
    private int _positionY;
    private int _rotation;
    private RoadType _type;

    public Road(RoadType type, int rotation)
    {
        _type = type;
        _rotation = rotation;
    }

    public Road()
    {
    }

    public Road SetRoadType(RoadType type)
    {
        _type = type;
        return this;
    }

    public RoadType GetRoadType()
    {
        return _type;
    }

    public Road SetPosition(Vector2Int newPos)
    {
        _positionX = newPos.x;
        _positionY = newPos.y;
        return this;
    }

    public Vector2Int GetPosition()
    {
        return new Vector2Int(_positionX, _positionY);
    }

    public Road SetRotation(int newRot)
    {
        _rotation = newRot;
        return this;
    }

    public int GetRotation()
    {
        return _rotation;
    }

    public Road SetNeighbours(int neighbours)
    {
        _neighbours = neighbours;
        return this;
    }

    public int GetNeighbours()
    {
        return _neighbours;
    }
}