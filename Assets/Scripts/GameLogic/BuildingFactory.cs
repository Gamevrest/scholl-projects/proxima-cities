﻿using System;
using UnityEngine;

public static class BuildingFactory  {

    public static BuildingsAbstract generateNewBuilding<T>(Vector2Int position) where T : BuildingsAbstract
    {
        T building = (T)Activator.CreateInstance(typeof(T));
        building.setPosition(position);
        return building;
    }
    
    public static BuildingsAbstract generateNewBuilding<T>() where T : BuildingsAbstract
    {
        T building = (T)Activator.CreateInstance(typeof(T));
        return building;
    }

    public static BuildingsAbstract upgradeBuilding<T>(BuildingsAbstract oldBuilding) where T : BuildingsAbstract
    {
        T building = (T)Activator.CreateInstance(typeof(T));
        if (oldBuilding.getBuildingType().getDefaultName() != oldBuilding.getName())
        {
            building.setName(oldBuilding.getName());
        }
        building.setPosition(oldBuilding.getPosition());
        return building;
    }
}