using System;
using System.Linq;
using UnityEngine;
using Random = System.Random;

namespace GameLogic.Weather
{
    public class WeatherManager
    {
        private WeatherState _currentWeather = WeatherState.UNDEFINED;
        private readonly EventManager _eventManager = EventManager.Instance;
        public void WeatherLoop()
        {
            if (GameManager.Instance.GetWeatherSchedule().Count > 0 && _currentWeather == WeatherState.UNDEFINED && DateTime.Now < GameManager.Instance.GetNextWeatherChange())
            {
                ChangeWeather(GameManager.Instance.GetWeatherSchedule().First());
            }
            if (DateTime.Now < GameManager.Instance.GetNextWeatherChange()) return;
            
            ChangeWeather(SelectNextWeather());
            GameManager.Instance.SetNextWeatherChange(DateTime.Now.AddDays(1));
        }
        
        

        private WeatherState SelectNextWeather()
        {
            var weatherSchedule = GameManager.Instance.GetWeatherSchedule();
            if (weatherSchedule.Count == 0) {
                var rand = new Random();
                while (weatherSchedule.Count < 7)
                {
                    weatherSchedule.Add((WeatherState) rand.Next(1, 5));
                }
            }
            var nextWeather = weatherSchedule.First();
            weatherSchedule.Remove(nextWeather);
            GameManager.Instance.SetWeatherSchedule(weatherSchedule);
            return nextWeather;
        }

        private void ChangeWeather(WeatherState newWeather)
        {
            _currentWeather = newWeather;
            _eventManager.Raise(new WeatherChangeEvent().SetWeather(_currentWeather));
        }

        public WeatherState GetCurrentWeather()
        {
            return _currentWeather;
        }
    }
}