namespace GameLogic.Weather
{
    public enum WeatherState
    {
        UNDEFINED = 0,
        SUNNY = 1,
        RAINY = 2,
        WINDY = 3,
        STORMY = 4
    }
}