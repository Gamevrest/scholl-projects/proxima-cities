using System;

[Serializable]
public class GoToGreenHouseMissionRequirement : MissionRequirement
{
    public GoToGreenHouseMissionRequirement() : base(MissionRequirementType.GO_TO_GREENHOUSE)
    {
    }
}