using System;

[Serializable]
public class HarvestPlantMissionRequirement : MissionRequirement
{
    public HarvestPlantMissionRequirement(int seedCount) : base(MissionRequirementType.GROW_PLANT, seedCount)
    {
    }

}