using System;

[Serializable]
public class EarnMoneyMissionRequirement : MissionRequirement
{
    public EarnMoneyMissionRequirement(long moneyCount) :base(MissionRequirementType.EARN_MONEY, moneyCount)
    {
    }
}