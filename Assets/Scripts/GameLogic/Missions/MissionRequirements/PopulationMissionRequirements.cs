using System;

[Serializable]
public class PopulationMissionRequirements : MissionRequirement
{
    public PopulationMissionRequirements(long populationCount) :base(MissionRequirementType.POPULATION, populationCount)
    {
    }
}