using System;

[Serializable]
public class AddBuildingMissionRequirement : MissionRequirement
{
    private readonly BuildingType _buildingType;
    public AddBuildingMissionRequirement(BuildingType buildingType, int buildingCount) : base(MissionRequirementType.ADD_BUILDING, buildingCount)
    {
        _buildingType = buildingType;
    }

    public BuildingType getBuildingType()
    {
        return _buildingType;
    }
}