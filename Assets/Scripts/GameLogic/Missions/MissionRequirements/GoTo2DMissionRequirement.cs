using System;

[Serializable]
public class GoTo2DMissionRequirement : MissionRequirement
{
    public GoTo2DMissionRequirement() : base(MissionRequirementType.GO_TO_2D)
    {
    }
}