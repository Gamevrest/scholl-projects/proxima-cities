using System.Collections.Generic;
using UnityEngine;

public enum MissionName
{
    //NORMAL
    TEST_HOUSE,
    TEST_ROADS,
    TEST_WINDTURBINE,
    TEST_SEED,
    TEST_HABITATION,
    TEST_AR,
    TEST_BIGSEED,
    TEST_POLLUTION,
    TEST_ECOLOHOUSE,
    TUTO_HOUSE,
    TUTO_WINDTURBINE,
    TUTO_GREENHOUSE,
    TUTO_PLANTBASICSEED,
    TUTO_ROAD,
    TUTO_ACCESSARVIEW,
    TUTO_PLANTSEEDS,
    TUTO_ADVANCED,
    TUTO_EARNMONEY,
    TUTO_CITY,
    TUTO_POWERED,
    
    //DEMO
    DEMO_START
}

public static class MissionList
{
    public static readonly Dictionary<MissionName, Mission> list =
        new Dictionary<MissionName, Mission>
        {
            //NORMAL
            {MissionName.TEST_HOUSE, new Mission(MissionType.NORMAL).setMissionTextId("MISSION_BUILDHOUSE_TITLE")//Build House
                .addRequirement(new AddBuildingMissionRequirement(BuildingType.HOUSE, 2))
                .addActionOnCompletion(new GiveMoneyMissionAction(3500))
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TEST_ROADS))},
            {MissionName.TEST_ROADS, new Mission(MissionType.NORMAL).setMissionTextId("MISSION_BUILDROADS_TITLE")//Build Roads
                .addRequirement(new AddRoadMissionRequirement(5))
                .addActionOnCompletion(new GiveMoneyMissionAction(1000))
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TEST_WINDTURBINE))},
            {MissionName.TEST_WINDTURBINE, new Mission(MissionType.NORMAL).setMissionTextId("MISSION_BUILDWINDTURBINE_TITLE")//Build WindTurbine
                .addRequirement(new AddBuildingMissionRequirement(BuildingType.WIND_TURBINE, 1))
                .addActionOnCompletion(new GiveMoneyMissionAction(2000))
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TEST_SEED))},
            {MissionName.TEST_SEED, new Mission(MissionType.NORMAL).setMissionTextId("MISSION_HARVESTSEED_TITLE")//Harvest Seed
                .addRequirement(new HarvestPlantMissionRequirement(1))
                .addActionOnCompletion(new GiveMoneyMissionAction(1000))
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TEST_HABITATION))},
            {MissionName.TEST_HABITATION, new Mission(MissionType.NORMAL).setMissionTextId("MISSION_BUILDHABITATION_TITLE")//Build Habitation
                .addRequirement(new AddBuildingMissionRequirement(BuildingType.HABITATION, 1))
                .addActionOnCompletion(new GiveMoneyMissionAction(1000))
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TEST_AR))},
            {MissionName.TEST_AR, new Mission(MissionType.NORMAL).setMissionTextId("MISSION_USEAR_TITLE")//Use AR !
                .addRequirement(new GoToARMissionRequirement())
                .addActionOnCompletion(new GiveMoneyMissionAction(1000))
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TEST_BIGSEED))},
            {MissionName.TEST_BIGSEED, new Mission(MissionType.NORMAL).setMissionTextId("MISSION_HARVESTSEEDS_TITLE")//Harvest Seeds !
                .addRequirement(new HarvestPlantMissionRequirement(3))
                .addActionOnCompletion(new GiveMoneyMissionAction(3000))
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TEST_POLLUTION))},
            {MissionName.TEST_POLLUTION, new Mission(MissionType.NORMAL).setMissionTextId("MISSION_REDUCEPOLLUTION_TITLE")//Reduce pollution
                .addRequirement(new PollutionMissionRequirements(30))
                .addActionOnCompletion(new GiveMoneyMissionAction(1000))
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TEST_ECOLOHOUSE))},
            {MissionName.TEST_ECOLOHOUSE, new Mission(MissionType.NORMAL).setMissionTextId("MISSION_REDUCEPOLLUTION_TITLE")//Reduce pollution
                .addRequirement(new AddBuildingMissionRequirement(BuildingType.ECOLOGICAL_HOUSE, 1))
                .addActionOnCompletion(new GiveMoneyMissionAction(1000))
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TEST_HOUSE))},
            {MissionName.TUTO_HOUSE, new Mission(MissionType.TUTO).setMissionTextId("MISSION_FIRSTSTEP_TITLE")//First step !
                .addRequirement(new AddBuildingMissionRequirement(BuildingType.HOUSE, 1))
                .addActionOnStart(new ShowTextMissionAction("MISSION_FIRSTSTEP_TEXT_1"))//Hello, welcome to Proxima !\nI am Proximian and I live on this Planet.
                .addActionOnStart(new ShowTextMissionAction("MISSION_FIRSTSTEP_TEXT_2"))//You are here to develop your City aren't you ? I'll help you doing so !
                .addActionOnStart(new ShowTextMissionAction("MISSION_FIRSTSTEP_TEXT_3", new Vector2(-812, -422), 2))//Let's start by building your first <u><i>house</i></u> !\n Here is some money for you to start with
                .addActionOnStart(new GiveMoneyMissionAction(3000))
                .addActionOnCompletion(new ShowTextMissionAction("MISSION_FIRSTSTEP_TEXT_4"))//Well done !\nThis house will produce some money and you need to collect it
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TUTO_WINDTURBINE))
            },
            {MissionName.TUTO_WINDTURBINE, new Mission(MissionType.TUTO).setMissionTextId("MISSION_ENERGY_TITLE")//Energy !
                .addRequirement(new AddBuildingMissionRequirement(BuildingType.WIND_TURBINE, 1))
                .addActionOnStart(new ShowTextMissionAction("MISSION_ENERGY_TEXT_1"))//You can see on upper left that it increase your population
                .addActionOnStart(new ShowTextMissionAction("MISSION_ENERGY_TEXT_2"))//But also your pollution ! Be careful about it
                .addActionOnStart(new ShowTextMissionAction("MISSION_ENERGY_TEXT_3", new Vector2(-650, -422), 2))//Your house require power to become more effective, let's build a <i><u>wind turbine</u></i> !
                .addActionOnStart(new GiveMoneyMissionAction(6000))
                .addActionOnCompletion(new ShowTextMissionAction("MISSION_ENERGY_TEXT_4"))//Great !
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TUTO_GREENHOUSE))
            },
            {MissionName.TUTO_GREENHOUSE, new Mission(MissionType.TUTO).setMissionTextId("MISSION_GREENSEEDING_TITLE")//Green Seeding
                .addRequirement(new GoToGreenHouseMissionRequirement())
                .addRequirement(new PlantSeedMissionRequirement(1))
                .addActionOnStart(new ShowTextMissionAction("MISSION_GREENSEEDING_TEXT_1"))//Now, I'll show you about one of the most important buildings, the <i><u>GreenHouse</u></i> !
                .addActionOnStart(new ShowTextMissionAction("MISSION_GREENSEEDING_TEXT_2"))//You can here plant seeds, giving you rewards !
                .addActionOnStart(new ShowTextMissionAction("MISSION_GREENSEEDING_TEXT_3"))//Start by planting a basic seed !
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TUTO_PLANTBASICSEED))
            },
            {MissionName.TUTO_PLANTBASICSEED, new Mission(MissionType.TUTO).setMissionTextId("MISSION_FIRSTPLANT_TITLE")//First Plant !
                .addRequirement(new HarvestPlantMissionRequirement(1))
                .addActionOnStart(new ShowTextMissionAction("MISSION_FIRSTPLANT_TEXT_1"))//You did well !
                .addActionOnStart(new GiveMoneyMissionAction(1000))
                .addActionOnStart(new ShowTextMissionAction("MISSION_FIRSTPLANT_TEXT_2"))//You will be able to harvest it soon enough
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TUTO_ROAD))
            },
                        
            {MissionName.TUTO_ROAD, new Mission(MissionType.TUTO).setMissionTextId("MISSION_ROADS_TITLE")//Roads !
                .addRequirement(new AddRoadMissionRequirement(5))
                .addActionOnStart(new ShowTextMissionAction("MISSION_ROAD_TEXT_1"))//You can customize your city by adding some roads\nYou can access to the road menu by clicking on the icon in lower right corner
                .addActionOnStart(new ShowTextMissionAction("MISSION_ROAD_TEXT_2", new Vector2(775, -425), 2))//Let's start by adding <b><u>5 roads</u></b>
                .addActionOnCompletion(new ShowTextMissionAction("MISSION_ROAD_TEXT_3"))//Well done !
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TUTO_ACCESSARVIEW))
            },
            
            {MissionName.TUTO_ACCESSARVIEW, new Mission(MissionType.TUTO).setMissionTextId("MISSION_3DWORLD_TITLE")//3D world
                .addRequirement(new GoToARMissionRequirement())
                .addActionOnStart(new ShowTextMissionAction("MISSION_3DWORLD_TEXT_1"))//Now I'll show you about the coolest thing around here, the <i><u>AR view</u></i> !
                .addActionOnStart(new ShowTextMissionAction("MISSION_3DWORLD_TEXT_2", new Vector2(700, 470), 2))//You can access AR view just by clicking AR button !\nYou need a flashcode for it to work properly
                .addActionOnCompletion(new ShowTextMissionAction("MISSION_3DWORLD_TEXT_3"))//Back to Management !
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TUTO_PLANTSEEDS))
            },
            
            {MissionName.TUTO_PLANTSEEDS, new Mission(MissionType.TUTO).setMissionTextId("MISSION_MIXNPLANT_TITLE")//Mix n plant
                .addRequirement(new HarvestPlantMissionRequirement(2))
                .addActionOnStart(new ShowTextMissionAction("MISSION_MIXNPLANT_TEXT_1"))//Collect now a few seeds to mix them !
                .addActionOnStart(new ShowTextMissionAction("MISSION_MIXNPLANT_TEXT_2"))//Go to collect <b><u>2 seeds</u></b>
                .addActionOnStart(new GiveMoneyMissionAction(2000))
                .addActionOnCompletion(new ShowTextMissionAction("MISSION_MIXNPLANT_TEXT_3"))//That's great!
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TUTO_ADVANCED))
            },

            {MissionName.TUTO_ADVANCED, new Mission(MissionType.TUTO).setMissionTextId("MISSION_UPGRADE_TITLE")//UP grade
                .addRequirement(new BuildingOwnedMissionRequirement(BuildingType.HABITATION, 1))
                .addActionOnStart(new ShowTextMissionAction("MISSION_UPGRADE_TEXT_1", new Vector2(-812, -422), 2))//Upgrade your houses !\n You can that way have more citizen, and earn more money !
                .addActionOnStart(new ShowTextMissionAction("MISSION_UPGRADE_TEXT_2"))//Upgrade one house to <b><u>Habitation</u></b>
                .addActionOnCompletion(new ShowTextMissionAction("MISSION_UPGRADE_TEXT_3"))//Beautiful !
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TUTO_EARNMONEY))
            },
                        
            {MissionName.TUTO_EARNMONEY, new Mission(MissionType.TUTO).setMissionTextId("MISSION_MONEY_TITLE")//Money !
                .addRequirement(new EarnMoneyMissionRequirement(50))
                .addActionOnStart(new ShowTextMissionAction("MISSION_MONEY_TEXT_1", new Vector2(-780, 230), 2))//You will need some money for later\n Try to gather at least 8000 !
                .addActionOnStart(new ShowTextMissionAction("MISSION_MONEY_TEXT_2"))//Possess <b><u>8000 money</u></b>
                .addActionOnCompletion(new ShowTextMissionAction("MISSION_MONEY_TEXT_3"))//So rich !
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TUTO_CITY))
            },
            
            {MissionName.TUTO_CITY, new Mission(MissionType.TUTO).setMissionTextId("MISSION_CITY_TITLE")//Powered
                .addRequirement(new AddBuildingMissionRequirement(BuildingType.HOUSE, 3))
                .addActionOnStart(new ShowTextMissionAction("MISSION_CITY_TEXT_1", new Vector2(-812, -422), 2))//You need to expand\n Build a few Houses !
                .addActionOnStart(new ShowTextMissionAction("MISSION_CITY_TEXT_2"))//Build <b><u>4 House</u></b>
                .addActionOnCompletion(new ShowTextMissionAction("MISSION_CITY_TEXT_3"))//Gigantic !
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TUTO_POWERED))
            },
            
            {MissionName.TUTO_POWERED, new Mission(MissionType.TUTO).setMissionTextId("MISSION_POWERED_TITLE")//Powered
                .addRequirement(new AddBuildingMissionRequirement(BuildingType.WATER_TURBINE, 1))
                .addActionOnStart(new ShowTextMissionAction("MISSION_POWERED_TEXT_1", new Vector2(-300, -422), 2))//From now on, you will need more power\n What about a water turbine ?
                .addActionOnStart(new ShowTextMissionAction("MISSION_POWERED_TEXT_2"))//Construct a  <b><u>Water Turbine</u></b>
                .addActionOnCompletion(new ShowTextMissionAction("MISSION_POWERED_TEXT_3"))//Awesome !
                .addActionOnCompletion(new PushMissionMissionAction(MissionName.TEST_HOUSE))
            },
            //DEMO
            {MissionName.DEMO_START, new Mission(MissionType.TUTO).setMissionTextId("MISSION_DEMO_START_TITLE")//Demo version
                .addActionOnStart(new ShowTextMissionAction("MISSION_DEMO_START_TEXT_1"))//Welcome to this demo of proxima city !
                .addActionOnStart(new GiveMoneyMissionAction(1000000))
            },
        };
}