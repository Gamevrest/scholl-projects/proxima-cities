using System;

[Serializable]
public class GiveSeedMissionAction : MissionAction
{
    private readonly int _amount;
    private readonly SeedType _type;

    public GiveSeedMissionAction(SeedType type, int amount) : base(MissionActionType.GIVE_SEED)
    {
        _type = type;
        _amount = amount;
    }

    public SeedType getSeedType()
    {
        return _type;
    }

    public int getAmount()
    {
        return _amount;
    }
}