using System;

[Serializable]
public class ReducePollutionMissionAction : MissionAction
{
    private readonly long _pollutionCount;
    public ReducePollutionMissionAction(long pollutionCount) : base(MissionActionType.REDUCE_POLLUTION)
    {
        _pollutionCount = pollutionCount;
    }

    public long GetPollutionCount()
    {
        return _pollutionCount;
    }
}