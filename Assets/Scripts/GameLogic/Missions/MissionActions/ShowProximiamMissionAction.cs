using System;

[Serializable]
public class ShowProximiamMissionAction : MissionAction
{
    public ShowProximiamMissionAction() : base(MissionActionType.SHOW_PROXIMIAM)
    {
                
    }
}