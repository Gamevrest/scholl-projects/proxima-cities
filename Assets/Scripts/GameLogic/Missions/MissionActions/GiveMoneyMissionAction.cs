using System;

[Serializable]
public class GiveMoneyMissionAction : MissionAction
{
    private readonly long _moneyCount;

    public GiveMoneyMissionAction(long moneyCount) : base(MissionActionType.GIVE_GOLD)
    {
        _moneyCount = moneyCount;
    }

    public long GetMoneyCount()
    {
        return _moneyCount;
    }
}