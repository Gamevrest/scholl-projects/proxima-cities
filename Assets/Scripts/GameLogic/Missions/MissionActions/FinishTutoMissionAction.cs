using System;

[Serializable]
public class FinishTutoMissionAction : MissionAction
{
    public FinishTutoMissionAction() : base(MissionActionType.FINISH_TUTO)
    {
            
    }
}