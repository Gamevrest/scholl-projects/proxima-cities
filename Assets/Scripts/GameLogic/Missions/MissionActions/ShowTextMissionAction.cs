using System;
using UnityEngine;

[Serializable]
public class ShowTextMissionAction : MissionAction
{
    private readonly string _id;
    private readonly object[] _vars;
    private Vector2 _position;
    private int _scale;
    

    public ShowTextMissionAction(string id, Vector2? position = null, int scale = 0, params object[] vars) : base(MissionActionType.SHOW_TEXT)
    {
        _id = id;
        if (position != null)
        {
            _position = position.Value;
        }
        else
        {
            _position = Vector2.zero;
        }
        _scale = scale;
        _vars = vars;
    }

    public string GetId()
    {
        return _id;
    }

    public object[] GetVars()
    {
        return _vars;
    }
    
    public Vector2 GetPosition()
    {
        return _position;
    }

    public int GetScale()
    {
        return _scale;
    }
}