using System;

[Serializable]
public class PushMissionMissionAction : MissionAction
{

    private readonly MissionName _name;
    public PushMissionMissionAction(MissionName name) : base(MissionActionType.PUSH_MISSION)
    {
        _name = name;
    }

    public MissionName getMissionName()
    {
        return _name;
    }
}