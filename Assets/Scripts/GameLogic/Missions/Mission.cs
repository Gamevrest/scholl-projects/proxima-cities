using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public enum MissionType
{
    TUTO,
    NORMAL,
}
[Serializable]
public class Mission
{
    private readonly MissionType _type;
    private readonly List<MissionRequirement> _requirements = new List<MissionRequirement>();
    private readonly List<MissionAction> _actionOnStart = new List<MissionAction>();
    private readonly List<MissionAction> _actionOnCompletion = new List<MissionAction>();
    private bool _isCompleted;
    private string _missionTextId;


    public Mission(MissionType type)
    {
        _type = type;
    }

    public MissionType getMissionType()
    {
        return _type;
    }
    public Mission addRequirement(MissionRequirement requirement)
    {
        _requirements.Add(requirement);
        return this;
    }
    
    public Mission addActionOnStart(MissionAction action)
    {
        _actionOnStart.Add(action);
        return this;
    }
    
    public Mission addActionOnCompletion(MissionAction action)
    {
        _actionOnCompletion.Add(action);
        return this;
    }

    public List<MissionAction> getActionOnStart()
    {
        return _actionOnStart;
    }
    
    public List<MissionAction> getActionOnCompletion()
    {
        return _actionOnCompletion;
    }
    
    public Mission setMissionTextId(string newId)
    {
        _missionTextId = newId;
        return this;
    }

    public string getMissionTextId()
    {
        return _missionTextId;
    }
    
    public List<MissionRequirement> getMissionRequirements()
    {
        return _requirements;
    }

    public void setCompleted()
    {
        _isCompleted = true;
    }

    public bool isCompleted()
    {
        var isFinish = true;
        foreach (var requirement in _requirements)
        {
            if (requirement.isAchieved()) continue;
            isFinish = false;
            break;
        }

        return _isCompleted || isFinish;
    }


    public override string ToString()
    {
        var text = "\tMissionType => " + _type + "\n";
        text += "\tMissionRequirements :\n";
        foreach (var requirement in _requirements)
        {
            text += "\t\t" + requirement.getCurrentCount() + "/"+requirement.getNeededCount() + "  =>  " + requirement.getMissionRequirementType()+"\n";
        }
        text += "\tActionOnStart :\n";
        foreach (var action in _actionOnStart)
        {
            text += "\t\t" + action.getMissionActionType()+"\n";
        }
        text += "\tActionOnCompletion :\n";
        foreach (var action in _actionOnCompletion)
        {
            text += "\t\t" + action.getMissionActionType()+"\n";
        }
        
        return text;
    }

    public Mission Clone()
    {
        using (var ms = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            SurrogateSelector surrogateSelector = new SurrogateSelector();
            Vector3SerializationSurrogate vector3Ss = new Vector3SerializationSurrogate();
            Vector2SerializationSurrogate vector2Ss = new Vector2SerializationSurrogate();
            Vector2IntSerializationSurrogate vector2ISs = new Vector2IntSerializationSurrogate();
            surrogateSelector.AddSurrogate(typeof(Vector3),new StreamingContext(StreamingContextStates.All),vector3Ss);
            surrogateSelector.AddSurrogate(typeof(Vector2),new StreamingContext(StreamingContextStates.All),vector2Ss);
            surrogateSelector.AddSurrogate(typeof(Vector2Int),new StreamingContext(StreamingContextStates.All),vector2ISs);
            formatter.SurrogateSelector = surrogateSelector;
            formatter.Serialize(ms, this);
            ms.Position = 0;

            return (Mission) formatter.Deserialize(ms);
        }
    }
}