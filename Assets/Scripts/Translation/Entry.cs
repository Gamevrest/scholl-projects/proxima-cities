﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Translation
{
    [Serializable]
    public class Entry
    {
        [HideInInspector] public string name;
        private readonly string _key;
        private readonly SortedDictionary<Tr.Languages, string> _translations;

        public Entry(IList<string> row)
        {
            if (row.Count < 2)
                return;

            _key = row[0];
            _translations = new SortedDictionary<Tr.Languages, string>();
            var i = 0;


            foreach (Tr.Languages language in Tr.GetLanguages())
            {
                if (!_translations.ContainsKey(language))
                    _translations.Add(language, "");
                if (i <= row.Count)
                    _translations[language] = row[i].Replace("\"", "");
                i++;
            }

            name = _key;
        }

        public Entry(string id)
        {
            _key = id;
            _translations = new SortedDictionary<Tr.Languages, string>();
            foreach (Tr.Languages language in Tr.GetLanguages())
                if (!_translations.ContainsKey(language))
                {
                    var tmp = "";
                    if (language == Tr.Languages.ID) tmp = $"{id}";
                    _translations.Add(language, tmp);
                }

            name = _key;
        }

        public override string ToString()
        {
            var ret = _key;
            foreach (var translation in _translations)
                if (translation.Key != Tr.Languages.ID)
                    ret += $",\"{translation.Value}\"";

            return ret;
        }

        public void SetTranslations(IList<string> row)
        {
            var i = 0;
            foreach (Tr.Languages language in Tr.GetLanguages())
            {
                if (!_translations.ContainsKey(language))
                    _translations.Add(language, "");
                if (i < row.Count)
                    _translations[language] = row[i].Replace("\"", "");
                i++;
            }
        }

        public void SetTranslation(Tr.Languages lng, string str)
        {
            if (!_translations.ContainsKey(lng)) return;
            _translations[lng] = str;
        }

        public string GetKey() => _key ?? "EMPTY";
        public SortedDictionary<Tr.Languages, string> GetTranslations() => _translations;
        public bool IsValid() => !string.IsNullOrWhiteSpace(_key) && _translations != null && _translations.Count > 0;

        public string GetValue(Tr.Languages lng) =>
            !_translations.ContainsKey(lng) ? $"PLACEHOLDER [{_key}]" : _translations[lng];
    }
}