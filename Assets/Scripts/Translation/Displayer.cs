﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace Translation
{
    [CreateAssetMenu(fileName = "Displayer", menuName = "ScriptableObject/Translation Displayer", order = 0)]
    public class Displayer : ScriptableObject
    {
        [HideInInspector] public string importPath;
        [HideInInspector] public string exportPath;

        public void Test()
        {
            EventManager.Instance.Raise(new GameErrorEvent().SetException(new BuildingTypeNotFoundException("TEST")));
            EventManager.Instance.Raise(
                new GameErrorEvent().SetException(new EmptyPositionException(new Vector2(42, 24))));
            EventManager.Instance.Raise(new GameErrorEvent().SetException(new EmptyPotException(42)));
            EventManager.Instance.Raise(new GameErrorEvent().SetException(new EventNotFoundException("TEST")));
            //EventManager.Instance.Raise(new GameErrorEvent().SetException(new GamevrestException("TEST")));
            EventManager.Instance.Raise(
                new GameErrorEvent().SetException(new NoEnergyAtPositionException(new Vector2Int(42, 24))));
            EventManager.Instance.Raise(new GameErrorEvent().SetException(new NotEmptyPotException()));
            EventManager.Instance.Raise(new GameErrorEvent().SetException(new NotEnoughMoneyException()));
            EventManager.Instance.Raise(new GameErrorEvent().SetException(new NotEnoughMoneyException()));
            EventManager.Instance.Raise(
                new GameErrorEvent().SetException(new NotEnoughSeedException(SeedType.ADVANCED)));
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(Displayer)), CanEditMultipleObjects]
    public class DisplayerEditor : Editor
    {
        private bool _showDefault;

        public override void OnInspectorGUI()
        {
            DrawCustomInspector();
            _showDefault = EditorGUILayout.Foldout(_showDefault, "Default Inspector");
            if (_showDefault) DrawDefaultInspector();
            serializedObject.ApplyModifiedProperties();
        }

        private void DrawCustomInspector()
        {
            var importPath = serializedObject.FindProperty("importPath");
            var exportPath = serializedObject.FindProperty("exportPath");
            var targ = target as Displayer;

//            GUILayout.BeginHorizontal();
//            importPath.stringValue = EditorGUILayout.TextField(importPath.stringValue);
//            if (GUILayout.Button("Load"))
//                Tr.Instance.LoadCached();
//            GUILayout.EndHorizontal();
//            GUILayout.BeginHorizontal();
//            exportPath.stringValue = EditorGUILayout.TextField(exportPath.stringValue);
//            if (GUILayout.Button("Export"))
//                Tr.Instance.ExportToFile(exportPath.stringValue);
//            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Reset"))
                Tr.Instance.Reset();
            if (GUILayout.Button("Test"))
                targ.Test();
            GUILayout.EndHorizontal();
            GUILayout.Label($"Language choice : (cur = {Tr.GetLanguage()})");
            foreach (Tr.Languages language in Tr.GetLanguages())
            {
                GUILayout.BeginHorizontal();
                GUILayout.Space(10);
                if (GUILayout.Button(language.ToString()))
                    Tr.SelectLanguage(language);
                GUILayout.EndHorizontal();
            }

            serializedObject.ApplyModifiedProperties();
        }
    }

#endif
}