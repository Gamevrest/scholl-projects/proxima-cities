using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace UI
{
    //[CreateAssetMenu(menuName = "Resources Cache")]
    public class Resources : ScriptableObject

    {
        public static readonly Dictionary<string, UnityEngine.Object> Cache =
            new Dictionary<string, UnityEngine.Object>();

        public static T Load<T>(string path) where T : UnityEngine.Object
        {
            if (Cache.ContainsKey(path)) return (T) Cache[path];
            var tmp = UnityEngine.Resources.Load<T>(path);
            Cache[path] = tmp;
            return tmp;
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(Resources))]
    public class ResourcesCacheEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (Resources.Cache.Count <= 0) GUILayout.Label("Launch game to see loaded files");
            foreach (var c in Resources.Cache)
            {
                EditorGUILayout.BeginHorizontal();
                //GUILayout.Label(c.Key);
                EditorGUILayout.ObjectField(c.Key, c.Value, typeof(UnityEngine.Object), true);
                EditorGUILayout.EndHorizontal();
            }
        }
    }
#endif
}