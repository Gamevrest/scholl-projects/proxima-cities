﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UIManager : MonoBehaviour {
        public GameObject globalGrid;
        public GameObject gameGrid;
        public GameObject cellPrefab;
        public GameObject roadGrid;
        public GameObject roadCell;
        public GameObject gestionView;
        public GameObject arButton;
        public GameObject playerStats;
        public GameObject missionButton;
        public GameObject gestion;

        public int gridSize;
        public int gridRoadSize;
        
                
        public float zoomMin = 1;
        public float zoomMax = 4;
        
        private int _gridConstraint;
        private int _gridConstraintRoad;

        private bool _arActive;

        private void Start()
        {
            EventManager.Instance.Subscribe(Hide2DViewAR);
            EventManager.Instance.Subscribe(Hide2DViewGH);
            EventManager.Instance.Subscribe(Show2DView);
            _gridConstraint = gameGrid.GetComponent<GridLayoutGroup>().constraintCount;
            _gridConstraint = gameGrid.GetComponent<GridLayoutGroup>().constraintCount;
            _gridConstraintRoad = roadGrid.GetComponent<GridLayoutGroup>().constraintCount;
            CreateGrid();
            Invoke(nameof(StopLoading), 0.25f);

        }
        
        
        // Update is called once per frame
        void Update () {

            if (gestion.activeSelf)
            {
                if(Input.touchCount == 2){
                    Touch touchZero = Input.GetTouch(0);
                    Touch touchOne = Input.GetTouch(1);

                    Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                    Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                    float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                    float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

                    float difference = currentMagnitude - prevMagnitude;

                    Zoom(difference * 0.01f);
                }

                if (Input.GetAxis("Mouse ScrollWheel") != 0f) {
                    Zoom(Input.GetAxis("Mouse ScrollWheel"));
                }
            }

        }

        private void CreateGrid()
        {
            var curX = 0;
            var curY = 1;
            Vector2Int curPos;
            for (var i = 0; i < gridSize; i++)
            {
                curX++;
                if (curX % _gridConstraint == 0)
                {
                    curPos = new Vector2Int(curX, curY);
                    curY++;
                    curX = 0;
                }
                else
                {
                    curPos = new Vector2Int(curX, curY);
                }
            
                var newCell = Instantiate(cellPrefab, gameGrid.transform);
                newCell.GetComponent<DropMe>().SetPos(curPos);
                try{newCell.GetComponent<DropMe>().InitBuilding(GameManager.Instance.GetBuildingByPosition(curPos));}catch (EmptyPositionException){}
            }

            curX = 0;
            curY = 1;
            for (var i = 0; i < gridRoadSize; i++)
            {
                curX++;
                if (curX % _gridConstraintRoad == 0)
                {
                    curPos = new Vector2Int(curX, curY);
                    curY++;
                    curX = 0;
                }
                else
                {
                    curPos = new Vector2Int(curX, curY);
                }

                var newCell = Instantiate(roadCell, roadGrid.transform);
                newCell.GetComponent<RoadGrid>().SetPos(curPos);
                try {
                    var road = GameManager.Instance.GetRoadByPosition(curPos);
                    newCell.GetComponent<RoadGrid>().InitRoad(road.GetRoadType(), road.GetRotation());
                }
                catch (EmptyPositionException) {}
            }
        }

        private void DisableAr()
        {
            EventManager.Instance.Raise(new StartLoadingEvent());
            EventManager.Instance.Raise(new SwitchTo2DEvent());
            arButton.transform.Find("ARImage").gameObject.SetActive(true);
            arButton.transform.Find("CityImage").gameObject.SetActive(false);
            _arActive = false;
        }

        private void EnableAr()
        {
            EventManager.Instance.Raise(new StartLoadingEvent());
            EventManager.Instance.Raise(new SwitchToAREvent());
            arButton.transform.Find("ARImage").gameObject.SetActive(false);
            arButton.transform.Find("CityImage").gameObject.SetActive(true);
            _arActive = true;
        }

        public void SwitchToGreenHouse()
        {
            EventManager.Instance.Raise(new StartLoadingEvent());
            EventManager.Instance.Raise(new SwitchToGreenHouseEvent());
        }

        public void SwitchAr()
        {
            if (_arActive) DisableAr();
            else if (!_arActive) EnableAr();
        }
    
        private void Hide2DViewGH(SwitchToGreenHouseEvent evt)
        {
            gestionView.SetActive(false);
            playerStats.SetActive(false);
            missionButton.SetActive(false);
            arButton.SetActive(false);
        }

        private void Hide2DViewAR(SwitchToAREvent evt)
        {
            gestionView.SetActive(false);
            playerStats.SetActive(false);
            missionButton.SetActive(false);
        }

        private void Show2DView(SwitchTo2DEvent evt)
        {
            gestionView.SetActive(true);
            playerStats.SetActive(true);
            missionButton.SetActive(true);
            arButton.SetActive(true);

            Invoke(nameof(StopLoading), 0.25f);
        }

        private void StopLoading() => EventManager.Instance.Raise(new StopLoadingEvent());


        void Zoom(float increment){
            var localScale = globalGrid.transform.localScale;
            if (localScale.x + increment >= zoomMin &&
                localScale.y + increment <= zoomMax) {
                globalGrid.transform.localScale = new Vector3(localScale.x + increment, localScale.y + increment, 1);
            }
        }
    }
}