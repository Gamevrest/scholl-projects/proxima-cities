﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class RoadGrid : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        //            Up = 1
        //Left = 10           Right = 20 
        //            Down = 2
        private readonly Dictionary<int, Correspondances> _corresDict = new Dictionary<int, Correspondances>
        {
            {0, new Correspondances {Type = RoadType.CROSSROAD, Rotation = 0}}, //none
            {1, new Correspondances {Type = RoadType.DEADEND, Rotation = 0}}, //up  
            {2, new Correspondances {Type = RoadType.DEADEND, Rotation = 2}}, //down
            {3, new Correspondances {Type = RoadType.LINE, Rotation = 0}}, //up down
            {10, new Correspondances {Type = RoadType.DEADEND, Rotation = 3}}, //left
            {11, new Correspondances {Type = RoadType.TURN, Rotation = 3}}, //up left
            {12, new Correspondances {Type = RoadType.TURN, Rotation = 2}}, //down left
            {13, new Correspondances {Type = RoadType.THREE_WAY, Rotation = 3}}, //up down left
            {20, new Correspondances {Type = RoadType.DEADEND, Rotation = 1}}, //right
            {21, new Correspondances {Type = RoadType.TURN, Rotation = 0}}, //up right
            {22, new Correspondances {Type = RoadType.TURN, Rotation = 1}}, //down right
            {23, new Correspondances {Type = RoadType.THREE_WAY, Rotation = 1}}, //up down right
            {30, new Correspondances {Type = RoadType.LINE, Rotation = 1}}, //left right
            {31, new Correspondances {Type = RoadType.THREE_WAY, Rotation = 0}}, //left right up
            {32, new Correspondances {Type = RoadType.THREE_WAY, Rotation = 2}}, //left right down
            {33, new Correspondances {Type = RoadType.CROSSROAD, Rotation = 0}}, //all
        };

        public Image containerImage;
        private Sprite dropSprite;
        public EventManager eventManager = EventManager.Instance;
        public Color highlightColor = Color.yellow;
        [HideInInspector] public bool isEmpty = true;


        private Color normalColor;
        public Image receivingImage;
        private Vector2Int roadPos;
        private int roadRotation;
        private RoadType roadType;
        private WindowManager windowManager;

        public void OnDrop(PointerEventData data)
        {
            containerImage.color = normalColor;

            if (receivingImage == null)
                return;

            dropSprite = GetDropSprite(data);
            if (dropSprite == null) return;
            eventManager.Subscribe(GetErrorEmpty);

            var up = GameManager.Instance.GetRoadByPosition(roadPos - Vector2Int.up, false);
            var down = GameManager.Instance.GetRoadByPosition(roadPos + Vector2Int.up, false);
            var left = GameManager.Instance.GetRoadByPosition(roadPos - Vector2Int.right, false);
            var right = GameManager.Instance.GetRoadByPosition(roadPos + Vector2Int.right, false);

            var neighbours = 0;
            if (up != null)
            {
                neighbours += 1;
                MajRoad(up.GetPosition(), up.GetNeighbours() + 2);
            }

            if (down != null)
            {
                neighbours += 2;
                MajRoad(down.GetPosition(), down.GetNeighbours() + 1);
            }

            if (left != null)
            {
                neighbours += 10;
                MajRoad(left.GetPosition(), left.GetNeighbours() + 20);
            }

            if (right != null)
            {
                neighbours += 20;
                MajRoad(right.GetPosition(), right.GetNeighbours() + 10);
            }

            var newRoad = new AddRoadEvent();
            newRoad.setNeighbours(neighbours);
            newRoad.setRoadType(_corresDict[neighbours].Type);
            newRoad.setRotation(_corresDict[neighbours].Rotation);
            newRoad.setPosition(roadPos);
            eventManager.Raise(newRoad);
        }

        public void OnPointerClick(PointerEventData pointerEventData)
        {
            windowManager.HideWindows();
            if (!isEmpty)
                windowManager.ShowRoadOptions(transform.GetComponent<RoadGrid>());
            else
            {
                windowManager.HideEverything();
            }
        }

        public void OnPointerEnter(PointerEventData data)
        {
            if (containerImage == null)
                return;

            var dropSpr = GetDropSprite(data);
            if (dropSpr != null)
                containerImage.color = highlightColor;
        }

        public void OnPointerExit(PointerEventData data)
        {
            if (containerImage == null)
                return;

            containerImage.color = normalColor;
        }

        private void MajRoad(Vector2Int pos, int neighbours)
        {
//            var del = new RemoveRoadEvent();
//            del.setPosition(pos);
//            eventManager.Raise(del);
//
//            var newRoad = new AddRoadEvent();
//            newRoad.setPosition(pos);
//            newRoad.setRoadType(_corresDict[neighbours].Type);
//            newRoad.setRotation(_corresDict[neighbours].Rotation);
//            newRoad.setNeighbours(neighbours);
//            eventManager.Raise(newRoad);

            eventManager.Raise(new UpdateRoadEvent().setPosition(pos).setRoadType(_corresDict[neighbours].Type)
                .setRotation(_corresDict[neighbours].Rotation).setNeighbours(neighbours));
        }

        void Start()
        {
            windowManager = GameObject.Find("WindowManager").GetComponent<WindowManager>();
            eventManager.Subscribe(AddedRoadEvent);
            eventManager.Subscribe(RoadRemoved);
            eventManager.Subscribe(RotatedRoad);
            eventManager.Subscribe(UpdatedRoadEvent);
        }

        public void InitRoad(RoadType type, int rotation)
        {
            isEmpty = false;
            SetRoadType(type);
            SetRotation(rotation);
            receivingImage.sprite = Resources.Load<Sprite>("UI/" + roadType);
            receivingImage.transform.rotation = Quaternion.Euler(0, 0, roadRotation * 90);
        }

        public void RemoveRoad()
        {
            var newRemoveRoadEvent = new RemoveRoadEvent();
            newRemoveRoadEvent.setPosition(roadPos);
            newRemoveRoadEvent.setRoadType(roadType);
            eventManager.Raise(newRemoveRoadEvent);

            var up = GameManager.Instance.GetRoadByPosition(roadPos - Vector2Int.up, false);
            var down = GameManager.Instance.GetRoadByPosition(roadPos + Vector2Int.up, false);
            var left = GameManager.Instance.GetRoadByPosition(roadPos - Vector2Int.right, false);
            var right = GameManager.Instance.GetRoadByPosition(roadPos + Vector2Int.right, false);
            if (up != null) MajRoad(up.GetPosition(), up.GetNeighbours() - 2);
            if (down != null) MajRoad(down.GetPosition(), down.GetNeighbours() - 1);
            if (left != null) MajRoad(left.GetPosition(), left.GetNeighbours() - 20);
            if (right != null) MajRoad(right.GetPosition(), right.GetNeighbours() - 10);
        }

        private void RoadRemoved(RoadRemovedEvent evt)
        {
            if (evt.getPosition() == roadPos)
            {
                Debug.Log("Road removed");
                receivingImage.sprite = Resources.Load<Sprite>("UI/emptyroadcell");
            }
        }

        private void AddedRoadEvent(RoadAddedEvent evt)
        {
            if (dropSprite != null && evt.getPosition() == roadPos)
            {
                InitRoad(evt.getRoadType(), evt.getRotation());
                eventManager.UnSubscribe(GetErrorEmpty);
            }
        }

        private void UpdatedRoadEvent(UpdateRoadEvent evt)
        {
            if (evt.getPosition() == roadPos)
            {
                InitRoad(evt.getRoadType(), evt.getRotation());
                eventManager.UnSubscribe(GetErrorEmpty);
            }
        }

        private void RotatedRoad(RotateRoadEvent evt)
        {
            if (!evt.getPosition().Equals(roadPos)) return;
            roadRotation = evt.getRotation();
            receivingImage.transform.rotation = Quaternion.Euler(0, 0, roadRotation * 90);
        }

        public void RotateRoad()
        {
            RotateRoadEvent rotateRoad = new RotateRoadEvent();
            rotateRoad.setPosition(roadPos);
            rotateRoad.setRotation((int) receivingImage.transform.rotation.eulerAngles.z / 90 + 1 % 4);
            eventManager.Raise(rotateRoad);
        }

        private void GetErrorEmpty(GameErrorEvent evt)
        {
            if (evt.GetException() is NotEmptyPositionException t)
            {
                print(evt.GetMessageId());
                eventManager.UnSubscribe(GetErrorEmpty);
            }
        }

        public void OnEnable()
        {
            if (containerImage != null)
                normalColor = containerImage.color;
        }

        private Sprite GetDropSprite(PointerEventData data)
        {
            var originalObj = data.pointerDrag;
            if (originalObj == null)
                return null;

            var dragMe = originalObj.GetComponent<DragMe>();
            if (dragMe == null)
                return null;
            if (!dragMe.isRoad)
                return null;
            roadType = dragMe.roadType;
            var srcImage = originalObj.GetComponent<Image>();
            if (srcImage == null)
                return null;

            return srcImage.sprite;
        }

        public void SetPos(Vector2Int pos)
        {
            roadPos = pos;
        }

        public void SetRoadType(RoadType type)
        {
            roadType = type;
        }

        public void SetRotation(int rotation)
        {
            roadRotation = rotation;
        }

        private struct Correspondances
        {
            public RoadType Type;
            public int Rotation;
        }
    }
}