﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class PlayerStats : MonoBehaviour
    {
        public Text citizensText;

        private GameManager gameManager = GameManager.Instance;
        public Text moneyText;
        public Text pollutionText;


        // Update is called once per frame
        void Update()
        {
            moneyText.text = gameManager.GetMoney().ToString();
            citizensText.text = gameManager.GetCitizens().ToString();
            pollutionText.text = gameManager.GetPollution().ToString();
        }
    }
}