﻿using System;
using GameLogic;
using GameLogic.Weather;
using TMPro;
using Translation;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    public class WindowManager : MonoBehaviour
    {
        private static GameObject staticInfoWindow;
        private static GameObject staticInfoPanel;
        
        private static GameObject staticSettingsPanel;
        public GameObject cloudSaveButton;

        private static AutoSetter titleSetter;
        private static AutoSetter descSetter;

        private readonly EventManager eventManager = EventManager.Instance;
        public GameObject buildingInfo;
        public GameObject buildingOptions;
        private DropMe buildingScript;
        private BuildingsAbstract buildingSelected;
        public GameObject buildingStats;

        public GameObject CityHallInfo;
        private GameObject currCityHallView;

        public GameObject buildingUpgradePrefab;

        private bool clock;
        public GameObject energyZone;
        public GameObject infoWindow;
        private bool MissionCompleted;
        public Color MissionCompletedColor;

        public Image MissionImage;
        public GameObject missionPrefab;
        public GameObject missionsWindow;
        public GameObject movingScreen;
        public Color NoMissionCompletedColor;
        public GameObject roadOptions;
        private RoadGrid roadScript;

        private GameObject statsPanel;

        public GameObject upgradeWindow;
        public GameObject upgradeInfosWindow;
        private GameObject upgradePanel;
        private Button upgradeButton;

        public GameObject confirmSellBuildingWindow;

        public GameObject cityNameWindow;

        public Canvas buildingBar;
        public Canvas expBar;

        public Sprite[] weatherIcons;
        void Start()
        {
            eventManager.Subscribe(ErrorEvent);
            eventManager.Subscribe(CollectedMissionEvent);
            eventManager.Subscribe(MissionUpdate);
            staticSettingsPanel = gameObject.transform.Find("SettingsMenu").gameObject;
            staticInfoPanel = infoWindow.transform.Find("InfoPanel").gameObject;
            upgradePanel = upgradeWindow.transform.Find("UpgradePanel").gameObject;
            statsPanel = buildingStats.transform.Find("BuildingStatsPanel").gameObject;
            staticInfoWindow = infoWindow;
            titleSetter = new AutoSetter("DEFAULT",
                staticInfoPanel.transform.Find("Title").GetComponent<TextMeshProUGUI>());
            descSetter = new AutoSetter("DEFAULT",
                staticInfoPanel.transform.Find("Description").GetComponent<TextMeshProUGUI>());
            InvokeRepeating(nameof(blinkMissionCompleted), 0, 0.5f);
            MissionUpdate(new MissionUpdateEvent());
            cloudSaveButton.SetActive(GPGS.Instance.logged);
        }

        public void blinkMissionCompleted()
        {
            if (!MissionCompleted) return;
            clock = !clock;
            MissionImage.color = clock ? MissionCompletedColor : NoMissionCompletedColor;
        }

        public void MissionUpdate(MissionUpdateEvent e)
        {
            GameManager.Instance.GetCurrentMissions().ForEach(mission =>
            {
                if (mission.isCompleted())
                    MissionCompleted = true;
            });
        }

        public static void ShowInfoMessage(string titleId = "WDMANAGER_TITLE_DEFAULT",
            string descId = "WDMANAGER_DESC_DEFAULT", params object[] descVars)
        {
            titleSetter.SetId(titleId);
            descSetter.SetId(descId);
            staticInfoWindow.GetComponent<UIController>().Show();
        }

        public static void HideInfoMessage()
        {
            staticInfoWindow.GetComponent<UIController>().Hide();
        }

        private void ErrorEvent(GameErrorEvent evt)
        {
            ShowInfoMessage(evt.GetTitleId(), evt.GetMessageId(), evt.GetVars());
        }

        public void MenuButton()
        {
            SceneManager.LoadScene(1);
        }

        public void SwitchToFr()
        {
            Tr.SelectLanguage(Tr.Languages.francais);
        }
        
        public void SwitchToEn()
        {
            Tr.SelectLanguage(Tr.Languages.anglais);
        }

        public void ShowBuildingOptions(DropMe script)
        {
            HideEverything();
            if (buildingScript && buildingScript.transform.childCount == 2)
            {
                Destroy(buildingScript.transform.GetChild(1).gameObject);
            }

            buildingScript = script;
            buildingScript.highlight.enabled = true;
            buildingSelected = buildingScript.GetBuilding();
            buildingOptions.GetComponent<UIController>().Show();
            if (buildingSelected.getEnergyGenerate() > 0)
            {
                GameObject newEnergyZone = Instantiate(energyZone, buildingScript.transform);
                newEnergyZone.transform.SetParent(buildingScript.transform, false);
                newEnergyZone.GetComponent<Image>().sprite =
                    Resources.Load<Sprite>("UI/EnergyZone_" + buildingSelected.getBuildingType());
            }

            //don't show upgrade button if there is no update available
            buildingOptions.transform.Find("UpgradeBuildingButton").gameObject
                .SetActive(buildingSelected.getAvailableUpdagrade().Count > 0);
            //disable Delete button if it's the greenHouse
            buildingOptions.transform.Find("DeleteBuildingButton").gameObject
                .SetActive(!buildingSelected.getBuildingType().Equals(BuildingType.GREEN_HOUSE) &&
                           !buildingSelected.getBuildingType().Equals(BuildingType.TOWN_HALL));

            //ShowBuildingInfo();
        }

        public void ShowRoadOptions(RoadGrid script)
        {
            HideEverything();
            roadScript = script;
            roadScript.receivingImage.color = roadScript.highlightColor;
            roadOptions.GetComponent<UIController>().Show();
        }

        public void HideRoadOptions()
        {
            roadScript.receivingImage.color = Color.white;
            roadScript = null;
            roadOptions.SetActive(false);
        }

        public void RotateRoad()
        {
            roadScript.RotateRoad();
        }

        public void DeleteRoad()
        {
            roadScript.RemoveRoad();
            HideEverything();
        }

        public void ShowBuildingStats(BuildingType type)
        {
            HideEverything();
            buildingStats.GetComponent<UIController>().Show();

            var building = GameManager.AllBuildings[type];

            statsPanel.transform.Find("NamePlank").transform.Find("NameLabel").GetComponent<TextMeshProUGUI>().text =
                building.getName();

            GameObject stats = statsPanel.transform.Find("StatsText").gameObject;


            var citizenStat = stats.transform.Find("CitizenStat");
            if (building.getCitizens() > 0)
            {
                citizenStat.gameObject.SetActive(true);
                citizenStat.GetComponent<TextMeshProUGUI>().text =
                    "Citizens: " + building.getCitizens();
            }
            else
            {
                citizenStat.gameObject.SetActive(false);
            }

            var pollutionStat = stats.transform.Find("PollutionStat");
            if (building.getPollution() != 0)
            {
                pollutionStat.gameObject.SetActive(true);
                pollutionStat.GetComponent<TextMeshProUGUI>().text =
                    "Pollution: " + building.getPollution();
            }
            else
            {
                pollutionStat.gameObject.SetActive(false);
            }

            var moneyStat = stats.transform.Find("MoneyStat");
            if (building.getMoneyPerMinute() > 0)
            {
                moneyStat.gameObject.SetActive(true);
                moneyStat.GetComponent<TextMeshProUGUI>().text =
                    "Money per minute: " + building.getMoneyPerMinute();
            }
            else
            {
                moneyStat.gameObject.SetActive(false);
            }

            var energyGeneratedStat = stats.transform.Find("EnergyGeneratedStat");
            if (building.getEnergyGenerate() > 0)
            {
                energyGeneratedStat.gameObject.SetActive(true);
                energyGeneratedStat.GetComponent<TextMeshProUGUI>().text =
                    "Energy generated: " + building.getEnergyGenerate();
            }
            else
            {
                energyGeneratedStat.gameObject.SetActive(false);
            }

            var energyNeededStat = stats.transform.Find("EnergyNeededStat");
            if (building.getEnergyNeeded() > 0)
            {
                energyNeededStat.gameObject.SetActive(true);
                energyNeededStat.GetComponent<TextMeshProUGUI>().text =
                    "Energy needed: " + building.getEnergyNeeded();
            }
            else
            {
                energyNeededStat.gameObject.SetActive(false);
            }

            var costStat = stats.transform.Find("CostStat");
            if (building.getPrice() > 0)
            {
                costStat.gameObject.SetActive(true);
                costStat.GetComponent<TextMeshProUGUI>().text =
                    "Cost: " + building.getPrice();
            }
            else
            {
                costStat.gameObject.SetActive(false);
            }
        }

        public void HideBuildingStats()
        {
            buildingStats.GetComponent<UIController>().Hide();
        }

        public void HideBuildingOptions()
        {
            if (buildingScript.transform.childCount == 2)
            {
                Destroy(buildingScript.transform.GetChild(1).gameObject);
            }

            if (DropMe.moving == false)
            {
                buildingScript.highlight.enabled = false;
                buildingScript = null;
                buildingSelected = null;
            }

            buildingOptions.GetComponent<UIController>().Hide();
        }

        public void ShowBuildingInfo()
        {
            if (buildingSelected.getBuildingType().Equals(BuildingType.TOWN_HALL)) {
                ShowCityHallInfo();
                return;
            }
            HideWindows();
            buildingInfo.GetComponent<UIController>().Show();
            var infoPanel = buildingInfo.transform.Find("BuildingInfoPanel").gameObject;

            var nameLabel = infoPanel.transform.Find("NamePlank").gameObject.transform.Find("NameLabel")
                .GetComponent<TextMeshProUGUI>();
            nameLabel.text = buildingSelected.getName();

            var stats = infoPanel.transform.Find("StatsText").gameObject;

            var collectMoney = infoPanel.transform.Find("CollectMoney");


            collectMoney.gameObject.SetActive(true);
            collectMoney.transform.Find("MoneyStat").GetComponent<TextMeshProUGUI>().text =
                $"Money available to collect:\n{buildingSelected.getMoneyToCollect()}/{buildingSelected.getMaxMoneyToCollect()}";

            var citizenStat = stats.transform.Find("CitizenStat");
            if (buildingSelected.getCitizens() > 0)
            {
                citizenStat.gameObject.SetActive(true);
                citizenStat.GetComponent<TextMeshProUGUI>().text =
                    "Citizens: " + buildingSelected.getCitizens();
            }
            else
            {
                citizenStat.gameObject.SetActive(false);
            }

            var pollutionStat = stats.transform.Find("PollutionStat");
            if (buildingSelected.getPollution() != 0)
            {
                pollutionStat.gameObject.SetActive(true);
                pollutionStat.GetComponent<TextMeshProUGUI>().text =
                    "Pollution: " + buildingSelected.getPollution();
            }
            else
            {
                pollutionStat.gameObject.SetActive(false);
            }

            var energyGeneratedStat = stats.transform.Find("EnergyGeneratedStat");
            if (buildingSelected.getEnergyGenerate() > 0)
            {
                energyGeneratedStat.gameObject.SetActive(true);
                energyGeneratedStat.GetComponent<TextMeshProUGUI>().text =
                    "Energy generated: " + buildingSelected.getEnergyGenerate();
            }
            else
            {
                energyGeneratedStat.gameObject.SetActive(false);
            }

            var energyNeededStat = stats.transform.Find("EnergyNeededStat");
            if (buildingSelected.getEnergyNeeded() > 0)
            {
                energyNeededStat.gameObject.SetActive(true);
                energyNeededStat.GetComponent<TextMeshProUGUI>().text =
                    "Energy needed: " + buildingSelected.getEnergyNeeded();
            }
            else
            {
                energyNeededStat.gameObject.SetActive(false);
            }

            var levelStat = stats.transform.Find("LevelStat");
            if (buildingSelected.getBuildingLevel() > 0)
            {
                levelStat.gameObject.SetActive(true);
                levelStat.GetComponent<TextMeshProUGUI>().text =
                    "Level: " + buildingSelected.getBuildingLevel();
            }
            else
            {
                levelStat.gameObject.SetActive(false);
            }

            var greenHouseAccess = infoPanel.transform.Find("GreenHouseAccess");
            var experienceStat = stats.transform.Find("ExperienceStat");
            var buildingWithoutFullEnergyStat = stats.transform.Find("BuildingWithoutFullEnergyStat");
            var moneyPerHourStat = stats.transform.Find("MoneyPerHourStat");
            if (buildingSelected.getBuildingType().Equals(BuildingType.GREEN_HOUSE))
            {
                energyNeededStat.gameObject.SetActive(false);
                energyGeneratedStat.gameObject.SetActive(false);
                pollutionStat.gameObject.SetActive(false);
                citizenStat.gameObject.SetActive(false);
                collectMoney.gameObject.SetActive(false);
                experienceStat.gameObject.SetActive(false);
                moneyPerHourStat.gameObject.SetActive(false);
                buildingWithoutFullEnergyStat.gameObject.SetActive(false);

                greenHouseAccess.gameObject.SetActive(true);
            }
            else if (buildingSelected.getBuildingType().Equals(BuildingType.TOWN_HALL))
            {
                energyNeededStat.gameObject.SetActive(false);
                energyGeneratedStat.gameObject.SetActive(false);
                pollutionStat.gameObject.SetActive(false);
                citizenStat.gameObject.SetActive(false);
                collectMoney.gameObject.SetActive(false);

                levelStat.gameObject.SetActive(true);
                experienceStat.gameObject.SetActive(true);
                moneyPerHourStat.gameObject.SetActive(true);
                buildingWithoutFullEnergyStat.gameObject.SetActive(true);
                greenHouseAccess.gameObject.SetActive(false);


                var townHall = GameManager.Instance.GetTownHall();

                levelStat.GetComponent<TextMeshProUGUI>().text =
                    $"Level: {townHall.GetLevel()}";
                experienceStat.GetComponent<TextMeshProUGUI>().text =
                    $"Experience: {townHall.GetExperience()}/{townHall.GetExperienceToNextLevel()}";
                buildingWithoutFullEnergyStat.GetComponent<TextMeshProUGUI>().text =
                    $"Building without full energy: {townHall.GetNumberOfBuildingWithoutFullEnergy()}";
                moneyPerHourStat.GetComponent<TextMeshProUGUI>().text =
                    $"Money/hour: {townHall.GetTotalMoneyPerHour()}";
            }
            else
            {
                experienceStat.gameObject.SetActive(false);
                greenHouseAccess.gameObject.SetActive(false);
                moneyPerHourStat.gameObject.SetActive(false);
                buildingWithoutFullEnergyStat.gameObject.SetActive(false);
                collectMoney.gameObject.SetActive(buildingSelected.getMoneyPerMinute() > 0);
            }
        }

        public void GetBuildingMoney()
        {
            buildingScript.CollectMoney();
            HideWindows();
        }

        public void HideBuildingInfo()
        {
            buildingInfo.GetComponent<UIController>().Hide();
        }

        public void ShowBuildingUpgrade()
        {
            HideWindows();
            upgradeWindow.GetComponent<UIController>().Show();
            Image buildingSprite = upgradePanel.transform.Find("BuildingSprite").GetComponent<Image>();
            buildingSprite.sprite = Resources.Load<Sprite>("UI/" + buildingSelected.getBuildingType());
            upgradePanel.transform.Find("Title").GetComponent<TextMeshProUGUI>().text = buildingSelected.getName();


            if (buildingScript.hasUpgrade)
            {
                GameObject noMoreUpgrades = upgradePanel.transform.Find("NoMoreUpgradeLabel").gameObject;
                noMoreUpgrades.SetActive(false);
                GameObject upgradeGrid =
                    upgradePanel.transform.Find("UpgradeList").transform.Find("UpgradeGrid").gameObject;
                GameObject children;
                Button upgradeInfosWindowButton;
                foreach (BuildingType building in buildingSelected.getAvailableUpdagrade())
                {
                    children = Instantiate(buildingUpgradePrefab, upgradeGrid.transform);
                    children.transform.Find("NewBuildingSprite").GetComponent<Image>().sprite =
                        Resources.Load<Sprite>("UI/" + building);
                    children.transform.Find("BuildingName").GetComponent<TextMeshProUGUI>().text =
                        building.getDefaultName();

                    upgradeInfosWindowButton = children.GetComponent<Button>();
                    upgradeInfosWindowButton.onClick.AddListener(() => ShowBuildingInfosUpgrade(building));
                }
            }
        }

        public void ShowBuildingInfosUpgrade(BuildingType type)
        {
            upgradeInfosWindow.GetComponent<UIController>().Show();

            upgradeInfosWindow.transform.Find("BuildingDisplay").transform.Find("BuildingSprite").GetComponent<Image>()
                    .sprite =
                Resources.Load<Sprite>("UI/" + type);

            var building = GameManager.AllBuildings[type];

            upgradeInfosWindow.transform.Find("BuildingName").GetComponent<TextMeshProUGUI>().text =
                building.getName();

            GameObject stats = upgradeInfosWindow.transform.Find("StatsText").gameObject;


            var citizenStat = stats.transform.Find("CitizenStat");
            if (building.getCitizens() > 0)
            {
                citizenStat.gameObject.SetActive(true);
                citizenStat.GetComponent<TextMeshProUGUI>().text =
                    "Citizens: " + building.getCitizens();
            }
            else
            {
                citizenStat.gameObject.SetActive(false);
            }

            var pollutionStat = stats.transform.Find("PollutionStat");
            if (building.getPollution() != 0)
            {
                pollutionStat.gameObject.SetActive(true);
                pollutionStat.GetComponent<TextMeshProUGUI>().text =
                    "Pollution: " + building.getPollution();
            }
            else
            {
                pollutionStat.gameObject.SetActive(false);
            }

            var moneyStat = stats.transform.Find("MoneyStat");
            if (building.getMoneyPerMinute() > 0)
            {
                moneyStat.gameObject.SetActive(true);
                moneyStat.GetComponent<TextMeshProUGUI>().text =
                    "Money per minute: " + building.getMoneyPerMinute();
            }
            else
            {
                moneyStat.gameObject.SetActive(false);
            }

            var energyGeneratedStat = stats.transform.Find("EnergyGeneratedStat");
            if (building.getEnergyGenerate() > 0)
            {
                energyGeneratedStat.gameObject.SetActive(true);
                energyGeneratedStat.GetComponent<TextMeshProUGUI>().text =
                    "Energy generated: " + building.getEnergyGenerate();
            }
            else
            {
                energyGeneratedStat.gameObject.SetActive(false);
            }

            var energyNeededStat = stats.transform.Find("EnergyNeededStat");
            if (building.getEnergyNeeded() > 0)
            {
                energyNeededStat.gameObject.SetActive(true);
                energyNeededStat.GetComponent<TextMeshProUGUI>().text =
                    "Energy needed: " + building.getEnergyNeeded();
            }
            else
            {
                energyNeededStat.gameObject.SetActive(false);
            }

            var costStat = stats.transform.Find("CostStat");
            if (building.getPrice() > 0)
            {
                costStat.gameObject.SetActive(true);
                costStat.GetComponent<TextMeshProUGUI>().text =
                    "Cost: " + building.getPrice();
            }
            else
            {
                costStat.gameObject.SetActive(false);
            }

            upgradeButton = upgradeInfosWindow.transform.Find("UpgradeButton").GetComponent<Button>();
            Debug.Log("building: " + type.getDefaultName());
            upgradeButton.onClick.RemoveAllListeners();
            upgradeButton.onClick.AddListener(() => buildingScript.UpgradeBuilding(type));
        }

        public void HideBuildingUpgrade()
        {
            upgradeWindow.GetComponent<UIController>().Hide();
            GameObject upgradeGrid =
                upgradePanel.transform.Find("UpgradeList").transform.Find("UpgradeGrid").gameObject;
            foreach (Transform children in upgradeGrid.transform)
            {
                Destroy(children.gameObject);
            }
        }

        public void MoveBuilding()
        {
            HideWindows();
            DropMe.moving = true;
            DropMe.movingBuilding = buildingSelected;
            Debug.Log(buildingScript);
            HideBuildingOptions();
            ShowMovingScreen();
        }

        public void ShowConfirmSellBuilding() {
            confirmSellBuildingWindow.GetComponent<UIController>().Show();

            var sellBuildingPanel = confirmSellBuildingWindow.transform.Find("SellBuildingPanel").gameObject;

            var buildingSprite = sellBuildingPanel.transform.Find("BuildingSprite").GetComponent<Image>();
            buildingSprite.sprite = Resources.Load<Sprite>("UI/" + buildingSelected.getBuildingType());

            var goldValue = sellBuildingPanel.transform.Find("GoldValue").GetComponent<TextMeshProUGUI>();
            var moneyRefund = buildingSelected.getPrice() * GameManager.Instance.GetMoneyRefundPercentage();
            goldValue.text = moneyRefund.ToString("F0");

        }

        public void SellBuilding()
        {
            buildingScript.SellBuilding();
            HideWindows();
        }

        public void HideConfirmSellBuilding() {
            confirmSellBuildingWindow.GetComponent<UIController>().Hide();
        }
        
        public void ShowCityHallInfo()
        {
            HideWindows();
            CityHallInfo.GetComponent<UIController>().Show();
            var infoPanel = CityHallInfo.transform.Find("CityHallInfoPanel").gameObject;
            
            var townHall = GameManager.Instance.GetTownHall();
                    
            var cityName = infoPanel.transform.Find("CityName").GetComponent<TextMeshProUGUI>();
            cityName.text = String.IsNullOrEmpty(townHall.GetCityName()) ? "Your city name" : townHall.GetCityName();


            var statsView = infoPanel.transform.Find("StatsView").gameObject;
            ShowCityHallView(statsView);

        }

        public void ShowCityHallView(GameObject view) {
            if (currCityHallView)
                currCityHallView.SetActive(false);
            view.SetActive(true);
            switch (view.name) {
                case "StatsView":
                    var stats = view.transform.Find("StatsText").gameObject;

                    var levelStat = stats.transform.Find("LevelStat");
                    var experienceStat = stats.transform.Find("ExperienceStat");
                    var buildingWithoutFullEnergyStat = stats.transform.Find("BuildingWithoutFullEnergyStat");
                    var moneyPerHourStat = stats.transform.Find("MoneyPerHourStat");

                    levelStat.gameObject.SetActive(true);
                    experienceStat.gameObject.SetActive(true);
                    moneyPerHourStat.gameObject.SetActive(true);
                    buildingWithoutFullEnergyStat.gameObject.SetActive(true);


                    var townHall = GameManager.Instance.GetTownHall();

                    var levelStatText = levelStat.GetComponent<TextMeshProUGUI>();
                    var levelStatSetter = new AutoSetter("CITY_HALL_LEVEL_STAT", levelStatText);
                    levelStatText.text += $" {townHall.GetLevel()}";
                    //levelStat.GetComponent<TextMeshProUGUI>().text =
                    //    $"Level: {townHall.GetLevel()}";
                    var experienceStatText = experienceStat.GetComponent<TextMeshProUGUI>();
                    var experienceStatSetter = new AutoSetter("CITY_HALL_EXPERIENCE_STAT", experienceStatText);
                    experienceStatText.text += $" {townHall.GetExperience() / townHall.GetExperienceToNextLevel()}";
                    //experienceStat.GetComponent<TextMeshProUGUI>().text =
                    //    $"Experience: {townHall.GetExperience()}/{townHall.GetExperienceToNextLevel()}";
                    var buildingWithoutFullEnergyStatText = buildingWithoutFullEnergyStat.GetComponent<TextMeshProUGUI>();
                    var buildingWithoutFullEnergyStatSetter = new AutoSetter("CITY_HALL_BUILDING_WITHOUT_FULL_ENERGY_STAT", buildingWithoutFullEnergyStatText);
                    buildingWithoutFullEnergyStatText.text += $" {townHall.GetNumberOfBuildingWithoutFullEnergy()}";
                   // buildingWithoutFullEnergyStat.GetComponent<TextMeshProUGUI>().text =
                   //     $"Building without full energy: {townHall.GetNumberOfBuildingWithoutFullEnergy()}";
                    var moneyPerHourStatText = moneyPerHourStat.GetComponent<TextMeshProUGUI>();
                    var moneyPerHourStatSetter = new AutoSetter("CITY_HALL_MONEY_PER_HOUR_STAT", moneyPerHourStatText);
                    moneyPerHourStatText.text += $" {townHall.GetTotalMoneyPerHour()}";
                    //moneyPerHourStat.GetComponent<TextMeshProUGUI>().text =
                    //    $"Money/hour: {townHall.GetTotalMoneyPerHour()}";
                    break;
                case "WeatherView":
                    var weatherSchedule = GameManager.Instance.GetWeatherSchedule();
                    for (var i = 0; i < 5; i++) {
                        var weatherPanel = view.transform.Find("WeatherPanel_" + i).gameObject;
                        var weatherSprite = weatherPanel.GetComponent<Image>();
                        var weatherText = weatherPanel.GetComponentInChildren<TextMeshProUGUI>();
                        Debug.Log(weatherSchedule[i]);
                        AutoSetter setter;
                        switch (weatherSchedule[i]) {
                            case WeatherState.SUNNY:
                                weatherSprite.sprite = weatherIcons[0];
                                setter = new AutoSetter("WEATHER_SUN", weatherText);
                                //weatherText.text = "Soleil";
                                break;
                            case WeatherState.WINDY:
                                weatherSprite.sprite = weatherIcons[1];
                                setter = new AutoSetter("WEATHER_WIND", weatherText);
                               // weatherText.text = "Vent";
                                break;
                            case WeatherState.RAINY:
                                weatherSprite.sprite = weatherIcons[2];
                                setter = new AutoSetter("WEATHER_RAIN", weatherText);
                                //weatherText.text = "Pluie";
                                break;
                            case WeatherState.STORMY:
                                weatherSprite.sprite = weatherIcons[3];
                                setter = new AutoSetter("WEATHER_STORM", weatherText);
                                //weatherText.text = "Orage";
                                break;
                            default:
                                weatherSprite.sprite = weatherIcons[0];
                                setter = new AutoSetter("WEATHER_SUN", weatherText);
                                //weatherText.text = "Soleil";
                                break;
                        }
                        
                    }
                    break;
                default:
                    Debug.Log("default");
                    break;
            }
            currCityHallView = view;
        }

        public void HideCityHallInfo() {
            currCityHallView.SetActive(false);
            CityHallInfo.GetComponent<UIController>().Hide();
        }

        public void ShowMissions()
        {
            if (missionsWindow.activeSelf) return;
            MissionCompleted = false;
            MissionImage.color = NoMissionCompletedColor;
            missionsWindow.GetComponent<UIController>().Show();
            var missionGrid = missionsWindow.transform.Find("MissionPanel").transform.Find("MissionsList").transform
                .Find("MissionsGrid").gameObject;
            GameObject missionObject;
            Button collectButton;
            var index = 0;
            GameManager.Instance.GetCurrentMissions().ForEach(mission =>
            {
                missionObject = Instantiate(missionPrefab, missionGrid.transform);
                var uGui = missionObject.transform.Find("MissionTitle").GetComponent<TextMeshProUGUI>();
                var setter = new AutoSetter(mission.getMissionTextId(), uGui);
//                missionObject.transform.Find("MissionTitle").GetComponent<TextMeshProUGUI>().text =
//                    mission.getMissionTextId();
                collectButton = missionObject.transform.Find("MissionCollectButton").GetComponent<Button>();
                var suffixTExt = "";
                var requirementText = "";
                string prefixText;
                mission.getMissionRequirements().ForEach(requirement =>
                {
                    switch (requirement.getMissionRequirementType())
                    {
                        case MissionRequirementType.EARN_MONEY:
                            prefixText = "Earn money";
                            break;
                        case MissionRequirementType.GROW_PLANT:
                            prefixText = "Harvest";
                            suffixTExt = "any type of plant";

                            break;
                        case MissionRequirementType.POLLUTION:
                            prefixText = "Pollution";
                            break;
                        case MissionRequirementType.POPULATION:
                            prefixText = "Population";
                            break;
                        case MissionRequirementType.GO_TO_AR:
                            prefixText = "Go to the AR View";
                            break;
                        case MissionRequirementType.GO_TO_2D:
                            prefixText = "Go to the 2D View";
                            break;
                        case MissionRequirementType.GO_TO_GREENHOUSE:
                            prefixText = "Go to the GreenHouse";
                            break;
                        case MissionRequirementType.ADD_BUILDING:
                            prefixText = "Add building";
                            suffixTExt = ((AddBuildingMissionRequirement) requirement).getBuildingType()
                                .getDefaultName();
                            break;
                        case MissionRequirementType.BUILDING_OWNED:
                            prefixText = "Building owned";
                            suffixTExt = ((BuildingOwnedMissionRequirement) requirement).getBuildingType()
                                .getDefaultName();
                            break;
                        case MissionRequirementType.MISSION_DONE_COUNT:
                            prefixText = "Mission done";
                            break;
                        case MissionRequirementType.TOTAL_MONEY:
                            prefixText = "Total money";
                            break;
                        case MissionRequirementType.PLANT_SEED:
                            prefixText = "Plant";
                            suffixTExt = "any type of seed";
                            break;
                        case MissionRequirementType.ADD_ROAD:
                            prefixText = "Add roads";
                            suffixTExt = requirement.getNeededCount() > 1 ? "Roads" : "Road";
                            break;
                        default:
                            prefixText = "Default";
                            break;
                    }

                    var need = requirement.getNeededCount() > 0
                        ? $"{requirement.getCurrentCount()}/{requirement.getNeededCount()}"
                        : "";

                    var tmpRequirementText =
                        need.Length > 0 ? $" - {prefixText}\n    {need} {suffixTExt}" : $" - {prefixText}\n";
                    if (requirement.isAchieved())
                    {
                        tmpRequirementText = $"<color=#00CA00><b>{tmpRequirementText}</b></color>";
                    }

                    requirementText += tmpRequirementText;
                    missionObject.transform.Find("MissionDescription").GetComponent<TextMeshProUGUI>().text =
                        requirementText;
                });
                var i = index;
                collectButton.onClick.AddListener(() => CollectMission(i));
                if (!mission.isCompleted())
                {
                    collectButton.interactable = false;
                }

                index++;
            });
        }

        private void CollectMission(int index)
        {
            print("index in collect mission : " + index);
            var missionCollect = new CollectMissionEvent();
            missionCollect.setMissionIndex(index);
            eventManager.Raise(missionCollect);
        }

        private void CollectedMissionEvent(MissionCollectedEvent evt)
        {
            HideMissions();
            MissionCompleted = false;
            MissionImage.color = NoMissionCompletedColor;
        }

        public void HideMissions()
        {
            missionsWindow.GetComponent<UIController>().Hide();
            GameObject missionGrid = missionsWindow.transform.Find("MissionPanel").transform.Find("MissionsList")
                .transform.Find("MissionsGrid").gameObject;
            foreach (Transform children in missionGrid.transform)
            {
                Destroy(children.gameObject);
            }
        }

        public void ShowMovingScreen()
        {
            buildingBar.enabled = false;
            expBar.enabled = false;
            movingScreen.SetActive(true);
        }

        public void HideMovingScreen()
        {
            buildingBar.enabled = true;
            expBar.enabled = true;
            movingScreen.SetActive(false);
            buildingScript.highlight.enabled = false;
            buildingScript = null;
            buildingSelected = null;
        }

        public void ShowCityName()
        {
            if (cityNameWindow.activeSelf) return;
            cityNameWindow.GetComponent<UIController>().Show();
        }

        public void HideCityName()
        {
            cityNameWindow.GetComponent<UIController>().Hide();
        }

        public bool IsInfoMessageVisible()
        {
            return infoWindow.activeSelf;
        }

        public bool IsBuildingUpgradeVisible()
        {
            return upgradeWindow.activeSelf;
        }

        public bool IsBuildingOptionsVisible()
        {
            return buildingOptions.activeSelf;
        }

        public bool IsBuildingInfoVisible()
        {
            return buildingInfo.activeSelf;
        }

        public bool IsRoadOptionsVisible()
        {
            return roadOptions.activeSelf;
        }

        public bool IsBuildingStatsVisible()
        {
            return buildingStats.activeSelf;
        }

        public bool IsMissionWindowVisible()
        {
            return missionsWindow.activeSelf;
        }

        public bool IsCityNameWindowVisible()
        {
            return cityNameWindow.activeSelf;
        }

        public bool IsCityHallInfoVisible() {
            return CityHallInfo.activeSelf;
        }

        public bool IsConfirmSellBuildingVisible() {
            return confirmSellBuildingWindow.activeSelf;
        }

        public void HideWindows()
        {
            if (IsBuildingUpgradeVisible())
                HideBuildingUpgrade();
            if (IsInfoMessageVisible())
                HideInfoMessage();
            if (IsBuildingInfoVisible())
                HideBuildingInfo();
            if (IsRoadOptionsVisible())
                HideRoadOptions();
            if (IsBuildingStatsVisible())
                HideBuildingStats();
            if (IsMissionWindowVisible())
                HideMissions();
            if (IsCityNameWindowVisible())
                HideCityName();
            if (IsCityHallInfoVisible())
                HideCityHallInfo();
            if (IsConfirmSellBuildingVisible())
                HideConfirmSellBuilding();
        }

        public void HideEverything()
        {
            HideWindows();
            if (IsBuildingOptionsVisible())
                HideBuildingOptions();
        }
        
        public void OpenCloudSave()
        {
            staticSettingsPanel.GetComponent<UIController>().Hide();
            Invoke(nameof(DelayScreenShot), 0.25f);
            Invoke(nameof(DelayCloudSave), 0.40f);
        }

        private void DelayScreenShot()
        {
            StartCoroutine(GPGS.Instance.CaptureScreenShot());
        }

        private void DelayCloudSave()
        {
            GPGS.Instance.OpenCloudSave();

        }
    }
}