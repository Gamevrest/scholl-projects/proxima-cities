﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Tools
{
    public class DevConsole : MonoBehaviour
    {
        public GameObject varPrefab;
        public Text console;
        public RectTransform varsTransform;
        public GameObject transformToHide;
        public GameObject ConsoleButton;
        private readonly Dictionary<string, GameObject> _varsDictionary = new Dictionary<string, GameObject>();
        private static Queue<Tuple<string, string>> logQueue = new Queue<Tuple<string, string>>();


        private void Start()
        {
            var a = false;
#if DEBUG
            a = true;
            Application.logMessageReceived += ApplicationOnLogMessageReceived;
#endif
            gameObject.SetActive(a);
            DontDestroyOnLoad(this);
        }

        private void ApplicationOnLogMessageReceived(string condition, string stacktrace, LogType type)
        {
            if (type != LogType.Log) Log(condition);
        }

#if DEBUG
        private void Update()
        {
            while (logQueue.Count > 0)
            {
                var (log, varKey) = logQueue.Dequeue();
                InnerLog(log, varKey);
            }
        }
#endif

        public void SwitchConsole()
        {
            transformToHide.SetActive(!transformToHide.activeSelf);
        }

        public static void Log(string log, string varKey = null)
        {
            Debug.Log($"'{varKey}' : '{log}'");
#if DEBUG
            logQueue.Enqueue(new Tuple<string, string>(log, varKey));
#endif
        }

        private string InnerLog(string log, string varKey = null)
        {
            if (varKey == null)
            {
                console.text += $"\n{log}";
                return log;
            }

            if (string.IsNullOrEmpty(log) && _varsDictionary.ContainsKey(varKey))
            {
                Destroy(_varsDictionary[varKey]);
                _varsDictionary.Remove(varKey);
                return log;
            }

            if (!_varsDictionary.ContainsKey(varKey))
                _varsDictionary.Add(varKey, Instantiate(varPrefab, varsTransform));
            var tmp = _varsDictionary[varKey];
            tmp.GetComponent<Text>().text = $"{varKey} : {log}";
            return log;
        }
    }
}