﻿using UnityEngine;

namespace Tools
{
    public class Utils
    {
        public static float RoundFloat(float nbToRound, int decimalsToKeep = 1)
        {
            var div = Mathf.Pow(10, decimalsToKeep);
            return (int) (nbToRound * div) / div;
        }

        public static void CleanChildren(Transform parent)
        {
            foreach (Transform child in parent)
            {
                Object.Destroy(child.gameObject);
            }
        }
    }
}