using System;

public class EventNotFoundException : GamevrestException
{
    public EventNotFoundException(String type)
        : base(String.Format("Event type not found {0}", type),
            "ERROR_EVENT", type)
    {
    }
}