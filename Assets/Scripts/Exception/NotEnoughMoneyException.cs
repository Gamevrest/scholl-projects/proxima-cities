public class NotEnoughMoneyException : GamevrestException
{

    public NotEnoughMoneyException()
        : base("Not enought money",
            "ERROR_NOTENOUGHMONEY")
    {

    }
}