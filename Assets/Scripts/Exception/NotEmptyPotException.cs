public class NotEmptyPotException : GamevrestException
{
    public NotEmptyPotException()
        : base($"You cannot collect this pot, you must wait and collect the mutation",
            "ERROR_NOTEMPTYPOT")
    {
    }

}