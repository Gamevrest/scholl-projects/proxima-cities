public class EmptyPotException : GamevrestException
{
    private int _potIndex;
    public EmptyPotException(int potIndex)
        : base($"The pot index [{potIndex}] is empty",
            "ERROR_EMPTYPOT", potIndex)
    {
        _potIndex = potIndex;
    }

    public int getPotIndex()
    {
        return _potIndex;
    }
}