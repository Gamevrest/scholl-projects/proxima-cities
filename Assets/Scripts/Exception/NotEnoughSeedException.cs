public class NotEnoughSeedException : GamevrestException
{
    private SeedType _type;

    public NotEnoughSeedException(SeedType type)
        : base($"You don't have enough seed of type [{type}]",
            "ERROR_NOTENOUGHSEED", type)
    {
        _type = type;
    }

    public SeedType getSeedType()
    {
        return _type;
    }
}