using UnityEngine;

public class NotEmptyPositionException : GamevrestException
{
    private Vector2 position;
    public NotEmptyPositionException(Vector2 newPosition)
        : base($"Not Empty position x:{newPosition.x} y:{newPosition.y}",
            "ERROR_NOTEMPTY", newPosition.x, newPosition.y)
    {
        position = newPosition;
    }

    public Vector2 getPosition()
    {
        return position;
    }
}