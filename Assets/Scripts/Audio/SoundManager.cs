﻿using UnityEngine;

public class SoundManager : MonoBehaviour {

    public AudioSource musicPlayer;
    public AudioSource sfxPlayer;

    [Header("MUSIC")] 
    public AudioClip gameMusic;

    private void Start() {
        musicPlayer.clip = gameMusic;
        musicPlayer.Play();
    }
}
