﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GreenHouseWeather : MonoBehaviour
{
    GameManager _gameManager = GameManager.Instance;
    public Sprite[] weather;
    private Image img;
    void OnEnable()
    {
        img = GetComponent<Image>();
        EventManager.Instance.Subscribe(OnWeatherChanged);
        img.sprite = weather[(int)_gameManager.GetCurrentWeather() - 1];
    }

    private void OnWeatherChanged(WeatherChangeEvent evt)
    {
        img.sprite = weather[(int)evt.GetWeather() - 1];
    }

}
