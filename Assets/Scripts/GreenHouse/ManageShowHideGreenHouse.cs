﻿using UnityEngine;

public class ManageShowHideGreenHouse : MonoBehaviour
{

    public GameObject GreenHouseCanvas;
    public GameObject QuitGreenHouseButton;
    private void Start()
    {
        EventManager.Instance.Subscribe(EnableGreenHouse);
        EventManager.Instance.Subscribe(DisableGreenHouse);
    }

    private void EnableGreenHouse(SwitchToGreenHouseEvent e)
    {
        GreenHouseCanvas.SetActive(true);
        QuitGreenHouseButton.SetActive(true);
        EventManager.Instance.Raise(new StopLoadingEvent());
    }
    
    private void DisableGreenHouse(SwitchTo2DEvent e)
    {
        GreenHouseCanvas.SetActive(false);
        QuitGreenHouseButton.SetActive(false);
    }

    public void QuitGreenHouse()
    {
        EventManager.Instance.Raise(new StartLoadingEvent());
        EventManager.Instance.Raise(new SwitchTo2DEvent());
    }

    
}