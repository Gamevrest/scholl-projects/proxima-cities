﻿using System;
using UnityEngine;
using UnityEngine.Experimental.Animations;

namespace AR
{
    public class PollutionDisplayer : MonoBehaviour
    {
        public MeshRenderer renderer;
        public Vector2 pollutionRange;
        public Vector2 alphaRange;

        private void Update()
        {
            var p = Mathf.Clamp(GameManager.Instance.GetPollution(), pollutionRange.x, pollutionRange.y);
            var alpha = (alphaRange.y - alphaRange.x) / p + alphaRange.y;
            var sharedMaterial = renderer.sharedMaterial;
            sharedMaterial.SetVector("_ALPHA", new Vector4(alpha, alpha));
            renderer.material = sharedMaterial;
        }
    }
}