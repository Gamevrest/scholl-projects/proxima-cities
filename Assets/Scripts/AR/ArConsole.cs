﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using TMPro;
//using UnityEngine;
//
//public class ArConsole : MonoBehaviour
//{
//    public enum Type
//    {
//        CONSOLE,
//        DBG1,
//        DBG2,
//        DBG3
//    }
//
//    private static TextMeshProUGUI console;
//    private static TextMeshProUGUI dbg1;
//    private static TextMeshProUGUI dbg2;
//    private static TextMeshProUGUI dbg3;
//
//    public static void LogConsole(string t, Type type = Type.CONSOLE)
//    {
//        if (!console) console = GameObject.Find("ArConsole").GetComponent<TextMeshProUGUI>();
//        if (!dbg1) dbg1 = GameObject.Find("DBG1").GetComponent<TextMeshProUGUI>();
//        if (!dbg2) dbg2 = GameObject.Find("DBG2").GetComponent<TextMeshProUGUI>();
//        if (!dbg3) dbg3 = GameObject.Find("DBG3").GetComponent<TextMeshProUGUI>();
//        var tmp = type == Type.CONSOLE ? console : type == Type.DBG1 ? dbg1 : type == Type.DBG2 ? dbg2 : dbg3;
//        if (type == Type.CONSOLE)
//            tmp.text += $"\n {t}";
//        else
//            tmp.text = t;
//    }
//    
//    void HandleLog(string logString, string stackTrace, LogType type)
//    {
//      //  LogConsole($"{type.ToString()} : {logString}");
//    }
//}