﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;


namespace AR
{
    public class ArManager : MonoBehaviour
    {
        private ARRaycastManager _raycastManager;
        public ArPrefabDictionary dictionary;
        public Camera arCamera;
        public Transform container;
        public GameObject defaultPrefab;
        public Vector2 scaleRange;
        public float decal;
        private bool _isCityFixed;
        public bool shouldCreateAr = true;

        private void OnEnable()
        {
            if (!_raycastManager) _raycastManager = GetComponent<ARRaycastManager>();
            _isCityFixed = false;
            if (shouldCreateAr)
                CreateArCity();
        }

        private void OnDisable()
        {
            if (shouldCreateAr)
                foreach (Transform child in container.transform)
                {
                    if (child.name != "NODELETE")
                        Destroy(child.gameObject);
                }
        }

        private void Update()
        {
            UpdateCityPosition();
        }

        private void CreateArCity()
        {
            var center = CreateBuildings();
            CreateRoads(center);
        }

        private Vector2 CreateBuildings()
        {
            var buildings = GameManager.Instance.GetBuildings();
            var buildList = new List<Transform>();
            var min = (Vector2) buildings[0].getPosition() * decal;
            var max = (Vector2) buildings[0].getPosition() * decal;
            foreach (var building in buildings)
            {
                var prefab = defaultPrefab;
                if (dictionary.buildingPrefabs.ContainsKey(building.getBuildingType()))
                    prefab = dictionary.buildingPrefabs[building.getBuildingType()];
                var obj = Instantiate(prefab, container);
                buildList.Add(obj.transform);
                obj.name = building.getName();
                var pos = (Vector2) building.getPosition() * decal;
                obj.transform.localPosition = new Vector3(pos.x, 0, pos.y);
                min.x = Mathf.Min(min.x, pos.x);
                min.y = Mathf.Min(min.y, pos.y);
                max.x = Mathf.Max(max.x, pos.x);
                max.y = Mathf.Max(max.y, pos.y);
            }

            var center = min + (max - min) / 2.0f;
            foreach (var t in buildList)
                t.localPosition -= new Vector3(center.x, 0, center.y);
            return center;
        }

        private void CreateRoads(Vector2 center)
        {
            var roads = GameManager.Instance.GetRoads();
            foreach (var road in roads)
            {
                var prefab = defaultPrefab;
                if (dictionary.roadPrefabs.ContainsKey(road.GetRoadType()))
                    prefab = dictionary.roadPrefabs[road.GetRoadType()];
                var obj = Instantiate(prefab, container);
                obj.name = road.GetType().ToString();
                obj.transform.localPosition =
                    new Vector3(road.GetPosition().x * decal - center.x - decal / 2f, 0,
                        road.GetPosition().y * decal - center.y - decal / 2f);
                obj.transform.localRotation = Quaternion.Euler(0, road.GetRotation() * -90, 0);
            }
        }

        private void UpdateCityPosition()
        {
            if (_isCityFixed) return;
            var hitResults = new List<ARRaycastHit>();
            var resolution = Screen.currentResolution;
            _raycastManager.Raycast(new Vector2(resolution.width / 2f, resolution.height / 2f), hitResults,
                TrackableType.Planes);
            if (hitResults.Count <= 0)
            {
                container.gameObject.SetActive(false);
                return;
            }

            container.gameObject.SetActive(true);
            var cameraForward = arCamera.transform.forward;
            var cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
            var pose = hitResults[0].pose;
            container.SetPositionAndRotation(pose.position, pose.rotation);
        }

        public void RotateCity(float r)
        {
            container.Rotate(0, -r, 0);
        }

        public void ZoomCity(float r)
        {
            var newSCale = container.localScale.x + r;
            if (newSCale > scaleRange.y) newSCale = scaleRange.y;
            if (newSCale < scaleRange.x) newSCale = scaleRange.x;
            container.localScale = Vector3.one * newSCale;
        }

        public void DropCity()
        {
            _isCityFixed = !_isCityFixed;
        }
    }
}