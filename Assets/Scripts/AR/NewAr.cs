﻿using System;
using System.Collections;
using RotaryHeart.Lib.SerializableDictionary;
using Tools;
using UI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.ARFoundation;

namespace AR
{
    public class NewAr : MonoBehaviour
    {
#if UNITY_EDITOR
        public bool bypassAr;
#endif
        public bool arCompatible;
        public GameObject arContainer;
        [SerializeField] private ARSession session;

        private IEnumerator CheckSupport()
        {
#if UNITY_EDITOR
            if (bypassAr)
            {
                arCompatible = true;
                yield break;
            }
#endif
            yield return ARSession.CheckAvailability();
            if (ARSession.state == ARSessionState.NeedsInstall)
            {
                DevConsole.Log("Ar supported but need install");
                yield return ARSession.Install();
            }

            if (ARSession.state == ARSessionState.Ready)
            {
                DevConsole.Log("AR is supported and installed");
                arCompatible = true;
            }
            else
            {
                switch (ARSession.state)
                {
                    case ARSessionState.Unsupported:
                        DevConsole.Log("AR unsupported");
                        break;
                    case ARSessionState.NeedsInstall:
                        DevConsole.Log("AR install failed or cancelled");
                        break;
                }
            }
        }

        private void Start()
        {
            EventManager.Instance.Subscribe(EnableAr);
            EventManager.Instance.Subscribe(DisableAr);
            arContainer.SetActive(false);
            arCompatible = false;
            StartCoroutine(CheckSupport());
        }

        private void EnableAr(SwitchToAREvent e)
        {
            if (arCompatible)
            {
                arContainer.SetActive(true);
                session.enabled = true;
                Invoke(nameof(StopLoading), 2);
            }
            else
            {
                WindowManager.ShowInfoMessage("AR_COMPATIBILITY_TITLE", "AR_COMPATIBILITY_DESC");
                DevConsole.Log("DEVICE NOT AR COMPATIBLE");
                EventManager.Instance.Raise(new SwitchTo2DEvent());
            }
        }

        private void DisableAr(SwitchTo2DEvent e)
        {
            session.enabled = false;
            arContainer.SetActive(false);
        }

        private static void StopLoading() => EventManager.Instance.Raise(new StopLoadingEvent());
    }
}