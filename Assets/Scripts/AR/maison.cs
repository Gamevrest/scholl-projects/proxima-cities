﻿using UnityEngine;

namespace AR
{
    public class maison : MonoBehaviour
    {
        public Transform Maison;

        [ReadOnly] public Vector3 position = Vector3.zero;
        [ReadOnly] public Vector3 rotation = Vector3.zero;
        [ReadOnly] public Vector3 scale = Vector3.one;

        public void Update()
        {
            Maison.localPosition = position;
            Maison.localEulerAngles = rotation;
            Maison.localScale = scale;
        }
    }
}