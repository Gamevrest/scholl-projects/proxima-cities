using System;
using UnityEngine;

namespace Buildings
{
    [Serializable]
    public class TownHallBuilding : BuildingsAbstract
    {
        public TownHallBuilding() : base (BuildingType.TOWN_HALL, BuildingClickAction.OPEN_TOWNHALL)
        {
            setPosition(new Vector2Int(6,5));
        }
    }
}