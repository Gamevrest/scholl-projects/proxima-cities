using System.ComponentModel;


    public enum BuildingType
    {
        [Description("Error Empty")]
        EMPTY,
        [Description("House")]
        HOUSE,
        [Description("Economical Habitation")]
        ECONOMICAL_HABITATION,
        [Description("Habitation")]
        HABITATION,
        [Description("Ecological House")]
        ECOLOGICAL_HOUSE,
        [Description("Business")]
        BUSINESS,
        [Description("Large Economical Habitation")]
        LARGE_ECONOMICAL_HABITATION,
        [Description("Better Habitation")]
        BETTER_HABITATION,
        [Description("Ecological Habitation")]
        ECOLOGICAL_HABITATION,
        [Description("Ecological Business")]
        ECOLOGICAL_BUSINESS,
        [Description("First Class Business")]
        FIRST_CLASS_BUSINESS,
        [Description("Wind Turbine")]
        WIND_TURBINE,
        [Description("Water Turbine")]
        WATER_TURBINE,
        [Description("Solar Panels")]
        SOLAR_PANELS,
        [Description("Nuclear Power Plant")]
        NUCLEAR_POWER_PLANTS,
        [Description("Charcoal Power Plant")]
        CHARCOAL_POWER_PLANTS,
        [Description("Water Recycling Station")]
        WATER_RECYCLING_STATION,
        [Description("Plastic Recycling Station")]
        PLASTIC_RECYCLING_STATION,
        [Description("Air Purifier")]
        AIR_PURIFIER,
        [Description("Greenhouse")]
        GREEN_HOUSE,
        [Description("Town Hall")]
        TOWN_HALL
    }

    public static class BuildingTypeExtension
    {
        public static string getDefaultName(this BuildingType val)
        {
            var attributes = (DescriptionAttribute[])val
                .GetType()
                .GetField(val.ToString())
                .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }