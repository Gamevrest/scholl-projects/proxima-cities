using System;


[Serializable]
public class WaterRecyclingStation : BuildingsAbstract
{
    public WaterRecyclingStation() : base (BuildingType.WATER_RECYCLING_STATION, BuildingClickAction.OPEN_INFO)
    {
        setPrice(30000);
        setEnergyNeededBase(5);
        setPollutionBase(-60);
    }
}