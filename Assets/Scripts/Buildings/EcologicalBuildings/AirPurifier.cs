using System;


[Serializable]
public class AirPurifier : BuildingsAbstract
{
    public AirPurifier() : base (BuildingType.AIR_PURIFIER, BuildingClickAction.OPEN_INFO)
    {
        setPrice(20000);
        setEnergyNeededBase(3);
        setPollutionBase(-20);
    }
}