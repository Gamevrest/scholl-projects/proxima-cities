using System;
using System.Collections.Generic;

[Serializable]
public class Habitation : BuildingsAbstract
{
    public Habitation() : base (BuildingType.HABITATION, BuildingClickAction.COLLECT_MONEY)
    {
        setPrice(13500);
        setMaxMoneyToCollect(16000);
        setCitizenBase(2);
        setEnergyNeededBase(2);
        setMoneyPerMinuteBase(200);
        setPollutionBase(20);
        setUpgradeList(new List<BuildingType>
        {
            BuildingType.BETTER_HABITATION,
            BuildingType.ECOLOGICAL_HABITATION
        });
    }
}