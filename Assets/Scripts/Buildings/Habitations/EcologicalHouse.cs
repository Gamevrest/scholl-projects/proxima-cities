using System;
using System.Collections.Generic;

[Serializable]
public class EcologicalHouse : BuildingsAbstract
{
    public EcologicalHouse() : base (BuildingType.ECOLOGICAL_HOUSE, BuildingClickAction.COLLECT_MONEY)
    {
        setPrice(21000);
        setMaxMoneyToCollect(16000);
        setCitizenBase(1);
        setEnergyNeededBase(2);
        setMoneyPerMinuteBase(200);
        setPollutionBase(10);
        setUpgradeList(new List<BuildingType>
        {
            BuildingType.ECOLOGICAL_HABITATION,
            BuildingType.ECOLOGICAL_BUSINESS
        });
    }
}