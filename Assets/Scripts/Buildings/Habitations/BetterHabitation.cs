using System;


[Serializable]
public class BetterHabitation : BuildingsAbstract
{
    public BetterHabitation() : base(BuildingType.BETTER_HABITATION, BuildingClickAction.COLLECT_MONEY)
    {
        setPrice(27000);
        setMaxMoneyToCollect(40000);
        setCitizenBase(4);
        setEnergyNeededBase(3);
        setMoneyPerMinuteBase(500);
        setPollutionBase(40);
    }
}