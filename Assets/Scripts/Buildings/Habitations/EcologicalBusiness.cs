using System;


[Serializable]
public class EcologicalBusiness : BuildingsAbstract
{
    public EcologicalBusiness() : base (BuildingType.ECOLOGICAL_BUSINESS, BuildingClickAction.COLLECT_MONEY)
    {
        setPrice(54000);
        setMaxMoneyToCollect(56000);
        setCitizenBase(2);
        setEnergyNeededBase(6);
        setMoneyPerMinuteBase(700);
        setPollutionBase(30);
    }
}