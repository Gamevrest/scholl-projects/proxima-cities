using System;

[Serializable]
public class FirstClassBusiness : BuildingsAbstract
{
    public FirstClassBusiness() : base (BuildingType.FIRST_CLASS_BUSINESS, BuildingClickAction.COLLECT_MONEY)
    {
        setPrice(66000);
        setMaxMoneyToCollect(80000);
        setCitizenBase(2);
        setEnergyNeededBase(6);
        setMoneyPerMinuteBase(1000);
        setPollutionBase(60);
    }
}