using System;

[Serializable]
public class EcologicalHabitation : BuildingsAbstract
{
    public EcologicalHabitation() : base (BuildingType.ECOLOGICAL_HABITATION, BuildingClickAction.COLLECT_MONEY)
    {
        setPrice(36000);
        setMaxMoneyToCollect(32000);
        setCitizenBase(3);
        setEnergyNeededBase(4);
        setMoneyPerMinuteBase(400);
        setPollutionBase(20);
    }
}