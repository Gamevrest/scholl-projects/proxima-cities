using System;
using System.Collections.Generic;


[Serializable]
public class EconomicalHabitation : BuildingsAbstract
{
    public EconomicalHabitation() : base (BuildingType.ECONOMICAL_HABITATION, BuildingClickAction.COLLECT_MONEY)
    {
        setPrice(9000);
        setMaxMoneyToCollect(8000);
        setCitizenBase(2);
        setEnergyNeededBase(1);
        setMoneyPerMinuteBase(100);
        setPollutionBase(20);
        setUpgradeList(new List<BuildingType>
        {
            BuildingType.LARGE_ECONOMICAL_HABITATION,
            BuildingType.BETTER_HABITATION
        });
    }
}