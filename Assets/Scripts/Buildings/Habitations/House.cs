using System;
using System.Collections.Generic;


[Serializable]
public class House : BuildingsAbstract
{
    public House() : base (BuildingType.HOUSE, BuildingClickAction.COLLECT_MONEY)
    {
        setPrice(3000);
        setMaxMoneyToCollect(8000);
        setCitizenBase(1);
        setEnergyNeededBase(1);
        setMoneyPerMinuteBase(100);
        setPollutionBase(10);
        setUpgradeList(new List<BuildingType>
        {
            BuildingType.ECONOMICAL_HABITATION,
            BuildingType.HABITATION,
            BuildingType.ECOLOGICAL_HOUSE,
            BuildingType.BUSINESS
        });
    }
}