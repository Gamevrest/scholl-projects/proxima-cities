using System;
using System.Collections.Generic;

[Serializable]
public class Business : BuildingsAbstract
{
    public Business() : base (BuildingType.BUSINESS, BuildingClickAction.COLLECT_MONEY)
    {
        setPrice(27000);
        setMaxMoneyToCollect(32000);
        setCitizenBase(1);
        setEnergyNeededBase(3);
        setMoneyPerMinuteBase(400);
        setPollutionBase(30);
        setUpgradeList(new List<BuildingType>
        {
            BuildingType.ECOLOGICAL_BUSINESS,
            BuildingType.FIRST_CLASS_BUSINESS
        });
    }
}