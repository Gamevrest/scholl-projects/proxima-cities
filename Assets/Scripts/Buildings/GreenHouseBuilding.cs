using System;
using UnityEngine;

namespace Buildings
{
    [Serializable]
    public class GreenHouseBuilding : BuildingsAbstract
    {
        public GreenHouseBuilding() : base (BuildingType.GREEN_HOUSE, BuildingClickAction.OPEN_GREENHOUSE)
        {
            setPosition(new Vector2Int(7,5));
        }
    }
}