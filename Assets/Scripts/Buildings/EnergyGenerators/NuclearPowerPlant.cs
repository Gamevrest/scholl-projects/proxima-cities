﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class NuclearPowerPlant : BuildingsAbstract
{
    public NuclearPowerPlant() : base (BuildingType.NUCLEAR_POWER_PLANTS, BuildingClickAction.OPEN_INFO)
    {
        setPrice(24000);
        setPollutionBase(200);
        setEnergyGenerateBase(20);
        setEnergyZone(new List<Vector2Int>
        {
            new Vector2Int(-2,-2), new Vector2Int(-1,-2), new Vector2Int(0,-2), new Vector2Int(1,-2), new Vector2Int(2,-2),
            new Vector2Int(-2,-1), new Vector2Int(-1,-1), new Vector2Int(0,-1), new Vector2Int(1,-1), new Vector2Int(2,-1),
            new Vector2Int(-2,0), new Vector2Int(-1,0), new Vector2Int(0,0), new Vector2Int(1,0), new Vector2Int(2,0),
            new Vector2Int(-2,1), new Vector2Int(-1,1), new Vector2Int(0,1), new Vector2Int(1,1), new Vector2Int(2,1),
            new Vector2Int(-2,2), new Vector2Int(-1,2), new Vector2Int(0,2), new Vector2Int(1,2), new Vector2Int(2,2),
        });
    }
}