﻿using System;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class WindTurbine : BuildingsAbstract
{
    public WindTurbine() : base (BuildingType.WIND_TURBINE, BuildingClickAction.OPEN_INFO)
    {
        setPrice(6000);
        setPollutionBase(5);
        setEnergyGenerateBase(1);
        setUpgradeList(new List<BuildingType>
        {
            BuildingType.SOLAR_PANELS
        });
        setEnergyZone(new List<Vector2Int>
        {
            new Vector2Int(-2,0), new Vector2Int(-1,0), new Vector2Int(0,0),new Vector2Int(1,0), new Vector2Int(2,0),
        });
    }
}