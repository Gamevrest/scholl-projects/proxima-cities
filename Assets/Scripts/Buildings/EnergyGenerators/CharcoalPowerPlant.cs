﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CharcoalPowerPlant : BuildingsAbstract
{
    public CharcoalPowerPlant() : base (BuildingType.CHARCOAL_POWER_PLANTS, BuildingClickAction.OPEN_INFO)
    {
        setPrice(6000);
        setPollutionBase(80);
        setEnergyGenerateBase(4);
        setUpgradeList(new List<BuildingType>
        {
            BuildingType.NUCLEAR_POWER_PLANTS
        });
        setEnergyZone(new List<Vector2Int>
        {
            new Vector2Int(-2,-1), new Vector2Int(-1,-1), new Vector2Int(0,-1), new Vector2Int(1,-1), new Vector2Int(2,-1),
            new Vector2Int(-2,0), new Vector2Int(-1,0), new Vector2Int(0,0), new Vector2Int(1,0), new Vector2Int(2,0),
            new Vector2Int(-2,1), new Vector2Int(-1,1), new Vector2Int(0,1), new Vector2Int(1,1), new Vector2Int(2,1),
        });
    }
}