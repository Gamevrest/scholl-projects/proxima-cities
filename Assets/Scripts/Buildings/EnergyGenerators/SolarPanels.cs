﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SolarPanels : BuildingsAbstract
{
    public SolarPanels() : base (BuildingType.SOLAR_PANELS, BuildingClickAction.OPEN_INFO)
    {
        setPrice(12000);
        setPollutionBase(10);
        setEnergyGenerateBase(3);
        setEnergyZone(new List<Vector2Int>
        {
            new Vector2Int(-1,-1), new Vector2Int(0,-1), new Vector2Int(1,-1),
            new Vector2Int(-1,0), new Vector2Int(0,0), new Vector2Int(1,0),
            new Vector2Int(-1,1), new Vector2Int(0,1), new Vector2Int(1,1),
        });
    }
}